﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Models.Response
{
    public class UserStatsResponse
    {

        public int Total { get; set; }
        public int NumOfUsers { get; set; }
        public int NumOfSuperUsers { get; set; }
        public int NumOfAdministrators { get; set; }

    }
}
