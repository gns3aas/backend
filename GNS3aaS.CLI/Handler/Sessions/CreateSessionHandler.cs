﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    public class CreateSessionHandler : BaseHandler, IHandler
    {
        public CreateSessionHandler(ConfigStorage configStorage) : base(configStorage)
        {

        }

        private async Task handle()
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.CreateSession();
                if (response.Success)
                {
                    Console.WriteLine($"{response.Data}");
                }
                else
                {
                    Console.WriteLine("session creation failed");
                }
            }
        }

        public Command GetCommand()
        {
            var command = new Command("create", "create a new session for current user");

            command.SetHandler(async () =>
            {
                await handle();
            });

            return command;
        }
    }
}
