﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Models.Requests
{
    public class NewServerNodeRequest
    {
        public string Name { get; set; }

        public string HostAddress { get; set; }

        public int OpenVpnPort { get; set; }

        public int WireGuardPort { get; set; }

    }
}
