﻿using Microsoft.Extensions.Configuration;

namespace GNS3aaS.Lib.Authentification
{
    public class JwtTokenProvider
    {
        private readonly IConfiguration configuration;

        public JwtTokenProvider(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public string GenerateTokenFor(Guid userId, string email, string groupName, JwtUserRoles userRole, int validForMinutes)
        {
            return JwtTokenHelper.GenerateNewToken(configuration, userId.ToString(), email, groupName, userRole, validForMinutes);
        }
    }
}
