﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Providers;
using GNS3aaS.Lib.Models.Enums;
using Microsoft.EntityFrameworkCore;

namespace GNS3aaS.Controller.Testing
{
    public class DbTestHelper
    {
        public GNS3aaSDbContext DbContext = new GNS3aaSDbContext(inMemory: false);
        public static UserEntity TestUser1;
        public static UserEntity AdminUser;
        public static readonly string UserEmail = "user@gns3aas.com";
        public static readonly string AdminEmail = "admin@gns3aas.com";
        private readonly IConfiguration Configuration;

        public DbTestHelper(IConfiguration configuration)
        {
            Configuration = configuration;
            var numberOfMigrations = DbContext.Database.GetPendingMigrations().Count();
            DbContext.Database.Migrate();
            if (numberOfMigrations > 0)
            {
                Populate();
            }
            
        }

        public void Populate()
        {
            DbTestHelper.Populate(DbContext, Configuration);
        }

        public static void RetrieveUsers()
        {
            
        }

        public static void Populate(GNS3aaSDbContext dbContext, IConfiguration Configuration)
        {
            var secretProvider = new SecretProvider(Configuration);
            TestUser1 = new UserEntity()
            {
                EMailHash = secretProvider.HashEmail(UserEmail),
                Id = Guid.NewGuid(),
                GroupName = "GroupNo1",
                UserRole = Lib.Authentification.JwtUserRoles.User
            };

            dbContext.Users.Add(TestUser1);

            AdminUser = new UserEntity()
            {
                EMailHash = secretProvider.HashEmail(AdminEmail),
                Id = Guid.NewGuid(),
                GroupName = "Administrators",
                UserRole = Lib.Authentification.JwtUserRoles.Administrator
            };

            dbContext.Users.Add(AdminUser);

            dbContext.SaveChanges();

            dbContext.ActiveGroups.Add(new ActiveGroupEntity()
            {
                Id = Guid.NewGuid(),
                Name = "GroupNo1"
            });

            dbContext.SaveChanges();

            var server51 = new ServerNodeEntity()
            {
                Id = Guid.NewGuid(),
                HostAddress = "10.0.33.127",
                Name = "Cloud-HF-27",
                ServerNodeState = ServerNodeState.New,  
            };
            dbContext.Add(server51);
            
            var server52 = new ServerNodeEntity()
            {
                Id = Guid.NewGuid(),
                HostAddress = "10.0.33.128",
                Name = "Cloud-HF-28",
                ServerNodeState = ServerNodeState.New
            };
            dbContext.Add(server52);
            var server53 = new ServerNodeEntity()
            {
                Id = Guid.NewGuid(),
                HostAddress = "10.0.33.129",
                Name = "Cloud-HF-29",
                ServerNodeState = ServerNodeState.New
            };
            dbContext.Add(server53);
            dbContext.SaveChanges();
        }

    }
}
