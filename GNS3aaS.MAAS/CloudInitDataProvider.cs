﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.MAAS
{
    public static class CloudInitDataProvider
    {
        public static string GetCloudInit()
        {
            var assmblyPath = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory!.FullName;

            var cloudInit = File.ReadAllText(Path.Combine(assmblyPath, "DeployScripts", "cloud-init.yaml")).Replace("\r\n", "\n");
            var networkingSh = File.ReadAllText(Path.Combine(assmblyPath, "DeployScripts", "networking.sh")).Replace("\r\n", "\n");
            var gns3RemoteInstall = File.ReadAllText(Path.Combine(assmblyPath, "DeployScripts", "gns3-remote-install.sh")).Replace("\r\n", "\n");


            cloudInit = cloudInit.Replace("---REPLACE-NETWORKING-SH---", System.Convert.ToBase64String(Encoding.ASCII.GetBytes(networkingSh)));
            cloudInit = cloudInit.Replace("---REPLACE-GNS3-REMOTE-INSTALL---", System.Convert.ToBase64String(Encoding.ASCII.GetBytes(gns3RemoteInstall)));

            return cloudInit;
        }
    }
}
