﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Providers;
using GNS3aaS.Lib.Authentification;
using GNS3aaS.Lib.Models.Requests;
using GNS3aaS.Lib.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GNS3aaS.Controller.Controllers
{
    [ApiController]
    public class ServerNodeController : ControllerBase
    {
        private GNS3aaSDbContext dbContext;

        public ServerNodeController(GNS3aaSDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [Authorize(Policy = "SuperUser")]
        [HttpGet("api/v1/servernodes")]
        public object GetServerNodes()
        {
            return dbContext.ServerNodes.ToList();
        }

        [Authorize(Policy = "Administrator")]
        [HttpPost("api/v1/servernode/add")]
        public IActionResult AddServerNode([FromBody] NewServerNodeRequest request)
        {
            if (dbContext.ServerNodes.Any(s => s.Name == request.Name || s.HostAddress.ToLower() == request.HostAddress.ToLower()))
            {
                return Ok(new DefaultResponse() { Success = false, Message = "hostaddress or name already exists" });
            }

            var serverNodeEntity = new ServerNodeEntity()
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                HostAddress = request.HostAddress,
                ServerNodeState = Lib.Models.Enums.ServerNodeState.New,
                OpenVpnPort = request.OpenVpnPort,
                WireGuardPort = request.WireGuardPort,
            };

            dbContext.ServerNodes.Add(serverNodeEntity);

            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "added servernote", Data = serverNodeEntity.Id });
        }

        [Authorize(Policy = "Administrator")]
        [HttpPost("api/v1/servernode/edit")]
        public IActionResult EditServerNode([FromBody] UpdateServerNodeRequest request)
        {
            var serverNode = dbContext.ServerNodes.SingleOrDefault(s => s.Id == request.Id);
            
            if (serverNode == null)
            {
                return NotFound();
            }

            if(request.OpenVpnPort.HasValue) serverNode.OpenVpnPort = request.OpenVpnPort.Value;
            if(request.WireGuardPort.HasValue) serverNode.WireGuardPort = request.WireGuardPort.Value;

            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "updated servernote", Data = serverNode.Id });
        }

        [Authorize(Policy = "Administrator")]
        [HttpDelete("api/v1/servernode/delete/{id}")]
        public IActionResult DeleteServerNode(Guid id)
        {
            var serverNode = dbContext.ServerNodes.SingleOrDefault(s => s.Id == id);

            if (serverNode == null)
            {
                return NotFound();
            }

            dbContext.ServerNodes.Remove(serverNode);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = $"server node deleted", Data = serverNode.Id });
        }

        [Authorize(Policy = "Administrator")]
        [HttpGet("api/v1/servernode/reset/{id}")]
        public object ResetServerNode(Guid id)
        {
            var serverNode = dbContext.ServerNodes.SingleOrDefault(s => s.Id == id);

            if(serverNode == null)
            {
                return NotFound();
            }

            serverNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.New;
            dbContext.SaveChanges();
            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

        [Authorize(Policy = "Administrator")]
        [HttpGet("api/v1/servernode/publish/{id}")]
        public object PublishServerNode(Guid id)
        {
            var serverNode = dbContext.ServerNodes.SingleOrDefault(s => s.Id == id);

            if (serverNode == null)
            {
                return NotFound();
            }

            if (serverNode.ServerNodeState != Lib.Models.Enums.ServerNodeState.New 
                && serverNode.ServerNodeState != Lib.Models.Enums.ServerNodeState.Reserved)
            {
                return Ok(new DefaultResponse() { Success = false, Message = "only new and reserved nodes can be published (=available)" });
            }

            serverNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Available;
            dbContext.SaveChanges();
            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

        [Authorize(Policy = "Administrator")]
        [HttpGet("api/v1/servernode/reserve/{id}")]
        public object ReserveServerNode(Guid id)
        {
            var serverNode = dbContext.ServerNodes.SingleOrDefault(s => s.Id == id);

            if (serverNode == null)
            {
                return NotFound();
            }

            serverNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Reserved;
            dbContext.SaveChanges();
            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

        [Authorize(Policy = "Administrator")]
        [HttpGet("api/v1/servernode/deploy/{id}")]
        public object DeployServerNode(Guid id)
        {
            var serverNode = dbContext.ServerNodes.SingleOrDefault(s => s.Id == id);

            if (serverNode == null)
            {
                return NotFound();
            }

            if(serverNode.ServerNodeState != Lib.Models.Enums.ServerNodeState.New)
            {
                return Ok(new DefaultResponse() { Success = false, Message = "only new nodes can be deployed" });
            }

            serverNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.InstallRequested;
            dbContext.SaveChanges();
            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

        [Authorize(Policy = "Administrator")]
        [HttpGet("api/v1/servernode/release/{id}")]
        public object ReleaseServerNode(Guid id)
        {
            var serverNode = dbContext.ServerNodes.SingleOrDefault(s => s.Id == id);

            if (serverNode == null)
            {
                return NotFound();
            }

            if (serverNode.ServerNodeState != Lib.Models.Enums.ServerNodeState.Reserved
                && serverNode.ServerNodeState != Lib.Models.Enums.ServerNodeState.Broken
                && serverNode.ServerNodeState != Lib.Models.Enums.ServerNodeState.Available)
            {
                return Ok(new DefaultResponse() { Success = false, Message = "only reserved, broken and available nodes can be released" });
            }

            serverNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.ReleaseRequested;
            dbContext.SaveChanges();
            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }
    }
}
