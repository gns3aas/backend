﻿namespace GNS3aaS.Lib.Models.Enums
{
    public enum SessionState
    {
        IDLE = 10,
        START_REQUESTED = 13,
        STARTING = 15,
        RUNNING = 20,
        BROKEN = 40,
        STOP_REQUESTED = 50,
        STOPPING = 60,
    }
}
