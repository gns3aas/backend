﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Helper
{
    public class WireguardStringConfigHelper
    {

        public static string? ExtractServerKey(string? fullConfig)
        {
            if (fullConfig == null) return null;

            // gen-wireguard.sh produces an output and includes the server config file as the last line
            return fullConfig!.Trim().Replace("\r", "").Split("\n").Last();
        }

        public static string? RemoveServerKey(string? fullConfig)
        {
            if (fullConfig == null) return null;

            var parts = fullConfig!.Trim().Replace("\r", "").Split('\n');
            if (parts.Count() <= 1) return fullConfig;
            return string.Join("\n", parts.SkipLast(1));
        }

        public static string? AdjustEndpoints(string? fullConfig, string hostName, int port)
        {
            if (fullConfig == null) return null;
            var encodedConfigs = fullConfig!.Trim().Replace("\r", "").Split('\n');
            for(int a = 0; a < encodedConfigs.Length -1; a++)
            {
                var encodedConfig = encodedConfigs[a];

                var config = Encoding.UTF8.GetString(Convert.FromBase64String(encodedConfig));
                config = AdjustEndpoint(config, hostName, port);
                encodedConfigs[a] = Convert.ToBase64String(Encoding.UTF8.GetBytes(config));
            }
            return string.Join("\n", encodedConfigs);
        }

        public static string AdjustEndpoint(string clientConfigFile, string host, int remotePort)
        {
            var configFileLines = clientConfigFile.Split("\n");
            for (int a = 0; a < configFileLines.Length; a++)
            {
                var line = configFileLines[a].Trim();
                if (line.StartsWith("Endpoint = "))
                {
                    configFileLines[a] = $"Endpoint = {host}:{remotePort}";
                }
            }
            return string.Join("\n", configFileLines);
        }

    }
}
