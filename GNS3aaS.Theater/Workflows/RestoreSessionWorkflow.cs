﻿using GNS3aaS.Lib.Helper;
using GNS3aaS.Lib.Logging;
using GNS3aaS.Theater.Playbooks;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Workflows
{
    public class RestoreSessionWorkflow : IWorkflow
    {
        private readonly string sessionId;
        private readonly SshConnectionInfo onHost;
        private readonly SshConnectionInfo? fromHost;

        public bool IsSuccessfull { get; private set; } = false;
        public string Message { get; private set; } = "";

        public bool IsFinished { get; private set; } = false;

        

        public string? GNS3Version { get; private set; }
        public string? WireGuardServerConf { get; private set; }

        public string? OpenVPNClientConfigFile { get; private set; }
        public string? OpenVPNServerConfigFile { get; private set; }
        public PublicHostConnectionInfo PublicHostInfo { get; }
        public string? WireGuardConf { get; set; }

        private readonly ILogger<RestoreSessionWorkflow> logger = StaticLoggerFactory.GetStaticLogger<RestoreSessionWorkflow>();

        public RestoreSessionWorkflow(string sessionId, 
            string? existingWireGuardServerConf, 
            string? existingOpenVPNClientConf,
            string? existingOpenVPNServerConf,
            PublicHostConnectionInfo publicHostInfo, 
            SshConnectionInfo onHost, 
            SshConnectionInfo? fromHost = null)
        {
            this.sessionId = sessionId;
            WireGuardServerConf = existingWireGuardServerConf;
            OpenVPNClientConfigFile = existingOpenVPNClientConf;
            OpenVPNServerConfigFile = existingOpenVPNServerConf;
            PublicHostInfo = publicHostInfo;
            this.onHost = onHost;
            this.fromHost = fromHost;
        }

        public void Run()
        {
            if (fromHost != null && !fromHost.Equals(onHost))
            {
                if(!RemoteCopyToOtherSystem(fromHost, onHost, sessionId))
                {
                    logError($"Failed RemoteCopyToOtherSystem from {fromHost} on {onHost} for {sessionId}");
                    IsFinished = true;
                    return;
                }
            }
            if (!CreateNewSessionNoArchiveWorkflow.GetGNS3Version(onHost, out var gns3version))
            {
                IsSuccessfull = false;
                IsFinished = true;
                logError($"Failed to get GNS3 Version on {onHost}");
                return;
            }
            GNS3Version = gns3version;

            if (OpenVPNServerConfigFile == null) {
                if (!CreateNewSessionNoArchiveWorkflow.NewOpenVPNKeysPlaybook(onHost, out var openVPNConfigFile, out var openVPNServerConfigurationFile))
                {
                    IsSuccessfull = false;
                    IsFinished = true;
                    logError($"Failed to create new openvpn client config on {onHost}");
                    return;
                }
                OpenVPNClientConfigFile = openVPNConfigFile;
                OpenVPNServerConfigFile = openVPNServerConfigurationFile;
            }
            else
            {
                if(!RestoreOpenVpnServerConf(onHost, OpenVPNServerConfigFile))
                {
                    IsSuccessfull = false;
                    IsFinished = true;
                    logError($"Failed to restore new openvpn configuration {onHost}");
                    return;
                }
            }
            
            if(WireGuardServerConf == null)
            {
                if(!CreateNewSessionNoArchiveWorkflow.NewWireguardKeys(onHost, PublicHostInfo.HostName, out var wireguardConfs))
                {
                    IsSuccessfull = false;
                    IsFinished = true;
                    logError($"Failed to create new wireguard configuration config on {onHost}");
                    return;
                }
                WireGuardConf = wireguardConfs;
            }
            else
            {
                if(!RestoreWiregardServerConf(onHost, WireGuardServerConf))
                {
                    IsSuccessfull = false;
                    IsFinished = true;
                    logError($"Failed to restore new wireguard configuration {onHost}");
                    return;
                }
            }

            if(OpenVPNClientConfigFile  == null)
            {
                logError($"OpenVPNClientConfigFile is null for {sessionId}");
                IsFinished = true;
                return;
            }

            OpenVPNClientConfigFile = UpdateOpenVPNClientConfigFile(OpenVPNClientConfigFile, PublicHostInfo);
            WireGuardConf = UpdateWireGuardConfigFile(WireGuardConf, PublicHostInfo);

            if (!LoadAndStartPlaybook(onHost, sessionId))
            {
                logError($"Failed to LoadAndStartPlaybook on {onHost} for {sessionId}");
                IsFinished = true;
                return;
            }
            IsSuccessfull = true;
            IsFinished = true;
        }

        public static string UpdateOpenVPNClientConfigFile(string openVPNClientConfigFile, PublicHostConnectionInfo publicHostInfo)
        {
            return OpenVPNConfigHelper.AdjustRemotePort(openVPNClientConfigFile, publicHostInfo.HostName, publicHostInfo.OpenVpnPort);
        }

        public static string? UpdateWireGuardConfigFile(string? wireGuardConfigFile, PublicHostConnectionInfo publicHostInfo)
        {
            return WireguardStringConfigHelper.AdjustEndpoints(wireGuardConfigFile, publicHostInfo.HostName, publicHostInfo.WireGuardPort);
        }

        public static bool RestoreWiregardServerConf(SshConnectionInfo onHost, string wireguardConf)
        {
            var wireguardRestoreRunner = new PlaybookRunner(onHost, new SetWireguardConfPlaybook(wireguardConf));
            wireguardRestoreRunner.Run();

            return wireguardRestoreRunner.IsSuccess();
        }

        public static bool RestoreOpenVpnServerConf(SshConnectionInfo onHost, string openVpnServerConfig)
        {
            var setOpenvpnConfPlaybookRunner = new PlaybookRunner(onHost, new SetOpenvpnConfPlaybook());
            setOpenvpnConfPlaybookRunner.Files[ConfigureOpenVpnPlaybook.OPENVPN_BACKUP_FILE] = Convert.FromBase64String(openVpnServerConfig);
            setOpenvpnConfPlaybookRunner.Run();

            return setOpenvpnConfPlaybookRunner.IsSuccess();
        }

        private void logError(string message)
        {
            Message += " " + message;
            Message = Message.Trim();
            logger.LogError(message);
        }

        static bool LoadAndStartPlaybook(SshConnectionInfo host, string sessionId)
        {
            var playbookRunner = new PlaybookRunner(host, new LoadAndStartPlaybook(sessionId));

            playbookRunner.Run();

            return playbookRunner.IsSuccess();
        }

        private bool RemoteCopyToOtherSystem(SshConnectionInfo fromHost, SshConnectionInfo toHost, string sessionId)
        {
            var checkFileAndMd5SumRunner = new PlaybookRunner(fromHost, new CheckArchiveFileAvailabilityAndMd5SumPlaybook(sessionId, fromHost));
            checkFileAndMd5SumRunner.Run();

            if(!checkFileAndMd5SumRunner.IsSuccess())
            {
                logError($"Archive file for {sessionId} not accessible on {fromHost}");
                return false;
            }

            if(!InstallKeyOnOtherSystem(fromHost, toHost, out var publicKey))
            {
                logError($"Could not install key from {fromHost} on {toHost} for {sessionId}");
                return false;
            }

            var sshCopyPlaybookRunner = new PlaybookRunner(fromHost, new RemoteCopyUserProjectPlaybook(sessionId, toHost));

            sshCopyPlaybookRunner.Run();

            var copyFailed = false;

            if (!sshCopyPlaybookRunner.IsSuccess())
            {
                logger.LogError($"Error copy {sessionId}");
                copyFailed = true;
            }

            if(!RemoveKeyFromOtherSystem(toHost, publicKey))
            {
                logError($"Could nod remove key from {toHost} for {sessionId}");
            }

            if (copyFailed)
            {
                return false;
            }

            var verifyMd5SumPlaybookRunner = new PlaybookRunner(toHost, new VerifyMd5SumPlaybook(checkFileAndMd5SumRunner.Artifacts["md5sum"].Trim()));

            verifyMd5SumPlaybookRunner.Run();

            if (!verifyMd5SumPlaybookRunner.IsSuccess())
            {
                logger.LogError($"Error verify copied project archive {sessionId}");
                return false;
            }

            var deleteArchiveOnFromHost = new PlaybookRunner(fromHost, new DeleteUserProjectArchivePlaybook(sessionId));

            deleteArchiveOnFromHost.Run();

            return deleteArchiveOnFromHost.IsSuccess();
        }

        private bool InstallKeyOnOtherSystem(SshConnectionInfo accessingHost, SshConnectionInfo destinationHost, out string? publicKey)
        {
            publicKey = null;
            
            var sshKeyGenPlaybookRunner = new PlaybookRunner(accessingHost, new CreateSSHPrivateKeyPlaybook());
            sshKeyGenPlaybookRunner.Run();

            if (!sshKeyGenPlaybookRunner.IsSuccess()) return false;

            publicKey = Encoding.UTF8.GetString(sshKeyGenPlaybookRunner.Files[CreateSSHPrivateKeyPlaybook.SSH_PUBLIC_KEY_PATH]);

            var sshAddPubKeyPlaybookRunner = new PlaybookRunner(destinationHost, new AddPublicKeyToAuthorizedKeysPlaybook(publicKey));
            sshAddPubKeyPlaybookRunner.Run();

            if (!sshAddPubKeyPlaybookRunner.IsSuccess()) return false;

            return true;
        }

        private bool RemoveKeyFromOtherSystem(SshConnectionInfo host, string publicKey)
        {
            var sshRemovePubKeyPlaybookRunner = new PlaybookRunner(host, new RemovePublicKeyToAuthorizedKeysPlaybook(publicKey));

            sshRemovePubKeyPlaybookRunner.Run();

            return sshRemovePubKeyPlaybookRunner.IsSuccess();
        }
    }
}
