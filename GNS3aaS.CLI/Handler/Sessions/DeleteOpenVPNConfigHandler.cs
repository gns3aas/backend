﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    internal class DeleteOpenVPNConfigHandler : BaseHandler, IHandler
    {
        public DeleteOpenVPNConfigHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("delete-openvpn", $"delete openvpn config from session (dangerous!)");

            var sessionIdArg = new Argument<string>("sessionId");

            command.AddArgument(sessionIdArg);

            command.SetHandler(async (sessionId) =>
            {
                await handle(sessionId);
            }, sessionIdArg);
            return command;
        }

        private async Task handle(string sessionId)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.DeleteOpenVPNFromSession(sessionId);
                if (response != null && response.Success)
                {
                    Console.WriteLine($"delete openvpn config from session OK");
                }
                else
                {
                    Console.WriteLine($"delete openvpn config from session FAILED with Message {response?.Message}");
                }
            }
        }
    }
}
