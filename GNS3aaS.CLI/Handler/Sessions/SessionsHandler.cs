﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    internal class SessionsHandler : BaseHandler, IHandler
    {
        public SessionsHandler(ConfigStorage configStorage) : base(configStorage)
        {

        }

        public Command GetCommand()
        {
            var subCommands = new Command("session");

            subCommands.Add(new SessionsListHandler(configStorage).GetCommand());
            subCommands.Add(new GetSessionHandler(configStorage).GetCommand());
            subCommands.Add(new StartSessionHandler(configStorage).GetCommand());
            subCommands.Add(new StopSessionHandler(configStorage).GetCommand());
            subCommands.Add(new CreateSessionHandler(configStorage).GetCommand());
            subCommands.Add(new DownloadOpenVPNKeyForSessionHandler(configStorage).GetCommand());
            subCommands.Add(new DownloadWireguardKeyForSessionHandler(configStorage).GetCommand());
            subCommands.Add(new DeleteSessionHandler(configStorage).GetCommand());
            subCommands.Add(new CreateForUserHandler(configStorage).GetCommand());
            subCommands.Add(new ForceIdleSessionHandler(configStorage).GetCommand());
            subCommands.Add(new DeleteOpenVPNConfigHandler(configStorage).GetCommand());

            return subCommands;
        }
    }
}

