﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Services;
using GNS3aaS.Theater.Workflows;
using Renci.SshNet.Common;
using System.Diagnostics;
using System.Globalization;

namespace GNS3aaS.Controller.Tasks
{
    public class StatsCollectorTask : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly ILogger<StatsCollectorTask> logger;
        private readonly SshConnectionInfoProvider sshConnectionInfoProvider;

        public const int COLLECT_STATS_EVERY_MINUTES = 5;

        public IServiceProvider Services { get; }
        public IConfiguration Configuration { get; }

        private bool isRunning = false;

        private DateTime nextRun;

        private bool isConfigured = false;

        public StatsCollectorTask(
            ILogger<StatsCollectorTask> _logger, IServiceProvider services, 
            IConfiguration configuration, SshConnectionInfoProvider sshConnectionInfoProvider)
        {
            logger = _logger;
            Services = services;
            Configuration = configuration;
            this.sshConnectionInfoProvider = sshConnectionInfoProvider;
            nextRun = DateTime.Now;
        }

        public void Dispose()
        {
            _timer?.Change(Timeout.Infinite, 0);
            _timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(tryDoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(60));

            logger.LogInformation("StatsCollectorTask is started.");

            return Task.CompletedTask;
        }

        private void tryDoWork(object? state)
        {
            try
            {
                if (isRunning) return;
                isRunning = true;
                doWork(state);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    //throw ex;
                }
                logger.LogError($"StatsCollectorTask {ex.Message}", ex);
            }
            isRunning = false;
        }

        private void doWork(object? state)
        {
            if (DateTime.Now > nextRun)
            {
                CollectStats();
                nextRun = DateTime.Now.AddMinutes(COLLECT_STATS_EVERY_MINUTES);
            }
        }

        private void CollectStats()
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var nodes = dbContext.ServerNodes.Where(sn => 
                    sn.ServerNodeState == Lib.Models.Enums.ServerNodeState.Available
                    || sn.ServerNodeState == Lib.Models.Enums.ServerNodeState.Occupied
                ).ToList();

                logger.LogInformation($"Collecting stats from {nodes.Count} nodes");

                foreach (var node in nodes)
                {
                    try
                    {
                        var sshConInfo = sshConnectionInfoProvider.GetConnectionInfo(node);
                        var statsWorkflow = new SystemStatsCollectorWorkflow(sshConInfo);
                        statsWorkflow.Run();
                        if (!statsWorkflow.IsSuccessfull)
                        {
                            logger.LogWarning($"Failed to cllect stats from node {node.Name} {node.HostAddress}");
                            continue;
                        }
                        node.CPU = statsWorkflow.CPU;
                        node.RAM = statsWorkflow.RAM;
                        node.HDD = statsWorkflow.HDD;
                        node.LastStatsUpdate = DateTime.Now;
                    }catch(Exception ex) {
                        if(DateTime.Now - node.LastStatsUpdate > TimeSpan.FromHours(1))
                        {
                            node.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
                        }
                        logger.LogWarning(ex, $"stats update error on node {node.Id}");
                    }
                    dbContext.SaveChanges();
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("StatsCollectorTask is stopped");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
