
using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Helper;
using GNS3aaS.Controller.Providers;
using GNS3aaS.Controller.Services;
using GNS3aaS.Controller.Services.Notifications;
using GNS3aaS.Controller.Tasks;
using GNS3aaS.Controller.Testing;
using GNS3aaS.Lib.Authentification;
using GNS3aaS.Lib.Logging;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Text;
using System.Text.Json.Serialization;

namespace GNS3aaS.Controller
{
    public class Program
    {
        private static ILogger _logger = StaticLoggerFactory.GetStaticLogger<Program>();

        public static void Main(string[] args)
        {
            var db = new GNS3aaSDbContext();

            _logger.LogInformation($"DB path = {GNS3aaSDbContext.DbPath}");

            var databaseAlreadyExists = db.Database.CanConnect();

            db.Database.Migrate();

            if ((!databaseAlreadyExists && Global.IsDevelopment) || Global.IsUnitTest)
            {
                Debug.WriteLine("Popuplating DB with test data");
                DbTestHelper.Populate(db, ConfigurationHelper.InitConfiguration());
            }
            if (databaseAlreadyExists)
            {
                _logger.LogInformation($"Created DB in {GNS3aaSDbContext.DbPath}");
            }

            db.Dispose();

            var app = CreateWebApplication(args);
            Global.App = app;
            app.Run();
        }

        public static WebApplication CreateWebApplication(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Configuration.AddEnvironmentVariables(prefix: "GNS3AAS_");

            builder.Services.AddControllers();
            builder.Services.AddDbContext<GNS3aaSDbContext>(ServiceLifetime.Transient);
            builder.Services.AddSingleton<SecretProvider>();
            builder.Services.AddSingleton<JwtTokenProvider>();
            builder.Services.AddSingleton<NotificationService>();
            builder.Services.AddSingleton<RateLimiterService>();
            builder.Services.AddSingleton<ScheduledShutdownTimeProvider>();

            if (!Global.IsDevelopment || !string.IsNullOrWhiteSpace(builder.Configuration["Development:RunServices"]))
            {
                builder.Services.AddSingleton<SshConnectionInfoProvider>();
                builder.Services.AddHostedService<SessionStarterTask>();
                builder.Services.AddHostedService<SessionStopperTask>();
                builder.Services.AddHostedService<ScheduledSessionStopperTask>();
                builder.Services.AddHostedService<StatsCollectorTask>();
                builder.Services.AddHostedService<CleanUpTask>();
                builder.Services.AddHostedService<MAASTask>();
            }
            else
            {
                _logger.LogWarning("Development Mode is active. Not starting important node handling tasks.");
                builder.Services.AddHostedService<TestingSessionHandlerTask>();
            }


            var MyAllowSpecificOrigins = "_AllowSpecificOrigins";

            var enableCors = !string.IsNullOrWhiteSpace(builder.Configuration["Origins"]);

            if (enableCors)
            {
                builder.Services.AddCors(options =>
                {
                    options.AddPolicy(MyAllowSpecificOrigins,
                                        policy =>
                                        {
                                            policy.WithOrigins(builder.Configuration["Origins"]!)
                                                        .AllowAnyOrigin()
                                                        .AllowAnyHeader()
                                                        .AllowAnyMethod();
                                        });
                });
            }
            else
            {
                _logger.LogWarning("Origins for CORS not configured");
            }


            builder.Services.AddControllers()
                .AddMvcOptions(options => options.Filters.Add(new AuthorizeFilter()))
                .AddJsonOptions(opts =>
                {
                    var enumConverter = new JsonStringEnumConverter();
                    opts.JsonSerializerOptions.Converters.Add(enumConverter);
                    opts.JsonSerializerOptions.MaxDepth = 64;
                    opts.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
                })
                ;

            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(option =>
            {
                option.SwaggerDoc("v1", new OpenApiInfo { Title = "PNMTD", Version = "v1" });
                option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter a valid token",
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "Bearer",
                });
                option.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type=ReferenceType.SecurityScheme,
                                Id="Bearer",
                            }
                        },
                        new string[]{ "" }
                    }
                });
            });

            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = builder.Configuration["Jwt:Issuer"],
                    ValidAudience = builder.Configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey
                        (Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"])),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                };
            });
            builder.Services.AddAuthorization(options =>
            {
                foreach (var r in Enum.GetValues(typeof(JwtUserRoles)).Cast<JwtUserRoles>().ToList())
                {
                    options.AddPolicy(r.ToString(), policy => policy.RequireClaim(r.ToString()));
                }
            });

            if (!string.IsNullOrWhiteSpace(builder.Configuration["Proxy"]))
            {
                _logger.LogInformation($"Using Proxy = {builder.Configuration["Proxy"]}");
                builder.Services.Configure<ForwardedHeadersOptions>(options =>
                {
                    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
                    options.RequireHeaderSymmetry = false;
                    options.ForwardLimit = null;
                    // Example format: ::ffff:172.20.10.20
                    if (builder.Configuration["Proxy"] == "all")
                    {
                        options.KnownNetworks.Add(new Microsoft.AspNetCore.HttpOverrides.IPNetwork(IPAddress.Parse("172.16.0.0"), 12));
                        options.KnownNetworks.Add(new Microsoft.AspNetCore.HttpOverrides.IPNetwork(IPAddress.Parse("192.168.0.0"), 16));
                        options.KnownNetworks.Add(new Microsoft.AspNetCore.HttpOverrides.IPNetwork(IPAddress.Parse("10.0.0.0"), 8));
                    }
                    else
                    {
                        options.KnownProxies.Add(IPAddress.Parse(builder.Configuration["Proxy"]));
                    }

                });
            }

            var logFolder = "";
            if (!Global.IsDevelopment && RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                logFolder = Path.Combine("logs");
                if (!Directory.Exists(logFolder))
                {
                    Directory.CreateDirectory(logFolder);
                }
            }

            builder.Logging.ClearProviders();
            builder.Logging.AddConsole();
            builder.Logging.AddDebug();
            builder.Logging.AddFile(Path.Combine(logFolder, "gns3aas.log"), fileSizeLimitBytes: 52430000, retainedFileCountLimit: 10);

            var app = builder.Build();
            if (enableCors)
            {
                app.UseCors();
            }
            app.UseOptions();

            StaticLoggerFactory.Initialize(app.Services.GetRequiredService<ILoggerFactory>());

            _logger = StaticLoggerFactory.GetStaticLogger<Program>();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                var tokenProvider = app.Services.GetService<JwtTokenProvider>();
                var db = app.Services.GetService<GNS3aaSDbContext>();
                var testUser = db.Users.First();
                _logger.LogInformation("User-Token: " + tokenProvider.GenerateTokenFor(testUser.Id, DbTestHelper.UserEmail, testUser.GroupName, JwtUserRoles.User, Global.TokenValidityInMinutesAdmin));
                _logger.LogInformation("Admin-Token: " + tokenProvider.GenerateTokenFor(testUser.Id, DbTestHelper.AdminEmail, testUser.GroupName, JwtUserRoles.Administrator, Global.TokenValidityInMinutesAdmin));
            }

            if (string.IsNullOrEmpty(builder.Configuration["Https"]) || builder.Configuration["Https"] != "false")
            {
                app.UseHttpsRedirection();
            }
            app.UseForwardedHeaders();
            app.UseAuthorization();
            if (enableCors)
            {
                app.MapControllers().RequireCors(MyAllowSpecificOrigins);
            }
            else
            {
                app.MapControllers();
            }

            return app;
        }
    }
}
