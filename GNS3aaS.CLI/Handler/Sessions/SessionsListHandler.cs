﻿using GNS3aaS.CLI.Services;
using GNS3aaS.Lib.Helper;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GNS3aaS.CLI.Handler.Sessions
{
    internal class SessionsListHandler : BaseHandler, IHandler
    {
        public const string SessionLineFormat = "| {0,36} | {1,9} | {2,4} | {3,16} | {4,16} |{5,7} | {6,9} | {7,19} | {8,19} | {9,30} | {10}";
        public SessionsListHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("list", "get all sessions");

            var showDetailsOption = new Option<bool>(new string[] { "-d", "--details" }, "show details");

            command.AddOption(showDetailsOption);

            command.SetHandler(async (showDetails) =>
            {
                await handle(showDetails);
            }, showDetailsOption);

            return command;
        }

        public static void PrintHeader()
        {
            Console.WriteLine(string.Format(SessionLineFormat,
                    "Id",
                    "State",
                    "Keep",
                    "Node",
                    "Arc.-Node",
                    "GNS3",
                    "Archive",
                    "Started",
                    "Updated",
                    "Email",
                    "Message"
                    ));
        }

        private async Task handle(bool showDetails)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var sessions = await client.GetSessions();

                if (sessions == null) return;

                if(!showDetails) { PrintHeader(); }
                

                foreach (var session in sessions)
                {
                    if (showDetails)
                    {
                        foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(session))
                        {
                            string name = descriptor.Name;
                            object? value = descriptor.GetValue(session);
                            if (value != null && value is string) value = ((string)value).Replace("\n", " ");

                            Console.WriteLine("{0} = {1}", name, value);
                        }
                        Console.WriteLine(new string('-', 60));
                    }
                    else
                    {
                        Console.WriteLine(string.Format(SessionLineFormat,
                        session.Id,
                        session.State,
                        session.KeepSession ? "1" : "0",
                        session.ActiveNodeName,
                        session.ArchiveLocationNodeName,
                        session.GNS3Version,
                        FormatHelper.FormatBytes(session.ArchiveSize),
                        session.Started,
                        session.Updated,
                        session.Email,
                        session.Message
                        ));
                    }
                }
            }
        }
    }
}
