﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    internal class DeleteServerNodeHandler : BaseHandler, IHandler
    {
        public DeleteServerNodeHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("delete", "delete server node");

            var nodeIdArgs = new Argument<string>("nodeId");

            command.AddArgument(nodeIdArgs);

            command.SetHandler(async (nodeId) =>
            {
                await handle(nodeId);
            }, nodeIdArgs);

            return command;
        }

        private async Task handle(string nodeId)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.DeleteServerNode(nodeId);
                if (response.Success)
                {
                    Console.WriteLine($"node {nodeId} deleted");
                }
                else
                {
                    Console.WriteLine($"node {nodeId} delete FAILED");
                }
            }
        }
    }
}
