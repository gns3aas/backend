﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Models.Pocos
{
    public class ApplianceImagePoco
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string TemplateData { get; set; }

        public string DownloadUrl { get; set; }

        public string RemoteUnpackPath { get; set; }
    }
}
