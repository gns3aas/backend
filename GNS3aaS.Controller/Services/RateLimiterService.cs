﻿using System.Net;

namespace GNS3aaS.Controller.Services
{
    public class RateLimiterService
    {
        public const int MAX_NUM_OF_BAD_REQUESTS = 100;
        public const int TIMEFRAME_MINUTES = 15;

        public const int WAIT_TIME_BETWEEN_EMAILS = 5;
        public const int MAX_NUMBER_OF_EMAILS_PER_TIMEFRAME = 4;

        private Dictionary<string, List<BadRequestEvent>> ipToBadRequestEvents = new Dictionary<string, List<BadRequestEvent>>();
        private Dictionary<string, LoginEmailSentEvent> lastEmailSend = new Dictionary<string, LoginEmailSentEvent>();

        public RateLimiterService() { }


        public void LogBadRequest(string ipAddress) 
        { 
            if(!ipToBadRequestEvents.ContainsKey(ipAddress))
            {
                ipToBadRequestEvents.Add(ipAddress, new List<BadRequestEvent>());
            }
            ipToBadRequestEvents[ipAddress].Add(new BadRequestEvent(ipAddress));
            if(ipToBadRequestEvents[ipAddress].Count > (MAX_NUM_OF_BAD_REQUESTS * 2))
            {
                ipToBadRequestEvents[ipAddress].RemoveAt(0);
            }
        }

        public bool TooManyBadRequest(string ipAddress)
        {
            if (!ipToBadRequestEvents.ContainsKey(ipAddress)) return false;
            ipToBadRequestEvents[ipAddress].RemoveAll(i => DateTime.Now - i.Created > TimeSpan.FromMinutes(TIMEFRAME_MINUTES));
            if (ipToBadRequestEvents[ipAddress].Count > MAX_NUM_OF_BAD_REQUESTS) return true;
            return false;
        }

        public bool TooManyEmailRequest(string email)
        {
            if (!lastEmailSend.ContainsKey(email)) return false;
            if (DateTime.Now - lastEmailSend[email].LastSent > TimeSpan.FromMinutes(WAIT_TIME_BETWEEN_EMAILS)) lastEmailSend[email].ResetCounter();
            return lastEmailSend[email].Counter > MAX_NUMBER_OF_EMAILS_PER_TIMEFRAME;
        }

        public void LogSentEmailRequest(string email)
        {
            if (!lastEmailSend.ContainsKey(email))
            {
                lastEmailSend[email] = new LoginEmailSentEvent(email);
            }
            lastEmailSend[email].AddEvent();
        }
        
    }

    class BadRequestEvent
    {
        public readonly string IpAddress;
        public readonly DateTime Created;

        public BadRequestEvent(string ipAddress) {
            this.IpAddress = ipAddress;
            this.Created = DateTime.Now;
        }
    }

    class LoginEmailSentEvent
    {
        public LoginEmailSentEvent(string email)
        {
            Email = email;
            LastSent = DateTime.Now;
        }

        public void AddEvent()
        {
            Counter++;
            LastSent = DateTime.Now;
        }

        public void ResetCounter()
        {
            Counter = 0;
        }

        public string Email { get; }

        public DateTime LastSent { get; private set; }
        public int Counter { get; private set; }
    }
}
