﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Services;
using GNS3aaS.MAAS;
using GNS3aaS.MAAS.Models;
using GNS3aaS.Theater;
using GNS3aaS.Theater.Playbooks;
using GNS3aaS.Theater.Workflows;
using System.Diagnostics;

namespace GNS3aaS.Controller.Tasks
{
    public class MAASTask : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly ILogger<MAASTask> logger;
        private readonly SshConnectionInfoProvider sshConnectionInfoProvider;

        public const string CONFIG_MAAS_KEY = "MAAS:Key";
        public const string CONFIG_MAAS_URL = "MAAS:Url";

        public IServiceProvider Services { get; }
        public IConfiguration Configuration { get; }

        private bool isRunning = false;
        private MAASApiClient mAASApiClient;

        public MAASTask(ILogger<MAASTask> _logger, IServiceProvider services, IConfiguration configuration, SshConnectionInfoProvider sshConnectionInfoProvider)
        {
            logger = _logger;
            Services = services;
            Configuration = configuration;
            this.sshConnectionInfoProvider = sshConnectionInfoProvider;
        }

        public void Dispose()
        {
            _timer?.Change(Timeout.Infinite, 0);
            _timer?.Dispose();
        }

        private bool init()
        {
            if (Configuration[CONFIG_MAAS_KEY] == null || Configuration[CONFIG_MAAS_URL] == null) return false;
            var maasKeyConfig = Configuration[CONFIG_MAAS_KEY]!.Split(":");
            if (maasKeyConfig.Length != 3) return false;
            mAASApiClient = new MAASApiClient(Configuration[CONFIG_MAAS_URL]!, maasKeyConfig[0], maasKeyConfig[1], maasKeyConfig[2]);
            return true;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            if(!init())
            {
                logger.LogError($"Missing or malformed {CONFIG_MAAS_URL} {CONFIG_MAAS_KEY}");
                return Task.CompletedTask;
            }
            _timer = new Timer(tryDoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(60));

            logger.LogInformation("MAASTask is started.");

            return Task.CompletedTask;
        }

        private void tryDoWork(object? state)
        {
            try
            {
                if (isRunning) return;
                isRunning = true;
                doWork(state);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    throw ex;
                }
                logger.LogError($"MAASTask {ex.Message}", ex);
            }
            isRunning = false;
        }

        private void doWork(object? state)
        {
            AquireSystemIds();
            CheckStateMaasState();
            TriggerDeployment();
            ReleaseNodes();
            CheckStateGns3InstallState();
        }

        private void CheckStateGns3InstallState()
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var nodes = dbContext.ServerNodes.Where(f =>
                    (f.ServerNodeState == Lib.Models.Enums.ServerNodeState.InstallingApps)
                    && f.MaasSystemId != null
                ).ToList();

                if (!nodes.Any()) return;

                logger.LogInformation($"Found {nodes.Count} that are installing apps");

                foreach (var node in nodes)
                {
                    try
                    {
                        var playbookRunner = new PlaybookRunner(sshConnectionInfoProvider.GetConnectionInfo(node), new VerifyMachineInstallStatePlaybook());
                        playbookRunner.Run();
                        if(playbookRunner.IsSuccess())
                        {
                            node.ServerNodeState = Lib.Models.Enums.ServerNodeState.Reserved;
                        }
                    }
                    catch(Exception e)
                    {
                        logger.LogWarning(e, $"VerifyMachineInstallStatePlaybook for node {node.Id} {node.Name} failed");
                    }
                }
                dbContext.SaveChanges();
            }
        }

        private void CheckStateMaasState()
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var nodes = dbContext.ServerNodes.Where(f =>
                    (f.ServerNodeState == Lib.Models.Enums.ServerNodeState.Releasing 
                    || f.ServerNodeState == Lib.Models.Enums.ServerNodeState.InstallingBase)
                    && f.MaasSystemId != null
                ).ToList();

                if (!nodes.Any()) return;

                logger.LogInformation($"Found {nodes.Count} nodes that are releasing or installing");

                var machinesTask = mAASApiClient.GetMachines();
                machinesTask.Wait();
                var machines = machinesTask.Result;

                foreach (var node in nodes)
                {
                    var machine = machines.SingleOrDefault(m => m.HostName == node.Name);
                    if (machine == null)
                    {
                        logger.LogWarning($"Node {node.Name} not found in MAAS");
                        continue;
                    }
                    node.MaasState = machine.Status;
                    if(node.ServerNodeState == Lib.Models.Enums.ServerNodeState.InstallingBase && node.MaasState == MaasMachine.STATUS_DEPLOYED)
                    {
                        node.ServerNodeState = Lib.Models.Enums.ServerNodeState.InstallingApps;
                    }
                    else if (node.ServerNodeState == Lib.Models.Enums.ServerNodeState.Releasing && node.MaasState == MaasMachine.STATUS_READY)
                    {
                        node.ServerNodeState = Lib.Models.Enums.ServerNodeState.New;
                    }
                }
                dbContext.SaveChanges();
            }
        }

        private void ReleaseNodes()
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var nodes = dbContext.ServerNodes.Where(f =>
                    (f.ServerNodeState == Lib.Models.Enums.ServerNodeState.ReleaseRequested)
                    && f.MaasSystemId != null
                ).ToList();

                if (!nodes.Any()) return;

                logger.LogInformation($"Found {nodes.Count} nodes to release");

                foreach (var node in nodes)
                {
                    HandleSingleRelease(dbContext, node);
                    
                }
                dbContext.SaveChanges();
            }
        }

        private void AquireSystemIds()
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var nodes = dbContext.ServerNodes.Where(f =>
                    (f.ServerNodeState == Lib.Models.Enums.ServerNodeState.New ||
                    f.ServerNodeState == Lib.Models.Enums.ServerNodeState.ReleaseRequested ||
                    f.ServerNodeState == Lib.Models.Enums.ServerNodeState.InstallRequested)
                    && f.MaasSystemId == null
                ).ToList();

                if (!nodes.Any()) return;

                logger.LogInformation($"Found {nodes.Count} nodes with no system id");

                var machinesTask = mAASApiClient.GetMachines();
                machinesTask.Wait();
                var machines = machinesTask.Result;

                foreach (var node in nodes)
                {
                    var machine = machines.SingleOrDefault(m => m.HostName == node.Name);
                    if (machine == null)
                    {
                        node.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
                        continue;
                    }
                    node.MaasSystemId = machine.SystemId;
                    node.MaasState = machine.Status;

                    if (machine!.Status == MaasMachine.STATUS_DEPLOYED && node.ServerNodeState != Lib.Models.Enums.ServerNodeState.ReleaseRequested)
                    {
                        node.ServerNodeState = Lib.Models.Enums.ServerNodeState.Reserved;
                    }
                    if (machine.Status == MaasMachine.STATUS_READY && node.ServerNodeState != Lib.Models.Enums.ServerNodeState.InstallRequested)
                    {
                        node.ServerNodeState = Lib.Models.Enums.ServerNodeState.New;
                    }
                }
                dbContext.SaveChanges();
            }
        }

        private void TriggerDeployment()
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var nodes = dbContext.ServerNodes.Where(f => f.ServerNodeState == Lib.Models.Enums.ServerNodeState.InstallRequested && f.MaasSystemId != null)
                    .ToList().Where(sn => !string.IsNullOrWhiteSpace(sn.MaasSystemId)).ToList();

                if (!nodes.Any()) return;

                logger.LogInformation($"Found {nodes.Count} nodes ready to deploy");

                foreach (var node in nodes)
                {
                    HandleSingleDeployment(dbContext, node);
                }
            }
        }

        private void HandleSingleRelease(GNS3aaSDbContext dbContext, ServerNodeEntity node)
        {
            node.ServerNodeState = Lib.Models.Enums.ServerNodeState.Releasing;
            dbContext.SaveChanges();

            var taskRelease = mAASApiClient.ReleaseMachine(node.MaasSystemId!);
            taskRelease.Wait();
            if (taskRelease.Result == null)
            {
                node.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
                dbContext.SaveChanges();
                logger.LogError($"MAAS release failed for {node.Name} systemId {node.MaasSystemId}");
                return;
            }
            logger.LogInformation($"Released node {node.Id}");
        }

        private void HandleSingleDeployment(GNS3aaSDbContext dbContext, ServerNodeEntity node)
        {
            node.ServerNodeState = Lib.Models.Enums.ServerNodeState.InstallingBase;
            dbContext.SaveChanges();

            var taskAllocate = mAASApiClient.AllocateMachine(node.Name, node.MaasSystemId!);
            taskAllocate.Wait();
            if(taskAllocate.Result == null)
            {
                node.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
                dbContext.SaveChanges();
                logger.LogError($"MAAS allocation failed for {node.Name} systemId {node.MaasSystemId}");
                return;
            }

            var task = mAASApiClient.DeployMachine(node.MaasSystemId!, CloudInitDataProvider.GetCloudInit());
            task.Wait();
            if(task.Result == null )
            {
                node.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
                dbContext.SaveChanges();
                logger.LogError($"MAAS deployment failed for {node.Name} systemId {node.MaasSystemId}");
                return;
            }
            logger.LogInformation($"Deploying node {node.Id}");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("MAASTask is stopped");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
