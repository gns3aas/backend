﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    internal class ServerNodesHandler : BaseHandler, IHandler
    {
        public ServerNodesHandler(ConfigStorage configStorage) : base(configStorage)
        {

        }

        public Command GetCommand()
        {
            var subCommands = new Command("node");

            subCommands.Add(new ServerNodesListHandler(configStorage).GetCommand());
            subCommands.Add(new ServerNodesResetHandler(configStorage).GetCommand());
            subCommands.Add(new ServerNodesReserveHandler(configStorage).GetCommand());
            subCommands.Add(new ServerNodesDeployHandler(configStorage).GetCommand());
            subCommands.Add(new ServerNodesReleaseHandler(configStorage).GetCommand());
            subCommands.Add(new AddServerNodeHandler(configStorage).GetCommand());
            subCommands.Add(new DeleteServerNodeHandler(configStorage).GetCommand());
            subCommands.Add(new ServerNodesPublishHandler(configStorage).GetCommand());
            subCommands.Add(new ServerNodesEditParameters(configStorage).GetCommand());

            return subCommands;
        }
    }
}
