﻿using GNS3aaS.Controller.Services.ServerNodeLocking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Tests.Services
{
    [TestClass]
    public class ServerNodeLocksTests
    {

        [TestMethod]
        public void T01_ServerLock()
        {
            var serverLock = new ServerNodeLocks();

            Guid assignedNodeId = Guid.NewGuid();
            Guid sessionId = Guid.NewGuid();
            Guid lastNodeId = Guid.NewGuid();

            Assert.IsFalse(serverLock.IsNodeLocked(assignedNodeId));
            Assert.IsFalse(serverLock.IsNodeLocked(sessionId));
            Assert.IsFalse(serverLock.IsNodeLocked(lastNodeId));

            serverLock.Lock(sessionId, assignedNodeId, lastNodeId);

            Assert.IsFalse(serverLock.IsNodeLocked(sessionId));
            Assert.IsTrue(serverLock.IsNodeLocked(assignedNodeId));
            Assert.IsTrue(serverLock.IsNodeLocked(lastNodeId));

            serverLock.Release(sessionId);

            Assert.IsFalse(serverLock.IsNodeLocked(assignedNodeId));
            Assert.IsFalse(serverLock.IsNodeLocked(sessionId));
            Assert.IsFalse(serverLock.IsNodeLocked(lastNodeId));
        }

        [TestMethod]
        public void T02_ServerLock()
        {
            var serverLock = new ServerNodeLocks();

            Guid assignedNodeId = Guid.NewGuid();
            Guid sessionId = Guid.NewGuid();

            Assert.IsFalse(serverLock.IsNodeLocked(assignedNodeId));
            Assert.IsFalse(serverLock.IsNodeLocked(sessionId));

            serverLock.Lock(sessionId, assignedNodeId);

            Assert.IsFalse(serverLock.IsNodeLocked(sessionId));
            Assert.IsTrue(serverLock.IsNodeLocked(assignedNodeId));

            serverLock.Release(sessionId);

            Assert.IsFalse(serverLock.IsNodeLocked(assignedNodeId));
            Assert.IsFalse(serverLock.IsNodeLocked(sessionId));
        }

    }
}
