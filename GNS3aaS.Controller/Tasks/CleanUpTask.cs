﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Services;
using GNS3aaS.Theater.Workflows;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace GNS3aaS.Controller.Tasks
{
    public class CleanUpTask : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly ILogger<CleanUpTask> logger;
        private readonly SshConnectionInfoProvider sshConnectionInfoProvider;

        public const int CLEANUP_EVERY_MINUTES = 10;

        public IServiceProvider Services { get; }
        public IConfiguration Configuration { get; }

        private bool isRunning = false;

        private DateTime nextRun;

        public CleanUpTask(
            ILogger<CleanUpTask> _logger, IServiceProvider services,
            IConfiguration configuration, SshConnectionInfoProvider sshConnectionInfoProvider)
        {
            logger = _logger;
            Services = services;
            Configuration = configuration;
            this.sshConnectionInfoProvider = sshConnectionInfoProvider;
            nextRun = DateTime.Now;
        }

        public void Dispose()
        {
            _timer?.Change(Timeout.Infinite, 0);
            _timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(tryDoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(60));

            logger.LogInformation("CleanUpTask is started.");

            return Task.CompletedTask;
        }

        private void tryDoWork(object? state)
        {
            try
            {
                if (isRunning) return;
                isRunning = true;
                doWork(state);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    throw ex;
                }
                logger.LogError($"CleanUpTask {ex.Message}", ex);
            }
            isRunning = false;
        }

        private void doWork(object? state)
        {
            if (DateTime.Now > nextRun)
            {
                CleanUpNodes();
                nextRun = DateTime.Now.AddMinutes(CLEANUP_EVERY_MINUTES);
            }
        }

        private void CleanUpNodes()
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var nodes = dbContext.ServerNodes.Where(sn => sn.ServerNodeState == Lib.Models.Enums.ServerNodeState.Available).ToList();

                if (nodes.Count == 0) return;

                var sessionsIds = dbContext.Sessions.Select(session => session.Id.ToString()).ToList();

                logger.LogInformation($"Cleaning up {nodes.Count} nodes");

                int totalNumOfArchives = 0;
                int totalNumOfFilesDeleted = 0;

                foreach (var node in nodes)
                {
                    try
                    {
                        CleanUpNode(dbContext, sessionsIds, node, out var numOfArchives, out var numOfFilesDeleted);
                        totalNumOfArchives += numOfArchives;
                        totalNumOfFilesDeleted += numOfFilesDeleted;
                        if(numOfFilesDeleted > 0)
                            logger.LogInformation($"Cleaned up {node.Id} {node.Name}. Deleted {totalNumOfFilesDeleted} of {totalNumOfArchives} archives");
                    }
                    catch(Exception e)
                    {
                        logger.LogError(e, $"Failed to clean node {node.Id}");
                    }
                }
                logger.LogInformation($"Found {totalNumOfArchives} and deleted {totalNumOfFilesDeleted} archives");
            }
        }

        private void CleanUpNode(GNS3aaSDbContext dbContext, List<string> sessionsIds, ServerNodeEntity node, out int numOfArchives, out int numOfFilesDeleted)
        {
            var sshConInfo = sshConnectionInfoProvider.GetConnectionInfo(node);
            var cleanUpWorkflow = new CleanUpArchiveWorkflow(sshConInfo, sessionsIds);
            numOfArchives = 0;
            numOfFilesDeleted = 0;
            cleanUpWorkflow.Run();
            if (!cleanUpWorkflow.IsSuccessfull)
            {
                logger.LogWarning($"Cleanup task failed on node {node.Id}");
            }
            numOfArchives = cleanUpWorkflow.NumOfArchivesFound;
            numOfFilesDeleted = cleanUpWorkflow.NumOfArchivesDeleted;
            if (cleanUpWorkflow.NumOfArchivesDeleted > 0)
            {
                logger.LogInformation("Deleted archives with sessionIds: " + string.Join(",", cleanUpWorkflow.DeletedArchivesIds));
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("CleanUpTask is stopped");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
