﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    internal class CheckArchiveFileAvailabilityAndMd5SumPlaybook : IPlaybook
    {
        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public CheckArchiveFileAvailabilityAndMd5SumPlaybook(string sessionId, SshConnectionInfo host)
        {
            var archiveFile = $"{RemoteCopyUserProjectPlaybook.ARCHIVE_PATH}{sessionId}.tar.gz";
            orderList.Add(ExecutionOrderHelper.Md5SumOfFile(archiveFile));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
