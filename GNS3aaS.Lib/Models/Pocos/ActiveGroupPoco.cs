﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Models.Pocos
{
    public class ActiveGroupPoco
    {

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string ResponsiblePersonEmail { get; set; }

        public DateTime Created { get; set; }

    }
}
