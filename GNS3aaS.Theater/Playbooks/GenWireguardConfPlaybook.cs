﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class GenWireguardConfPlaybook : IPlaybook
    {
        public const string SCRIPT_PATH = "/opt/gns3aas/gen-wireguard.sh";
        public const string OPENVPN_FILE_PATH = "/etc/wireguard/wg0.conf";
        public const string WIREGUARD_CLIENT_CONF = "wireguardconf";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public GenWireguardConfPlaybook(string publicHostAddress)
        {
            orderList.Add(ExecutionOrderHelper.StopService("wg-quick@wg0", false));
            orderList.Add(ExecutionOrderHelper.CreateFolder("/opt/gns3aas"));
            orderList.Add(ExecutionOrderHelper.UploadFile(SCRIPT_PATH));
            orderList.Add(ExecutionOrderHelper.FixLineEndings(SCRIPT_PATH));
            orderList.Add(ExecutionOrderHelper.ExecuteBashScript(SCRIPT_PATH, WIREGUARD_CLIENT_CONF, publicHostAddress));
            orderList.Add(ExecutionOrderHelper.StartService("wg-quick@wg0"));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
