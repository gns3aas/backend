﻿using GNS3aaS.CLI.Services;
using GNS3aaS.Lib.Models.Response;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    internal class ServerNodesResetHandler : ServerNodesAbstractActionHandler
    {
        public ServerNodesResetHandler(ConfigStorage configStorage) : base("reset", "reset a node", configStorage)
        {
        }

        public override Task<DefaultResponse> DoApiRequest(string nodeId, GNS3aaSApiClient client)
        {
            return client.ResetNode(nodeId);
        }
    }
}