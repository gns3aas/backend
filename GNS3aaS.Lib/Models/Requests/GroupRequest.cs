﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Models.Requests
{
    public class GroupRequest
    {
        public string Name { get; set; }

        public string ResponsiblePersonEmail { get; set; }

    }
}
