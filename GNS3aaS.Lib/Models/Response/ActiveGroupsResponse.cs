﻿using GNS3aaS.Lib.Models.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Models.Response
{
    public class ActiveGroupsResponse
    {
        public List<string> AvailableGroups { get; set; }

        public List<string> ActiveGroupNames { get; set; }

        public List<ActiveGroupPoco> ActiveGroups { get; set; }


    }
}
