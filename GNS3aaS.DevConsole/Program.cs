﻿using GNS3aaS.Lib.Logging;
using GNS3aaS.MAAS;
using GNS3aaS.Theater;
using GNS3aaS.Theater.Playbooks;
using GNS3aaS.Theater.Workflows;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Renci.SshNet;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.DevConsole
{
    internal class Program
    {
        private static IConfigurationRoot config;
        private static ILogger<Program> logger;


        static void Main(string[] args)
        {
            config = new ConfigurationBuilder()
               .AddUserSecrets(Assembly.GetCallingAssembly(), true)
               .Build();

            StaticLoggerFactory.GlobalLogLevel = Microsoft.Extensions.Logging.LogLevel.Trace;

            logger = StaticLoggerFactory.GetStaticLogger<Program>();
            logger.LogDebug("Test");

            string? name = null;

            logger.LogDebug($"Name: {name}");

            logger.LogDebug("Done");

            /*var server51 = new SshConnectionInfo("10.0.34.160", "user", config["SSH:Key"]);

            CreateNewSessionNoArchiveWorkflow.NewWireguardKeys(server51, "cloud.tbz.ch", out var wireguardConfs);

            Console.WriteLine(wireguardConfs);*/

            //MaasStuff();

        }

        static void MaasStuff()
        {
            var parts = config["MAAS:Key"].Split(":");

            var api = new MAASApiClient("http://10.1.40.33:5240/", parts[0], parts[1], parts[2]);

            //var cloudHfSystemId = "8bk7xb";
            //var machineName = "Cloud-HF-34";

            var cloudHfSystemId = "bdahxr";
            var machineName = "Cloud-HF-32";

            var t = api.GetMachines();
            t.Wait();

            Console.WriteLine(t.Result);

            //File.WriteAllText("machines.json", t.Result);

            var taskAllocate = api.AllocateMachine(machineName, cloudHfSystemId);
            taskAllocate.Wait();

            var task = api.DeployMachine(cloudHfSystemId, CloudInitDataProvider.GetCloudInit());
            task.Wait();
            Console.WriteLine(task.Result);

            //var taskRelease = api.ReleaseMachine(cloudHfSystemId);
            //taskRelease.Wait();
            //Console.WriteLine(taskRelease.Result);

            //

            //Console.WriteLine();
        }


        static void WorkflowStuff()
        {
            var server51 = new SshConnectionInfo("10.0.34.151", "user", config["SSH:Key"]);
            var server52 = server51.CopyWithNewHost("10.0.34.152");

            //RestoreSessionWorkflow(server51, server52, "user58");

            // ArchiveSession(server52, "user58");
        }

        static void ArchiveSession(SshConnectionInfo onHost, string sessionId)
        {
            var archiveSessionWorkflow = new StopAndArchiveSessionWorkflow(sessionId, onHost);

            archiveSessionWorkflow.Run();

            Console.WriteLine($"IsSuccess {archiveSessionWorkflow.IsSuccessfull} Message: {archiveSessionWorkflow.Message}");
        }

        static void RestoreSessionWorkflow(SshConnectionInfo fromHost, SshConnectionInfo toHost, string sessionId)
        {
            var restoreSession = new RestoreSessionWorkflow(sessionId, "", "", "", new PublicHostConnectionInfo(), toHost, fromHost);

            restoreSession.Run();

            Console.WriteLine($"IsSuccess {restoreSession.IsSuccessfull} Message: {restoreSession.Message}");

            File.WriteAllText($"{toHost.Host}.ovpn", restoreSession.OpenVPNClientConfigFile);
        }

        static void NewSessionWorkflow(SshConnectionInfo server)
        {
            var createNewSession = new CreateNewSessionNoArchiveWorkflow(server, new PublicHostConnectionInfo());

            createNewSession.Run();

            Console.WriteLine($"IsSuccess {createNewSession.IsSuccessfull} Message: {createNewSession.Message}");

            File.WriteAllText($"{server.Host}.ovpn", createNewSession.OpenVPNClientConfigFile);
        }
    }
}
