﻿using System.Security.Cryptography;
using System.Text;

namespace GNS3aaS.Controller.Providers
{
    public class SecretProvider
    {
        private readonly IConfiguration configuration;

        public SecretProvider(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public string EmailHashSecret()
        {
            return configuration["EmailHashSecret"];
        }

        public string HashEmail(string email)
        {
            if(EmailHashSecret().Length < 64)
            {
                throw new InvalidProgramException("EmailHashSecret must be at least 64 chars");
            }
            using (SHA256 mySHA256 = SHA256.Create())
            {
                var hash = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(EmailHashSecret() + email));
                return Convert.ToHexString(hash);
            }
        }
    }
}
