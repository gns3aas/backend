﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    internal class ForceIdleSessionHandler : BaseHandler, IHandler
    {
        public ForceIdleSessionHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("force-idle", $"force state idle on session (dangerous!)");

            var sessionIdArg = new Argument<string>("sessionId");

            command.AddArgument(sessionIdArg);

            command.SetHandler(async (sessionId) =>
            {
                await handle(sessionId);
            }, sessionIdArg);
            return command;
        }

        private async Task handle(string sessionId)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.ForceIdleSession(sessionId);
                if (response != null && response.Success)
                {
                    Console.WriteLine($"session forced to state idle");
                }
                else
                {
                    Console.WriteLine($"session forced to state idle FAILED");
                }
            }
        }
    }
}
