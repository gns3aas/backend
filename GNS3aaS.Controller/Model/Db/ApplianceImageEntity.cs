﻿using System.ComponentModel.DataAnnotations;

namespace GNS3aaS.Controller.Model.Db
{
    public class ApplianceImageEntity
    {

        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string TemplateData { get; set; }

        public string DownloadUrl { get; set; }

        public string RemoteUnpackPath { get; set; }

    }
}
