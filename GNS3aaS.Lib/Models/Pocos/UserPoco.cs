﻿namespace GNS3aaS.Lib.Models.Pocos
{
    public class UserPoco
    {
        public Guid Id { get; set; }

        public required string EMailHash { get; set; }

        public string GroupName { get; set; }

        public string? JwtToken { get; set; }

        public DateTime? JwtTokenValidUntil { get; set; }

    }
}
