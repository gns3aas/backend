#!/bin/bash

echo "Disable cloudinit netplan"

echo "network: {config: disabled}" > /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg

ETH=`ip link | awk -F: '$0 !~ "lo|vir|wl|tap|br|^[^0-9]"{print $2;getline}'`

ETH=`echo $ETH | sed 's/ *$//g'`

echo "Adding ifupdown configuration"

cat <<EOFMOTD > /etc/network/interfaces.d/default
auto $ETH
iface $ETH inet dhcp

auto enp5s0
iface enp5s0 inet dhcp

auto br0
iface br0 inet static
address 192.168.23.1
netmask 255.255.255.0
pre-up brctl addbr br0

EOFMOTD


echo "Disable iptables for bridge"
# sysctl -w net.bridge.bridge-nf-call-iptables=0
# This is not really working
cat <<EOFCTL > /etc/sysctl.d/10-no-bridge-nf-call.conf
# So that Client To Client Communication works
net.bridge.bridge-nf-call-iptables=0

EOFCTL

echo "...fix ufw too"

cat <<EOFUFW >> /etc/ufw/sysctl.conf

net/bridge/bridge-nf-call-ip6tables = 0
net/bridge/bridge-nf-call-iptables = 0
net/bridge/bridge-nf-call-arptables = 0

EOFUFW

echo "...add it to sysctl.conf"

cat <<EOFSYS >> /etc/sysctl.conf
# So that Client To Client Communication works
net.bridge.bridge-nf-call-iptables=0
net.bridge.bridge-nf-call-ip6tables=0

EOFSYS

echo "...add service to reload sysctl after boot"

# This actually works
cat <<EOFCTL > /opt/sysctl-reload.service
[Unit]
Description=sysctl reload
Requires=multi-user.target
After=multi-user.target

[Service]
Type=oneshot
ExecStart=/usr/sbin/sysctl -p

[Install]
WantedBy=multi-user.target

EOFCTL

systemctl enable /opt/sysctl-reload.service

echo "Disable netplan"
rm -rf /etc/netplan/*

HOSTNAME=`hostname`

MY_EXTERNAL_HOSTNAME="cloud.tbz.ch"

arrHOSTNAME=(${HOSTNAME//-/ })

echo "My hostname is $HOSTNAME"

cat <<EOFDHCP > /etc/dhcp/dhcpd.conf
option domain-name "$HOSTNAME.local";

default-lease-time 600;
max-lease-time 7200;

subnet 192.168.23.0 netmask 255.255.255.0 {
 range 192.168.23.129 192.168.23.200;
 option domain-name-servers 8.8.8.8;
 option domain-name "$HOSTNAME.local";
}
EOFDHCP

sed -i 's/INTERFACESv4=""/INTERFACESv4="br0"/g' /etc/default/isc-dhcp-server

echo "Done setting up dhcpd"

MY_IP_ADDR=`ip addr show $ETH | grep -o "inet [0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"`

MY_IP_ADDR=(${MY_IP_ADDR[@]})

echo "My IP is $MY_IP_ADDR"

UUID=$(uuid)

echo "Update motd"

cat <<EOFMOTD > /etc/update-motd.d/70-gns3lab
#!/bin/sh
echo "GNS3 Lab Instance by Philipp Albrecht <philipp@uisa.ch>"
echo "_______________________________________________________________________________________________"
echo ""
EOFMOTD
chmod 755 /etc/update-motd.d/70-gns3lab


touch /opt/cloudinitinstall/networking.installed
