﻿using System.Diagnostics;

namespace GNS3aaS.Controller
{
    public static class Global
    {
        internal static WebApplication App;


        public static readonly int TokenValidityInMinutes = 3 * 30;
        public static readonly int TokenValidityInMinutesAdmin = 30 * 24 * 60;
        public static readonly int TokenValidityInMinutesSuperuser = 60 * 24;

        public static bool IsDevelopment
        {
            get
            {
                return Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            }
        }

        public static bool IsUnitTest
        {
            get
            {
                return Environment.GetEnvironmentVariable("UNITTEST_ENVIRONMENT") == "Enabled";
            }
        }

    }
}
