﻿using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater
{
    public static class ExecutionOrderHelper
    {
        public static Func<string, bool> CompareStringFunc(string shouldBe)
        {
            return (outputResult) =>
            {
                return outputResult.Trim() == shouldBe.Trim();
            };
        }

        public static Func<string, bool> CompareStringListFunc(params string[] canBe)
        {
            return (outputResult) =>
            {
                return canBe.Contains(outputResult.Trim());
            };
        }

        public static FullExecutionOrder UploadFile(string fileName)
        {
            return new FullExecutionOrder()
                .AddOrder(new FileUploadOrder(fileName))
                .AddPostCheck(new CommandExectionOrder($"[[ -f \"{fileName}\" ]] && echo 1 || echo 0", CompareStringFunc("1")));
        }

        public static FullExecutionOrder DownloadFile(string fileName)
        {
            return new FullExecutionOrder()
                .AddPreCheck(new CommandExectionOrder($"[[ -f \"{fileName}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
                .AddOrder(new FileDownloadOrder(fileName));
                
        }

        public static FullExecutionOrder StopService(string serviceName, bool verifyExitStatus = true, bool doPostCheck = true)
        {
            var executionOrder = new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"sudo systemctl stop {serviceName}", verifyExitStatus: verifyExitStatus));
            if(doPostCheck)
            {
                executionOrder.AddPostCheck(new CommandExectionOrder($"sudo systemctl is-active {serviceName}", CompareStringListFunc("inactive", "activating", "failed"), false));
            }
            return executionOrder;
        }

        public static FullExecutionOrder StartService(string serviceName)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"sudo systemctl start {serviceName}"))
                .AddPostCheck(new CommandExectionOrder($"sudo systemctl is-active {serviceName}", CompareStringListFunc("active", "activating"), false));
        }

        public static FullExecutionOrder CreateFolder(string absoluteFolderPath)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"sudo mkdir -p {absoluteFolderPath}"))
                .AddPostCheck(new CommandExectionOrder($"[ -d \"{absoluteFolderPath}\" ] && echo 1 || echo 0", CompareStringFunc("1")));
        }

        public static FullExecutionOrder ChownFolder(string user, string group, string absoluteFolderPath)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"sudo chown -R {user}:{group} {absoluteFolderPath}"));
        }

        public static FullExecutionOrder CompressFolder(string targetFolder, string outputFile)
        {
            return new FullExecutionOrder()
                .AddCondition(new CommandExectionOrder($"[[ -f \"{outputFile}\" ]] && echo 1 || echo 0", CompareStringFunc("0")))
                .AddOrder(new CommandExectionOrder($"sudo tar -c --use-compress-program=pigz -v -f  {outputFile} {targetFolder}"))
                .AddPostCheck(new CommandExectionOrder($"[[ -f \"{outputFile}\" ]] && echo 1 || echo 0", CompareStringFunc("1")));
        }

        public static FullExecutionOrder DecompressFolder(string inputFile, string targetFolder = "/")
        {
            return new FullExecutionOrder()
                .AddPreCheck(new CommandExectionOrder($"[[ -f \"{inputFile}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
                .AddOrder(new CommandExectionOrder($"sudo tar -x --use-compress-program=pigz -v -f {inputFile} -C {targetFolder}"));
        }

        public static FullExecutionOrder ExecuteBashScript(string scriptFile, string artifactName = "bash", string args = "")
        {
            return new FullExecutionOrder()
                .AddPreCheck(new CommandExectionOrder($"[[ -f \"{scriptFile}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
                .AddOrder(new CommandExectionOrder($"sudo bash {scriptFile} {args}".Trim(), artifactName: artifactName));
        }

        public static FullExecutionOrder DeleteFolderContents(string targetFolder)
        {
            if(!targetFolder.EndsWith("/")) targetFolder += "/";
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"sudo rm -rf {targetFolder}*"));
        }

        public static FullExecutionOrder DeleteFolderWithContent(string targetFolder)
        {
            if(targetFolder.Trim() == "/" || targetFolder.Trim() == "." || targetFolder.Length < 4)
            {
                return new FullExecutionOrder()
                    .AddOrder(new SleepExecutionOrder(1));
            }
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"sudo rm -rf {targetFolder}"));
        }

        public static FullExecutionOrder DeleteFile(string targetFile)
        {
            return new FullExecutionOrder()
                .AddPreCheck(new CommandExectionOrder($"[[ -f \"{targetFile}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
                .AddOrder(new CommandExectionOrder($"sudo rm {targetFile}"))
                .AddPostCheck(new CommandExectionOrder($"[[ -f \"{targetFile}\" ]] && echo 1 || echo 0", CompareStringFunc("0")));
        }

        public static FullExecutionOrder DeleteFileIfExists(string targetFile)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"sudo rm {targetFile}", verifyExitStatus: false))
                .AddPostCheck(new CommandExectionOrder($"[[ -f \"{targetFile}\" ]] && echo 1 || echo 0", CompareStringFunc("0")));
        }

        internal static FullExecutionOrder CreateRsaPrivateKey(string fileName, int keybits)
        {
            return new FullExecutionOrder()
                .AddCondition(new CommandExectionOrder($"[[ -f \"{fileName}\" ]] && echo 1 || echo 0", CompareStringFunc("0")))
                .AddOrder(new CommandExectionOrder($"ssh-keygen -t rsa -b {keybits} -f {fileName} -N \"\" -q"))
                .AddPostCheck(new CommandExectionOrder($"[[ -f \"{fileName}.pub\" ]] && echo 1 || echo 0", CompareStringFunc("1")));
        }

        internal static FullExecutionOrder AppendToExistingFile(string publicKey, string fileName)
        {
            return new FullExecutionOrder()
                .AddPreCheck(new CommandExectionOrder($"[[ -f \"{fileName}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
                .AddOrder(new CommandExectionOrder($"echo \"{publicKey}\" >> {fileName}"))
                .AddPostCheck(new CommandExectionOrder($"grep -q \"{publicKey}\" {fileName} && echo \"1\" || echo \"0\"", CompareStringFunc("1")));
        }

        internal static FullExecutionOrder RemovePublicKeyFromFile(string publicKey, string fileName)
        {
            string cleanPublicKey = publicKey.Replace("/", "\\/");
            return new FullExecutionOrder()
                .AddPreCheck(new CommandExectionOrder($"[[ -f \"{fileName}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
                .AddOrder(new CommandExectionOrder($"sed -i '/{cleanPublicKey}/d' {fileName}"))
                .AddPostCheck(new CommandExectionOrder($"grep -q \"{publicKey}\" {fileName} && echo \"1\" || echo \"0\"", CompareStringFunc("0")));
        }


        internal static FullExecutionOrder Md5SumOfFile(string localFilePath)
        {
            return new FullExecutionOrder()
                .AddPreCheck(new CommandExectionOrder($"[[ -f \"{localFilePath}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
                .AddOrder(new CommandExectionOrder($"md5sum {localFilePath}", artifactName: "md5sum"));
        }

        internal static FullExecutionOrder Gns3Version(string artifactName)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"gns3server --version", artifactName: artifactName));
        }

        internal static FullExecutionOrder SshCopyFromLocalToRemote(string localFilePath, SshConnectionInfo host, string remotePath)
        {
            return new FullExecutionOrder()
                .AddPreCheck(new CommandExectionOrder($"[[ -f \"{localFilePath}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
                .AddOrder(new CommandExectionOrder($"scp -o StrictHostKeyChecking=accept-new {localFilePath} {host.Username}@{host.Host}:{remotePath}"));
        }

        internal static FullExecutionOrder VerifyMd5Sum(string md5sumWithPath)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"echo \"{md5sumWithPath}\" | md5sum -c -", verifyOutputFunc: inp => inp.Trim().EndsWith("OK")));
        }

        internal static FullExecutionOrder FixLineEndings(string filePath)
        {
            return new FullExecutionOrder()
            .AddPreCheck(new CommandExectionOrder($"[[ -f \"{filePath}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
            .AddOrder(new CommandExectionOrder($"sudo sed -i 's/\r$//' {filePath}"));
        }

        internal static FullExecutionOrder CPUStats(string artifactName)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"cat /proc/loadavg", artifactName: artifactName));
        }

        internal static FullExecutionOrder RAMStats(string artifactName)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder("free -h | awk '/Mem:/ {print $4\"/\"$2}'", artifactName: artifactName));
        }

        internal static FullExecutionOrder HDDStats(string artifactName)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder("df -h / | awk 'NR==2{print $4\"/\"$2}'", artifactName: artifactName));
        }

        internal static FullExecutionOrder Ls(string path, string artifactName)
        {
            return new FullExecutionOrder()
                .AddPreCheck(new CommandExectionOrder($"[[ -d \"{path}\" ]] && echo 1 || echo 0", CompareStringFunc("1")))
                .AddOrder(new CommandExectionOrder($"ls -1 {path}", artifactName: artifactName));
        }

        internal static FullExecutionOrder SetFileContentFromBase64(string filePath, string base64EncodedFile)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"sudo bash -c 'echo \"{base64EncodedFile}\" | base64 -d > {filePath}'"))
                .AddPostCheck(new CommandExectionOrder($"sudo bash -c '[[ -f \"{filePath}\" ]] && echo 1 || echo 0'", CompareStringFunc("1")));
        }

        internal static FullExecutionOrder FileExists(string filePath)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"[[ -f \"{filePath}\" ]] && echo 1 || echo 0", CompareStringFunc("1")));
        }

        internal static FullExecutionOrder GetFileSize(string fileName, string fILE_SIZE_ARTIFACT_NAME)
        {
            return new FullExecutionOrder()
                .AddOrder(new CommandExectionOrder($"stat --format=%s {fileName}", artifactName: fILE_SIZE_ARTIFACT_NAME));
        }

        internal static FullExecutionOrder Sleep(int v)
        {
            return new FullExecutionOrder()
                .AddOrder(new SleepExecutionOrder(v));
                
        }
    }
}
