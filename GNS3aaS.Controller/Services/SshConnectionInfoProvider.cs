﻿using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Tasks;
using GNS3aaS.Theater;

namespace GNS3aaS.Controller.Services
{
    public class SshConnectionInfoProvider
    {
        private readonly ILogger<SshConnectionInfoProvider> logger;
        private readonly IConfiguration configuration;
        private readonly string? username;
        private readonly string? sshKey;

        public SshConnectionInfoProvider(ILogger<SshConnectionInfoProvider> _logger, IConfiguration configuration) 
        {
            logger = _logger;
            this.configuration = configuration;
            username = configuration["SSH:Username"];
            sshKey = configuration["SSH:Key"];
            sshKey = sshKey?.Replace("\\n", "\n");
            if(string.IsNullOrEmpty(username) || string.IsNullOrEmpty(sshKey))
            {
                throw new ArgumentException("Missing ssh:username oder ssh:key");
            }
        }

        public SshConnectionInfo GetConnectionInfo(ServerNodeEntity serverNodeEntity)
        {
            return new SshConnectionInfo(serverNodeEntity.HostAddress, username!, sshKey!);
        }
        

    }
}
