﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Groups
{
    internal class ListGroupsHandler : BaseHandler, IHandler
    {
        public const string LineFormat = "| {0,36} | {1,10} | {2,19} | {3,30} |";

        public ListGroupsHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("list", $"list available and active groups");

            command.SetHandler(async () =>
            {
                await handle();
            });

            return command;
        }

        private async Task handle()
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var activeGroupResponse = await client.ActiveGroups();
                Console.WriteLine($"Active Groups = {string.Join(",", activeGroupResponse.ActiveGroups.Select(a => a.Name))}");
                Console.WriteLine($"Groups = {string.Join(",", activeGroupResponse.AvailableGroups)}");

                Console.WriteLine();
                Console.WriteLine(string.Format(LineFormat,
                    "Id",
                    "Name",
                    "Created",
                    "Responsible"
                    ));
                foreach(var activeGroup in activeGroupResponse.ActiveGroups)
                {
                    Console.WriteLine(string.Format(LineFormat,
                        activeGroup.Id,
                        activeGroup.Name,
                        activeGroup.Created,
                        activeGroup.ResponsiblePersonEmail));
                }
            }
        }
    }
}