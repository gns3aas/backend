﻿namespace GNS3aaS.Lib.Models.Enums
{
    public enum ServerNodeState
    {

        Unknown = 5,
        Available = 10,
        Occupied = 20,
        Broken = 30,
        Reserved = 40,

        New = 105,
        InstallRequested = 120,
        InstallingBase = 130,
        InstallingApps = 140,

        ReleaseRequested = 200,
        Releasing = 210
    }
}
