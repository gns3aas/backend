﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    internal class ServerNodesListHandler : BaseHandler, IHandler
    {
        //                                          ID      Name      Addr     State    M-SystId
        public const string SessionLineFormat = "| {0,37} | {1,16} | {2,14} | {3,15} | {4,7} | {5,4} | {6,8} | {7,5} | {8,11} | {9,10} | {10,19} |";
        public ServerNodesListHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("list", "list all nodes");

            var showDetailsOption = new Option<bool>(new string[] { "-d", "--details" }, "show details");

            command.AddOption(showDetailsOption);

            command.SetHandler(async (showDetails) =>
            {
                await handle(showDetails);
            }, showDetailsOption);

            return command;
        }

        private async Task handle(bool showDetails)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var nodes = await client.GetNodes();

                if (nodes == null) return;

                if(!showDetails)
                {
                    Console.WriteLine(string.Format(SessionLineFormat,
                        "Id",
                        "Name",
                        "Address",
                        "State",
                        "M-Id",
                        "M-St",
                        "GNS3",
                        "CPU",
                        "RAM",
                        "HDD",
                        "Last"
                        ));
                }


                foreach (var node in nodes)
                {
                    if(showDetails)
                    {
                        foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(node))
                        {
                            string name = descriptor.Name;
                            object? value = descriptor.GetValue(node);
                            if (value != null && value is string) value = ((string)value).Replace("\n", " ");
                                    
                            Console.WriteLine("{0} = {1}", name, value);
                        }
                        Console.WriteLine(new string('-', 60));
                    }
                    else
                    {
                        Console.WriteLine(string.Format(SessionLineFormat,
                        node.Id,
                        node.Name,
                        node.HostAddress,
                        node.ServerNodeState,
                        node.MaasSystemId,
                        node.MaasState,
                        node.GNS3Version,
                        node.CPU?.Trim().Split(" ").FirstOrDefault(),
                        node.RAM,
                        node.HDD,
                        node.LastStatsUpdate
                        ));
                    }
                }
            }
        }
    }
}
