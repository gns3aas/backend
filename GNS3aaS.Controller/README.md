# GNS3aaS Controller


## Required Environment variables
```bash
GNS3AAS_SSH__Key
GNS3AAS_SSH__Username
GNS3AAS_Jwt__Issuer
GNS3AAS_Jwt__Audience
GNS3AAS_Jwt__Key
GNS3AAS_Smtp__Host
GNS3AAS_Smtp__Sender
GNS3AAS_Smtp__Username
GNS3AAS_Smtp__Password
GNS3AAS_Smtp__Port
GNS3AAS_EmailHashSecret
```

 - `GNS3AAS_Jwt__Key` requires min 512 bits

## Optional Enviroment variables
```bash

```