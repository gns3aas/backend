﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GNS3aaS.Controller.Migrations
{
    /// <inheritdoc />
    public partial class MaasState : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaasState",
                table: "ServerNodes",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaasState",
                table: "ServerNodes");
        }
    }
}
