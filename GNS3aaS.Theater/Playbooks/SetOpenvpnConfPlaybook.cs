﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    internal class SetOpenvpnConfPlaybook : IPlaybook
    {

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public SetOpenvpnConfPlaybook()
        {
            orderList.Add(ExecutionOrderHelper.StopService("openvpn", false));
            orderList.Add(ExecutionOrderHelper.CreateFolder("/opt/gns3aas"));
            orderList.Add(ExecutionOrderHelper.DeleteFolderWithContent(ConfigureOpenVpnPlaybook.OPENVPN_ETC_FOLDER));
            orderList.Add(ExecutionOrderHelper.DeleteFileIfExists(ConfigureOpenVpnPlaybook.OPENVPN_BACKUP_FILE));
            orderList.Add(ExecutionOrderHelper.UploadFile(ConfigureOpenVpnPlaybook.OPENVPN_BACKUP_FILE));
            orderList.Add(ExecutionOrderHelper.DecompressFolder(ConfigureOpenVpnPlaybook.OPENVPN_BACKUP_FILE));
            orderList.Add(ExecutionOrderHelper.StartService("openvpn"));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
