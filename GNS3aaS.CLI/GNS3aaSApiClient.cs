﻿using GNS3aaS.Lib.Authentification;
using GNS3aaS.Lib.Models.Pocos;
using GNS3aaS.Lib.Models.Requests;
using GNS3aaS.Lib.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.ComponentModel;
using System.Text.Json.Serialization;
using System.Web;
using Microsoft.AspNetCore.WebUtilities;

namespace GNS3aaS.CLI
{
    public class GNS3aaSApiClient : IDisposable
    {

        private readonly HttpClient Client;

        public GNS3aaSApiClient(string baseUrl, string? token = null)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri(baseUrl);
            if(token != null)
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
        }

        private JsonSerializerOptions GetJsonSerializerOptions()
        {
            var enumConverter = new JsonStringEnumConverter();
            var jsonOptions = new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true,
            };
            jsonOptions.Converters.Add(enumConverter);
            return jsonOptions;
        }

        public void Dispose()
        {
            Client.Dispose();
        }

        public async Task<DefaultResponse> Login( string email)
        {
            var body = new LoginRequest() { Email = email };
            var result = await Client.PostAsJsonAsync<LoginRequest>("api/v1/login", body);
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        private async Task<T> getFromJsonAsync<T>(string url)
        {
            var response = await Client.GetAsync(url);
            if(response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine($"Requested (GET) failed: {response.StatusCode}");
                return default(T);
            }
            var contentString = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<T>(contentString, GetJsonSerializerOptions());
        }

        private async Task<T> deleteFromJsonAsync<T>(string url)
        {
            var response = await Client.DeleteAsync(url);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine($"Requested (DELETE) failed: {response.StatusCode}");
                return default(T);
            }
            var contentString = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<T>(contentString, GetJsonSerializerOptions());
        }

        internal async Task<List<SessionPoco>> GetSessions()
        {
            return await getFromJsonAsync<List<SessionPoco>>("/api/v1/sessions");
        }

        internal async Task<DefaultResponse> CreateSession()
        {
            var result = await Client.PostAsJsonAsync<object>("/api/v1/session/create", new { });
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        internal async Task<DefaultResponse> SessionAction(string commandStr, string sessionId)
        {
            var result = await Client.PostAsJsonAsync<object>($"/api/v1/session/{commandStr}/{sessionId}", new { }, GetJsonSerializerOptions());
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        internal async Task<SessionPoco> GetSession(string sessionId)
        {
            return await getFromJsonAsync<SessionPoco>($"/api/v1/session/{sessionId}");
        }

        internal async Task<List<ServerNodePoco>> GetNodes()
        {
            return await getFromJsonAsync<List<ServerNodePoco>>("/api/v1/servernodes");
        }

        internal async Task<DefaultResponse> ResetNode(string nodeId)
        {
            return await getFromJsonAsync<DefaultResponse>($"/api/v1/servernode/reset/{nodeId}");
        }

        internal async Task<DefaultResponse> ReserverNode(string nodeId)
        {
            return await getFromJsonAsync<DefaultResponse>($"/api/v1/servernode/reserve/{nodeId}");
        }

        internal async Task<UserStatsResponse> UsersStats()
        {
            return await getFromJsonAsync<UserStatsResponse>($"/api/v1/users");
        }

        internal async Task<List<string>> Groups()
        {
            return await getFromJsonAsync<List<string>>($"/api/v1/groups");
        }

        internal async Task<ActiveGroupsResponse> ActiveGroups()
        {
            return await getFromJsonAsync<ActiveGroupsResponse>($"/api/v1/active-groups");
        }

        internal async Task<DefaultResponse> DeleteSession(string sessionId)
        {
            return await deleteFromJsonAsync<DefaultResponse>($"/api/v1/session/{sessionId}");
        }

        internal async Task<DefaultResponse> CreateSessionForUser(string email)
        {
            var result = await Client.PostAsJsonAsync<object>("/api/v1/session/create-for-user", new CreateSessionRequest()
            {
                Email = email
            }, GetJsonSerializerOptions());
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        internal async Task<DefaultResponse> ImportUsers(ImportUsersRequest importUsersRequest)
        {
            var result = await Client.PostAsJsonAsync<object>($"/api/v1/users/import", importUsersRequest, GetJsonSerializerOptions());
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        internal async Task<DefaultResponse> DeleteCommonUsers()
        {
            return await deleteFromJsonAsync<DefaultResponse>($"/api/v1/users/all-common-users");
        }

        internal async Task<DefaultResponse> DeleteSuperUser(string email)
        {
            var query = new Dictionary<string, string>();
            query["email"] = email;
            return await deleteFromJsonAsync<DefaultResponse>(QueryHelpers.AddQueryString("api/v1/superuser/delete", query));
        }

        internal async Task<DefaultResponse> AddSuperUser(string email, bool isadmin)
        {
            var result = await Client.PostAsJsonAsync<object>($"api/v1/superuser/add", new SuperUserRequest() { Email = email, IsAdmin = isadmin }, GetJsonSerializerOptions());
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        internal async Task<DefaultResponse> AddActiveGroup(string groupName, string responsible)
        {
            var result = await Client.PostAsJsonAsync<object>($"api/v1/active-group/add", new GroupRequest() { Name = groupName, ResponsiblePersonEmail = responsible }, 
                GetJsonSerializerOptions());
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        internal async Task<DefaultResponse> DeleteActiveGroup(string groupName)
        {
            var query = new Dictionary<string, string>();
            query["groupName"] = groupName;
            return await deleteFromJsonAsync<DefaultResponse>(QueryHelpers.AddQueryString("api/v1/active-group/delete", query));
        }

        internal async Task<DefaultResponse> AddServerNode(NewServerNodeRequest newServerNodeRequest)
        {
            var result = await Client.PostAsJsonAsync<object>($"api/v1/servernode/add", newServerNodeRequest, GetJsonSerializerOptions());
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        internal async Task<DefaultResponse> EditServerNode(UpdateServerNodeRequest updateServerNodeRequest)
        {
            var result = await Client.PostAsJsonAsync<object>($"api/v1/servernode/edit", updateServerNodeRequest, GetJsonSerializerOptions());
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        internal async Task<DefaultResponse> DeleteServerNode(string sessionId)
        {
            return await deleteFromJsonAsync<DefaultResponse>($"/api/v1/servernode/delete/{sessionId}");
        }

        internal async Task<DefaultResponse> DeployNode(string nodeId)
        {
            return await getFromJsonAsync<DefaultResponse>($"/api/v1/servernode/deploy/{nodeId}");
        }

        internal async Task<DefaultResponse> ReleaseNode(string nodeId)
        {
            return await getFromJsonAsync<DefaultResponse>($"/api/v1/servernode/release/{nodeId}");
        }

        internal async Task<DefaultResponse> PublishNode(string nodeId)
        {
            return await getFromJsonAsync<DefaultResponse>($"/api/v1/servernode/publish/{nodeId}");
        }

        internal async Task<DefaultResponse> ForceIdleSession(string sessionId)
        {
            var result = await Client.PostAsJsonAsync<object>($"/api/v1/session/force-idle/{sessionId}", new { }, GetJsonSerializerOptions());
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }

        internal async Task<DefaultResponse> DeleteOpenVPNFromSession(string sessionId)
        {
            var result = await Client.PostAsJsonAsync<object>($"/api/v1/session/delete-openvpn/{sessionId}", new { }, GetJsonSerializerOptions());
            var response = await result.Content.ReadFromJsonAsync<DefaultResponse>();
            return response!;
        }
    }
}
