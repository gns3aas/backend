﻿using GNS3aaS.Lib.Authentification;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace GNS3aaS.Controller.Model.Db
{
    [Index(nameof(EMailHash), IsUnique = true)]
    public class UserEntity
    {

        [Key]
        public Guid Id { get; set; }

        public required string EMailHash { get; set; }

        public JwtUserRoles UserRole { get; set; }

        public string GroupName { get; set; }

        public string? JwtToken { get; set; }

        public DateTime? JwtTokenValidUntil { get; set; }

        [JsonIgnore]
        public virtual List<SessionEntity> AchiveSessions { get; set; }

    }
}
