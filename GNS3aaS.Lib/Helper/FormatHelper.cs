﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Helper
{
    public class FormatHelper
    {

        public static string FormatBytes(long? bytes)
        {
            if (!bytes.HasValue) return "0 B";
            if (bytes < 0) return "0 B";
            

            string[] sizes = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            double len = bytes.Value;
            int order = 0;

            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len = len / 1024;
            }

            // Format the result as a string with two decimal places.
            return String.Format("{0:0.##} {1}", len, sizes[order]);
        }

    }
}
