﻿using GNS3aaS.CLI.Handler.Users;
using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Groups
{
    internal class GroupsHandler : BaseHandler, IHandler
    {
        public GroupsHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var subCommands = new Command("group");

            subCommands.Add(new ListGroupsHandler(configStorage).GetCommand());
            subCommands.Add(new ActivateGroupHandler(configStorage).GetCommand());
            subCommands.Add(new DisableGroupHandler(configStorage).GetCommand());

            return subCommands;
        }
    }
}