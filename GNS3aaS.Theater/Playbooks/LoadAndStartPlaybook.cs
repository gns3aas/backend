﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class LoadAndStartPlaybook : IPlaybook
    {
        public const string ARCHIVE_BASE_PATH = "/opt/project-archive";
        public const string ARCHIVE_FILE_ENDING = ".tar.gz";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public LoadAndStartPlaybook(string userId)
        {
            var archivePath = $"{ARCHIVE_BASE_PATH}/{userId}{ARCHIVE_FILE_ENDING}";

            orderList.Add(ExecutionOrderHelper.StopService("gns3", verifyExitStatus: false, doPostCheck: false));
            orderList.Add(ExecutionOrderHelper.StopService("gns3-server", verifyExitStatus: false, doPostCheck: false));
            // Usually it works the second time
            orderList.Add(ExecutionOrderHelper.Sleep(1000));
            orderList.Add(ExecutionOrderHelper.StopService("gns3-server"));
            orderList.Add(ExecutionOrderHelper.StopService("gns3"));

            orderList.Add(ExecutionOrderHelper.DecompressFolder(archivePath));
            orderList.Add(ExecutionOrderHelper.DeleteFile(archivePath));
            orderList.Add(ExecutionOrderHelper.ChownFolder("gns3", "gns3", StopAndArchivePlaybook.GNS_PROJECTS_PATH));
            orderList.Add(ExecutionOrderHelper.StartService("gns3"));
            orderList.Add(ExecutionOrderHelper.StartService("gns3-server"));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }

    }
}
