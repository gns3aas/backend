﻿namespace GNS3aaS.Controller.Services.ServerNodeLocking
{
    public class ServerNodeLocks
    {
        public List<ServerNodeLock> locks = new List<ServerNodeLock>();

        public ServerNodeLocks() { }

        public void Lock(Guid sessionId, Guid onNodeId, Guid? fromNodeId = null)
        {
            lock(locks)
            {
                locks.Add(new ServerNodeLock() { SessionId = sessionId, FromNodeId = fromNodeId, OnNodeId = onNodeId });
            }
        }

        public bool IsNodeLocked(Guid nodeId)
        {
            return locks.Any(x => x.OnNodeId == nodeId || x.FromNodeId == nodeId);
        }

        public void Release(Guid sessionId)
        {
            lock(locks)
            {
                var lockedServerNode = locks.FirstOrDefault(x => x.SessionId == sessionId);
                if (lockedServerNode == null) return;
                locks.Remove(lockedServerNode);
            }
        }

    }
}
