﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Lib.Authentification;
using Microsoft.EntityFrameworkCore;

namespace GNS3aaS.Controller.Services
{
    public static class AuthentificateUserDbExtension
    {

        private static string? GetEmailFromCurrentUser(HttpContext httpContext)
        {
            return httpContext.User.Claims.Single(c => c.Type == JwtTokenHelper.EMAIL_CLAIM || c.Type.EndsWith("/emailaddress")).Value;
        }

        private static string? GetUserIdFromCurrentUser(HttpContext httpContext)
        {
            return httpContext.User.Claims.Single(c => c.Type == JwtTokenHelper.USERID_CLAIM).Value;
        }

        public static bool AuthentificateUser(this GNS3aaSDbContext dbContext, HttpContext httpContext, out string eMail, out string userId, out UserEntity user)
        {
            eMail = GetEmailFromCurrentUser(httpContext)!;
            var _userId = GetUserIdFromCurrentUser(httpContext)!;
            userId = _userId;
            user = null!;

            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(eMail))
            {
                return false;
            }

            user = dbContext.Users.SingleOrDefault(u => u.Id == Guid.Parse(_userId))!;

            if (user == null)
            {
                return false;
            }

            return true;
        }

    }
}
