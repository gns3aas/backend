﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Helper
{
    public class OpenVPNConfigHelper
    {
        public static string AdjustRemotePort(string clientConfigFile, string host, int remotePort)
        {
            var configFileLines = clientConfigFile.Split("\n");
            for(int a = 0; a < configFileLines.Length; a++)
            {
                var line = configFileLines[a].Trim();
                if(line.StartsWith("remote ")) {
                    configFileLines[a] = $"remote {host} {remotePort} tcp4";
                }
            }
            return string.Join("\n", configFileLines);
        }
    }
}
