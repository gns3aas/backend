﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Models.Requests
{
    public class UpdateServerNodeRequest
    {
        public Guid Id { get; set; }

        public int? OpenVpnPort { get; set; }

        public int? WireGuardPort { get; set; }

    }
}
