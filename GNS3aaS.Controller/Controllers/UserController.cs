﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Providers;
using GNS3aaS.Lib.Authentification;
using GNS3aaS.Lib.Models.Requests;
using GNS3aaS.Lib.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GNS3aaS.Controller.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly GNS3aaSDbContext dbContext;
        private readonly SecretProvider secretProvider;
        private readonly JwtTokenProvider tokenProvider;

        public UserController(GNS3aaSDbContext dbContext, SecretProvider secretProvider, JwtTokenProvider tokenProvider)
        {
            this.dbContext = dbContext;
            this.secretProvider = secretProvider;
            this.tokenProvider = tokenProvider;
        }

        [Authorize(Policy = "Administrator")]
        [HttpPost("api/v1/users/import")]
        public IActionResult ImportUsers([FromBody] ImportUsersRequest importUsersRequests)
        {
            List<UserEntity> newUsers = new List<UserEntity>();

            foreach(var userEmailPart in importUsersRequests.Users)
            {
                newUsers.Add(new UserEntity()
                {
                    UserRole = Lib.Authentification.JwtUserRoles.User,
                    EMailHash = secretProvider.HashEmail(userEmailPart.Email),
                    Id = Guid.NewGuid(),
                    GroupName = userEmailPart.Group
                });
            }

            var emailHashes = newUsers.Select(n => n.EMailHash).ToList();
            var dublicatedUsers = dbContext.Users.Where(u => emailHashes.Contains(u.EMailHash)).Select(d => d.EMailHash).ToList();
            newUsers = newUsers.Where(n => !dublicatedUsers.Contains(n.EMailHash)).ToList();

            dbContext.Users.AddRange(newUsers);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = $"Added {newUsers.Count} users", Data = newUsers.Count });
        }

        [Authorize(Policy = "Administrator")]
        [HttpDelete("api/v1/users/all-common-users")]
        public IActionResult DeleteUsers()
        {
            var num = dbContext.Users.Where(i => i.UserRole == Lib.Authentification.JwtUserRoles.User).ExecuteDelete();
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = $"Deleted {num} users", Data = num });
        }

        [Authorize(Policy = "Administrator")]
        [HttpPost("api/v1/superuser/add")]
        public IActionResult AddSuperUser([FromBody] SuperUserRequest newSuperUserRequest)
        {
            var hashedEmail = secretProvider.HashEmail(newSuperUserRequest.Email);

            if(dbContext.Users.Any(d => d.EMailHash == hashedEmail))
            {
                return Ok(new DefaultResponse() { Success = false, Message = "user already exists"});
            }

            var userEntity = new UserEntity()
            {
                UserRole = newSuperUserRequest.IsAdmin ? JwtUserRoles.Administrator : JwtUserRoles.SuperUser,
                EMailHash = hashedEmail,
                Id = Guid.NewGuid(),
                GroupName = newSuperUserRequest.IsAdmin ? "Administrators" : "SuperUsers",
            };

            dbContext.Users.Add(userEntity);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = $"added user", Data = userEntity.Id });
        }

        [Authorize(Policy = "Administrator")]
        [HttpDelete("api/v1/superuser/delete")]
        public IActionResult DeleteSuperUser(string email)
        {
            var hashedEmail = secretProvider.HashEmail(email);

            var userEntity = dbContext.Users.FirstOrDefault(u => u.EMailHash == hashedEmail);

            if (userEntity == null)
            {
                return Ok(new DefaultResponse() { Success = false, Message = "user not found" });
            }

            dbContext.Users.Remove(userEntity);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = $"deleted user", Data = userEntity.Id });
        }

        [Authorize(Policy = "Administrator")]
        [HttpGet("api/v1/users")]
        public IActionResult UsersStats()
        {
            var stats = new UserStatsResponse(){
                Total = dbContext.Users.Count(),
                NumOfUsers = dbContext.Users.Count(u => u.UserRole == JwtUserRoles.User),
                NumOfSuperUsers = dbContext.Users.Count(u => u.UserRole == JwtUserRoles.SuperUser),
                NumOfAdministrators = dbContext.Users.Count(u => u.UserRole == JwtUserRoles.Administrator)
            };

            return Ok(stats);
        }

        [AllowAnonymous]
        [HttpGet("api/v1/users/init")]
        public IActionResult InitAdmin(string email)
        {
            if(dbContext.Users.Any(u => u.UserRole == Lib.Authentification.JwtUserRoles.Administrator))
            {
                return BadRequest();
            }

            var hashedEMail = secretProvider.HashEmail(email);

            var adminUser = new UserEntity()
            {
                Id = Guid.NewGuid(),
                EMailHash = hashedEMail,
                GroupName = "Administrators",
                UserRole = Lib.Authentification.JwtUserRoles.Administrator
            };

            adminUser.JwtToken = tokenProvider.GenerateTokenFor(adminUser.Id, email, adminUser.GroupName, adminUser.UserRole, Global.TokenValidityInMinutesAdmin);
            adminUser.JwtTokenValidUntil = DateTime.Now.AddDays(1);

            dbContext.Users.Add(adminUser);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = $"admin user created", Data = adminUser.JwtToken });
        }


    }
}
