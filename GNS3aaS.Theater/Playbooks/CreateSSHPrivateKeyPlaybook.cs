﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class CreateSSHPrivateKeyPlaybook : IPlaybook
    {
        public const string SSH_PRIVATE_KEY_PATH = ".ssh/id_rsa";
        public const string SSH_PUBLIC_KEY_PATH = ".ssh/id_rsa.pub";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public CreateSSHPrivateKeyPlaybook()
        {
            orderList.Add(ExecutionOrderHelper.CreateRsaPrivateKey(SSH_PRIVATE_KEY_PATH, 2048));
            orderList.Add(ExecutionOrderHelper.DownloadFile(SSH_PUBLIC_KEY_PATH));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
