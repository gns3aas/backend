﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Helper;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Model.Extensions;
using GNS3aaS.Controller.Providers;
using GNS3aaS.Controller.Services;
using GNS3aaS.Lib.Authentification;
using GNS3aaS.Lib.Models.Enums;
using GNS3aaS.Lib.Models.Requests;
using GNS3aaS.Lib.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Reflection.Metadata.Ecma335;

namespace GNS3aaS.Controller.Controllers
{
    [ApiController]
    public class SessionController : ControllerBase
    {
        private readonly GNS3aaSDbContext dbContext;
        private readonly SecretProvider secretProvider;
        private readonly ILogger<SessionController> logger;

        public SessionController(GNS3aaSDbContext dbContext, SecretProvider secretProvider, ILogger<SessionController> _logger)
        {
            this.dbContext = dbContext;
            this.secretProvider = secretProvider;
            logger = _logger;
        }

        [Authorize(Policy = "User")]
        [HttpPost("api/v1/session/create")]
        public IActionResult CreateSession()
        {
            if(!dbContext.AuthentificateUser(HttpContext, out var eMail, out var userId, out var user))
            {
                return BadRequest();
            }

            var session = dbContext.Sessions.SingleOrDefault(s => s.UserId == user.Id);

            if(session != null)
            {
                return BadRequest();
            }

            var newSession = new SessionEntity()
            {
                Id = Guid.NewGuid(),
                User = user,
                State = SessionState.IDLE,
                Email = eMail,
                KeepSession = false,
                KeepRunning = false,
                UserId = user.Id,
                Updated = DateTime.Now,
            };

            dbContext.Sessions.Add(newSession);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "", Data = newSession.Id.ToString() });
        }


        [Authorize(Policy = "SuperUser")]
        [HttpPost("api/v1/session/create-for-user")]
        public IActionResult CreateSessionForUser([FromBody] CreateSessionRequest createSessionRequest)
        {
            var hashedEMail = secretProvider.HashEmail(createSessionRequest.Email);

            var user = dbContext.Users.SingleOrDefault(u => u.EMailHash == hashedEMail);

            if(user == null)
            {
                return NotFound(new DefaultResponse() { Success = false, Message = "user not found" });
            }
            
            var session = dbContext.Sessions.SingleOrDefault(s => s.UserId == user.Id);

            if (session != null)
            {
                return BadRequest(new DefaultResponse() { Success = false, Message = "session already exists"});
            }

            var newSession = new SessionEntity()
            {
                Id = Guid.NewGuid(),
                User = user,
                State = SessionState.IDLE,
                Email = createSessionRequest.Email,
                KeepSession = false,
                UserId = user.Id,
                Updated = DateTime.Now,
            };

            dbContext.Sessions.Add(newSession);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "", Data = newSession.Id.ToString() });
        }

        [Authorize(Policy = "User")]
        [HttpGet("api/v1/session")]
        public IActionResult GetSessionForUser()
        {
            if (!dbContext.AuthentificateUser(HttpContext, out var eMail, out var userId, out var user))
            {
                return BadRequest();
            }

            var session = dbContext.Sessions.SingleOrDefault(s => s.UserId == user.Id);

            if(session == null)
            {
                return NotFound();
            }

            return Ok(session.ToPoco());
        }

        [Authorize(Policy = "SuperUser")]
        [HttpGet("api/v1/session/{sessionId}")]
        public IActionResult GetSessionForUser(Guid sessionId)
        {
            var session = dbContext.Sessions.SingleOrDefault(s => s.Id == sessionId);

            if (session == null)
            {
                return NotFound();
            }

            return Ok(session.ToPoco());
        }


        [Authorize(Policy = "SuperUser")]
        [HttpGet("api/v1/sessions")]
        public IActionResult GetSessions()
        {
            var sessions = dbContext.Sessions.Select(s => s.ToPoco()).ToList();

            return Ok(sessions);
        }


        [Authorize(Policy = "SuperUser")]
        [HttpDelete("api/v1/session/{sessionId}")]
        public IActionResult DeleteSession(Guid sessionId)
        {
            var session = dbContext.Sessions.SingleOrDefault(s => s.Id == sessionId);

            if (session == null) return NotFound();

            dbContext.Sessions.Remove(session);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

        [Authorize(Policy = "SuperUser")]
        [HttpPost("api/v1/session/force-idle/{sessionId}")]
        public IActionResult ForceIdleSession(Guid sessionId)
        {
            var session = dbContext.Sessions.SingleOrDefault(s => s.Id == sessionId);

            if (session == null) return NotFound();

            session.State = SessionState.IDLE;
            session.Message = "";
            session.Started = null;
            session.ActiveNode = null;
            session.ActiveNodeId = null;
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

        [Authorize(Policy = "SuperUser")]
        [HttpPost("api/v1/session/delete-openvpn/{sessionId}")]
        public IActionResult DeleteOpenVPNConfig(Guid sessionId)
        {
            var session = dbContext.Sessions.SingleOrDefault(s => s.Id == sessionId);

            if (session == null) return NotFound();

            if (!(session.State == SessionState.IDLE || session.State == SessionState.BROKEN))
            {
                return BadRequest(new DefaultResponse() { Success = false, Message = "session already exists" });
            }

            session.OpenVPNClientConfig = null;
            session.OpenVPNServerConfig = null;
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

        private void updateSession(SessionEntity session, SessionState state)
        {
            session.State = state;
            session.Message = "";
            session.Updated = DateTime.Now;
        }

        [Authorize(Policy = "User")]
        [HttpPost("api/v1/session/start/{sessionId}")]
        public IActionResult StartSession(Guid sessionId)
        {
            if (!dbContext.AuthentificateUser(HttpContext, out var eMail, out var userId, out var user))
            {
                return BadRequest();
            }

            var session = dbContext.Sessions.SingleOrDefault(s => s.Id == sessionId);

            if (session == null) return NotFound();

            var ipAddress = RemoteAddressHelper.StripPortFromRemoteAddressIfAny(this.GetRemoteIpAddressOrDefault());

            if (!HttpContext.User.Claims.Any(c => c.Type == "SuperUser") && user.Id != session.UserId )
            {
                return StatusCode((int)HttpStatusCode.Forbidden);
            }

            if(session.State != SessionState.IDLE)
            {
                return Ok(new DefaultResponse() { Success = false, Message = "Session is not idle and cannot be started" });
            }

            logger.LogInformation($"Session {sessionId} start request from {ipAddress} by {eMail} {userId}");

            updateSession(session, SessionState.START_REQUESTED);

            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

        [Authorize(Policy = "User")]
        [HttpPost("api/v1/session/stop/{sessionId}")]
        public IActionResult StopSession(Guid sessionId)
        {
            if (!dbContext.AuthentificateUser(HttpContext, out var eMail, out var userId, out var user))
            {
                return BadRequest();
            }

            var session = dbContext.Sessions.SingleOrDefault(s => s.Id == sessionId);

            if (session == null) return NotFound();

            if (!HttpContext.User.Claims.Any(c => c.Type == "SuperUser") && user.Id != session.UserId)
            {
                return StatusCode((int)HttpStatusCode.Forbidden);
            }

            if (session.State != SessionState.RUNNING && !HttpContext.User.Claims.Any(c => c.Type == "Administrator"))
            {
                return Ok(new DefaultResponse() { Success = false, Message = "Session is not running and cannot be stopped" });
            }

            updateSession(session, SessionState.STOP_REQUESTED);

            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

    }
}
