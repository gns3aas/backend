﻿using GNS3aaS.Lib.Logging;
using GNS3aaS.Theater.Playbooks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Workflows
{
    public class CreateNewSessionNoArchiveWorkflow : IWorkflow
    {

        public CreateNewSessionNoArchiveWorkflow(SshConnectionInfo sshConnectionInfo, PublicHostConnectionInfo publicHostInfo)
        {
            SshConnectionInfo = sshConnectionInfo;
            PublicHostInfo = publicHostInfo;
        }

        public SshConnectionInfo SshConnectionInfo { get; }
        public PublicHostConnectionInfo PublicHostInfo { get; }
        public bool IsSuccessfull { get; private set; } = false;
        public string Message { get; private set; } = "";

        public bool IsFinished { get; private set; } = false;

        public string? OpenVPNClientConfigFile { get; private set; }

        public string? OpenVPNServerConfigFile { get; private set; }

        public const int MIN_OPENVPN_SERVER_CONFIG_SIZE = 1000;

        public string? GNS3Version { get; private set; }
        
        public string? WireGuardConf { get; private set; }

        private readonly ILogger<CreateNewSessionNoArchiveWorkflow> logger = StaticLoggerFactory.GetStaticLogger<CreateNewSessionNoArchiveWorkflow>();

        public void Run()
        {
            if(!NewOpenVPNKeysPlaybook(SshConnectionInfo, out var openVPNConfigFile, out var openVPNServerConfigurationFile))
            {
                IsSuccessfull = false;
                IsFinished = true;
                logError($"Failed to create new openvpn client or to retrieve gns3version config on {SshConnectionInfo}");
                return;
            }
            if(!NewWireguardKeys(SshConnectionInfo, PublicHostInfo.HostName, out var wireguardConfs))
            {
                IsSuccessfull = false;
                IsFinished = true;
                logError($"Failed to create new wireguard key on {SshConnectionInfo}");
                return;
            }
            if(!GetGNS3Version(SshConnectionInfo, out var gns3version))
            {
                IsSuccessfull = false;
                IsFinished = true;
                logError($"Failed to get GNS3 Version on {SshConnectionInfo}");
                return;
            }
            OpenVPNClientConfigFile = openVPNConfigFile;
            OpenVPNServerConfigFile = openVPNServerConfigurationFile;
            GNS3Version = gns3version;
            WireGuardConf = wireguardConfs;

            OpenVPNClientConfigFile = RestoreSessionWorkflow.UpdateOpenVPNClientConfigFile(OpenVPNClientConfigFile, PublicHostInfo);
            WireGuardConf = RestoreSessionWorkflow.UpdateWireGuardConfigFile(WireGuardConf, PublicHostInfo);

            var cleanAndStartPlaybookRunner = new PlaybookRunner(SshConnectionInfo,
                new CleanAndStartGNS3Playbook());

            cleanAndStartPlaybookRunner.Run();

            IsSuccessfull = cleanAndStartPlaybookRunner.IsSuccess();

            IsFinished = true;
        }

        private void logError(string message)
        {
            Message += " " + message;
            Message = Message.Trim();
            logger.LogError(message);
        }

        internal static bool GetGNS3Version(SshConnectionInfo host, out string? gns3version)
        {
            gns3version = null; 
            var gns3versionPlaybook = new PlaybookRunner(host, new Gns3VersionPlaybook());
            gns3versionPlaybook.Run();
            if (!gns3versionPlaybook.IsSuccess())
            {
                return false;
            }
            gns3version = gns3versionPlaybook.Artifacts[Gns3VersionPlaybook.GNS3_VERSION_ARTIFACT].Trim();

            return true;
        }

        internal static bool NewOpenVPNKeysPlaybook(SshConnectionInfo host, out string? openVpnConfigFile, out string? openVPNServerConfigurationFile)
        {
            openVpnConfigFile = null;
            
            openVPNServerConfigurationFile = null;

            var configurePlaybook = new ConfigureOpenVpnPlaybook();
            var assmblyPath = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory!.FullName;
            var configurePlaybookRunner = new PlaybookRunner(host, configurePlaybook);
            var configureOpenVpnFile = File.ReadAllBytes(Path.Combine(assmblyPath, "Scripts" , "configure-openvpn.sh"));
            configurePlaybookRunner.Files[ConfigureOpenVpnPlaybook.SCRIPT_PATH] = configureOpenVpnFile; 

            configurePlaybookRunner.Run();
            if(!configurePlaybookRunner.IsSuccess())
            {
                return false;
            }

            openVpnConfigFile = Encoding.UTF8.GetString(configurePlaybookRunner.Files[ConfigureOpenVpnPlaybook.OPENVPN_FILE_PATH]);
            var openVPNServerConfigFileSize = configurePlaybookRunner.Files[ConfigureOpenVpnPlaybook.OPENVPN_BACKUP_FILE].Length;
            openVPNServerConfigurationFile = System.Convert.ToBase64String(configurePlaybookRunner.Files[ConfigureOpenVpnPlaybook.OPENVPN_BACKUP_FILE]);

            return openVpnConfigFile.Contains("client") && openVPNServerConfigFileSize > MIN_OPENVPN_SERVER_CONFIG_SIZE;
        }

        public static bool NewWireguardKeys(SshConnectionInfo host, string publicHostAddress, out string? wireguardConfs)
        {
            wireguardConfs = null;

            var assmblyPath = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory!.FullName;
            var genWireshardSh = File.ReadAllBytes(Path.Combine(assmblyPath, "Scripts", "gen-wireguard.sh"));
            var genWireguardPlaybookRunner = new PlaybookRunner(host, new GenWireguardConfPlaybook(publicHostAddress));
            genWireguardPlaybookRunner.Files[GenWireguardConfPlaybook.SCRIPT_PATH] = genWireshardSh;

            genWireguardPlaybookRunner.Run();

            if(!genWireguardPlaybookRunner.IsSuccess())
            {
                return false;
            }

            wireguardConfs = genWireguardPlaybookRunner.Artifacts[GenWireguardConfPlaybook.WIREGUARD_CLIENT_CONF].Trim();

            return wireguardConfs.Length > 0;
        }
    }
}
