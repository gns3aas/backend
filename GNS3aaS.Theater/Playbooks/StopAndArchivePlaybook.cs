﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class StopAndArchivePlaybook : IPlaybook
    {
        public const string GNS_PROJECTS_PATH = "/opt/gns3/projects";
        public const string FILE_SIZE_ARTIFACT_NAME = "filesize";


        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public StopAndArchivePlaybook(string userId) {
            var archiveFileName = $"/opt/project-archive/{userId}.tar.gz";
            orderList.Add(ExecutionOrderHelper.StopService("openvpn", false));
            orderList.Add(ExecutionOrderHelper.StopService("wg-quick@wg0", false));

            // Active connections to GNS3 from clients can cause gns3 not to stop. The following StopService commands failes and the session is marked broken.
            orderList.Add(ExecutionOrderHelper.Sleep(2000));
            orderList.Add(ExecutionOrderHelper.StopService("gns3", verifyExitStatus: false, doPostCheck: false));
            orderList.Add(ExecutionOrderHelper.StopService("gns3-server", verifyExitStatus: false, doPostCheck: false));
            // Usually it works the second time
            orderList.Add(ExecutionOrderHelper.Sleep(2000));
            orderList.Add(ExecutionOrderHelper.StopService("gns3-server"));
            orderList.Add(ExecutionOrderHelper.StopService("gns3"));

            orderList.Add(ExecutionOrderHelper.CreateFolder(LoadAndStartPlaybook.ARCHIVE_BASE_PATH));
            orderList.Add(ExecutionOrderHelper.ChownFolder("user", "user", LoadAndStartPlaybook.ARCHIVE_BASE_PATH));
            orderList.Add(ExecutionOrderHelper.CompressFolder(GNS_PROJECTS_PATH, archiveFileName));
            orderList.Add(ExecutionOrderHelper.GetFileSize(archiveFileName, FILE_SIZE_ARTIFACT_NAME));
            orderList.Add(ExecutionOrderHelper.DeleteFolderContents(GNS_PROJECTS_PATH));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
