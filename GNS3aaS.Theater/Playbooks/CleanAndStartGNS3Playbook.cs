﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class CleanAndStartGNS3Playbook : IPlaybook
    {
        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public CleanAndStartGNS3Playbook()
        {
            orderList.Add(ExecutionOrderHelper.StopService("gns3"));
            orderList.Add(ExecutionOrderHelper.StopService("gns3-server"));
            orderList.Add(ExecutionOrderHelper.DeleteFolderContents(StopAndArchivePlaybook.GNS_PROJECTS_PATH));
            orderList.Add(ExecutionOrderHelper.StartService("gns3"));
            orderList.Add(ExecutionOrderHelper.StartService("gns3-server"));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }

    }
}
