﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GNS3aaS.Controller.Migrations
{
    /// <inheritdoc />
    public partial class ResponsiblePersonEmail : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "ActiveGroups",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ResponsiblePersonEmail",
                table: "ActiveGroups",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "ActiveGroups");

            migrationBuilder.DropColumn(
                name: "ResponsiblePersonEmail",
                table: "ActiveGroups");
        }
    }
}
