﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    internal class DownloadOpenVPNKeyForSessionHandler : BaseHandler, IHandler
    {

        public DownloadOpenVPNKeyForSessionHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("openvpn", $"download openvpnkey for a session");

            var sessionIdArg = new Argument<string>("sessionId");

            var filePathOptions = new Option<string>("--path");

            command.AddArgument(sessionIdArg);
            command.AddOption(filePathOptions);

            command.SetHandler(async (sessionId, path) =>
            {
                await handle(sessionId, path);
            }, sessionIdArg, filePathOptions);
            return command;
        }

        private async Task handle(string sessionId, string filePath)
        {
            if (filePath == null)
            {
                filePath = $"{sessionId}.ovpn";
            }
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.GetSession(sessionId);
                if (response != null)
                {
                    File.WriteAllText(filePath, response.OpenVPNConfiguration);
                    Console.WriteLine(Path.GetFullPath(filePath));
                }
                else
                {
                    Console.WriteLine($"session download key failed");
                }
            }
        }
    }
}
