﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Helper;
using GNS3aaS.Controller.Testing;
using GNS3aaS.Lib.Authentification;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Tests
{
    public class CustomWebApplicationFactory<TProgram>
    : WebApplicationFactory<TProgram> where TProgram : class
    {
        internal DbTestHelper DbTestHelper { get; private set; }
        public IConfiguration Configuration { get; private set; }
        public string JwtToken { get; private set; }



        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            // TODO: Change this. This is only for Global.IsDevelopment
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Development");
            Environment.SetEnvironmentVariable("UNITTEST_ENVIRONMENT", "Enabled");

            builder.UseEnvironment("Development");

            builder.ConfigureServices(services =>
            {
                var dbContextDescriptor = services.SingleOrDefault(
                d => d.ServiceType ==
                    typeof(GNS3aaSDbContext));

                services.Remove(dbContextDescriptor);

                var config = ConfigurationHelper.InitConfiguration(useMain: true);

                Configuration = config;

                DbTestHelper = new DbTestHelper(config);

                JwtToken = JwtTokenHelper.GenerateNewToken(config, DbTestHelper.AdminUser.Id.ToString(), DbTestHelper.AdminEmail, DbTestHelper.AdminUser.GroupName, JwtUserRoles.Administrator);

                services.AddSingleton<IConfiguration>(config);

                var connectionString = "DataSource=myshareddb;mode=memory;cache=shared";
                var keepAliveConnection = new SqliteConnection(connectionString);
                keepAliveConnection.Open();

                // Create open SqliteConnection so EF won't automatically close it.
                services.AddSingleton<DbTestHelper>(DbTestHelper);
                services.AddSingleton<GNS3aaSDbContext>(DbTestHelper.DbContext);
                services.AddSingleton(keepAliveConnection);

            });

            
        }

        protected override void ConfigureClient(HttpClient client)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JwtToken);

            base.ConfigureClient(client);
        }
    }
}
