﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Users
{
    internal class SuperUserHandler : BaseHandler, IHandler
    {
        public SuperUserHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var subCommands = new Command("superuser");

            subCommands.Add(new DeleteSuperUserHandler(configStorage).GetCommand());
            subCommands.Add(new AddSuperUserHandler(configStorage).GetCommand());
            subCommands.Add(new UserStatsHandler(configStorage).GetCommand());  

            return subCommands;
        }
    }
}
