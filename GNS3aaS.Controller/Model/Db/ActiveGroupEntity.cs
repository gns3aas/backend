﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace GNS3aaS.Controller.Model.Db
{
    [Index(nameof(Name), IsUnique = true)]
    public class ActiveGroupEntity
    {

        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string ResponsiblePersonEmail { get; set; }

        public DateTime Created { get; set; }

    }
}
