﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    internal class CreateForUserHandler : BaseHandler, IHandler
    {
        public CreateForUserHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("create-for-user", $"create session for other user");

            var emailArg = new Argument<string>("email");

            command.AddArgument(emailArg);

            command.SetHandler(async (email) =>
            {
                await handle(email);
            }, emailArg);



            return command;
        }

        private async Task handle(string email)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.CreateSessionForUser(email);
                if (response.Success)
                {
                    Console.WriteLine($"{response.Data}");
                }
                else
                {
                    Console.WriteLine($"session creation failed: {response.Message}");
                }
            }
        }
    }
}