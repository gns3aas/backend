﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GNS3aaS.Controller.Migrations
{
    /// <inheritdoc />
    public partial class VPNPorts : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OpenVpnPort",
                table: "ServerNodes",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WireGuardPort",
                table: "ServerNodes",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OpenVpnPort",
                table: "ServerNodes");

            migrationBuilder.DropColumn(
                name: "WireGuardPort",
                table: "ServerNodes");
        }
    }
}
