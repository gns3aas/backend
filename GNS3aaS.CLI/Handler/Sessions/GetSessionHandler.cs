﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    public class GetSessionHandler : BaseHandler, IHandler
    {

        public GetSessionHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("get", $"get a session entity");

            var sessionIdArg = new Argument<string>("sessionId");

            command.AddArgument(sessionIdArg);

            command.SetHandler(async (sessionId) =>
            {
                await handle(sessionId);
            }, sessionIdArg);



            return command;
        }

        private async Task handle(string sessionId)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var session = await client.GetSession(sessionId);
                if (session != null)
                {
                    SessionsListHandler.PrintHeader();
                    Console.WriteLine(string.Format(SessionsListHandler.SessionLineFormat,
                        session.Id,
                        session.State,
                        session.KeepSession,
                        session.ActiveNodeName,
                        session.GNS3Version,
                        session.Started,
                        session.Updated,
                        session.Email,
                        session.Message
                        ));
                }
                else
                {
                    Console.WriteLine("Failed to get session");
                }
            }
        }
    }
}
