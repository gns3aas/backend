﻿using GNS3aaS.CLI.Services;
using GNS3aaS.Lib.Models.Response;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    internal class ServerNodesDeployHandler : ServerNodesAbstractActionHandler
    {
        public ServerNodesDeployHandler(ConfigStorage configStorage) : base("deploy", "deploy a node", configStorage)
        {
        }

        public override Task<DefaultResponse> DoApiRequest(string nodeId, GNS3aaSApiClient client)
        {
            return client.DeployNode(nodeId);
        }
    }
}