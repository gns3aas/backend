﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Helper;
using GNS3aaS.Controller.Providers;
using GNS3aaS.Controller.Services;
using GNS3aaS.Controller.Services.Notifications;
using GNS3aaS.Controller.Tasks;
using GNS3aaS.Lib.Authentification;
using GNS3aaS.Lib.Models.Requests;
using GNS3aaS.Lib.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace GNS3aaS.Controller.Controllers
{
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly GNS3aaSDbContext dbContext;
        private readonly ILogger<AuthController> logger;
        private readonly SecretProvider secretProvider;
        private readonly IConfiguration configuration;
        private readonly JwtTokenProvider tokenProvider;
        private readonly NotificationService notificationService;
        private readonly RateLimiterService rateLimiterService;

        public AuthController(GNS3aaSDbContext dbContext,
            ILogger<AuthController> _logger,
            SecretProvider secretProvider, IConfiguration configuration,
            JwtTokenProvider tokenProvider, NotificationService notificationService, RateLimiterService rateLimiterService)
        {
            this.dbContext = dbContext;
            logger = _logger;
            this.secretProvider = secretProvider;
            this.configuration = configuration;
            this.tokenProvider = tokenProvider;
            this.notificationService = notificationService;
            this.rateLimiterService = rateLimiterService;
        }

        [AllowAnonymous]
        [HttpPost("api/v1/login")]
        public IActionResult CreateSession([FromBody] LoginRequest createSessionRequest)
        {
            
            var ipAddress = RemoteAddressHelper.StripPortFromRemoteAddressIfAny(this.GetRemoteIpAddressOrDefault());
            if (rateLimiterService.TooManyBadRequest(ipAddress)) return BadRequest();
            logger.LogInformation($"New login request from {ipAddress}");

            var hashedEMail = secretProvider.HashEmail(createSessionRequest.Email);

            var user = dbContext.Users.SingleOrDefault(u => u.EMailHash == hashedEMail);

            if (user == null)
            {
                rateLimiterService.LogBadRequest(ipAddress);
                return NotFound(new DefaultResponse() { Success = false, Message = "User not found" });
            }

            if(rateLimiterService.TooManyEmailRequest(createSessionRequest.Email))
            {
                logger.LogWarning($"To many login requests for {ipAddress} {createSessionRequest.Email}");
                return StatusCode((int)HttpStatusCode.Unauthorized, new DefaultResponse() { Success = false, Message = $"Too many requests. Please wait {RateLimiterService.TIMEFRAME_MINUTES} minutes before trying again." });
            }

            if(user.UserRole == JwtUserRoles.User && !dbContext.ActiveGroups.Any(ag => ag.Name == user.GroupName))
            {
                return StatusCode((int)HttpStatusCode.Unauthorized, new DefaultResponse() { Success = false, Message = $"Group not authorized. Contact support." });
            }

            var tokenValidity = Global.TokenValidityInMinutes;
            if(user.UserRole == JwtUserRoles.Administrator)
            {
                tokenValidity = Global.TokenValidityInMinutesAdmin;
            }
            if(user.UserRole == JwtUserRoles.SuperUser)
            {
                tokenValidity = Global.TokenValidityInMinutesSuperuser;
            }

            user.JwtToken = tokenProvider.GenerateTokenFor(user.Id, createSessionRequest.Email, user.GroupName, user.UserRole, tokenValidity);
            user.JwtTokenValidUntil = DateTime.Now.AddDays(1);

            dbContext.SaveChanges();

            var loginMessage = $"Please use this login Link to access GNS3aaS:\n\n {configuration["LoginUrl"]}{user.JwtToken}";

            notificationService.SendNotification(createSessionRequest.Email, "GNS3aaS@TBZ - Login", loginMessage, loginMessage, Global.IsDevelopment);
            rateLimiterService.LogSentEmailRequest(createSessionRequest.Email);

            return Ok(new DefaultResponse() { Success = true, Message = "" });
        }

    }
}
