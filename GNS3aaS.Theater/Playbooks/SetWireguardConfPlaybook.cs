﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class SetWireguardConfPlaybook : IPlaybook
    {
        public const string WIREGUARD_CONFIG_PATH = "/etc/wireguard/wg0.conf";
        public const string WIREGUARD_CLIENT_CONF = "wireguardconf";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public SetWireguardConfPlaybook(string base64encodedServerConf)
        {
            orderList.Add(ExecutionOrderHelper.StopService("wg-quick@wg0", false));
            orderList.Add(ExecutionOrderHelper.DeleteFileIfExists(WIREGUARD_CONFIG_PATH));
            orderList.Add(ExecutionOrderHelper.SetFileContentFromBase64(WIREGUARD_CONFIG_PATH, base64encodedServerConf));
            orderList.Add(ExecutionOrderHelper.StartService("wg-quick@wg0"));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
