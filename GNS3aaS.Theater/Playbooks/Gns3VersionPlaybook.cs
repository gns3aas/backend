﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class Gns3VersionPlaybook : IPlaybook
    {
        public const string GNS3_VERSION_ARTIFACT = "gns3version";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public Gns3VersionPlaybook()
        {
            orderList.Add(ExecutionOrderHelper.Gns3Version(GNS3_VERSION_ARTIFACT));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
