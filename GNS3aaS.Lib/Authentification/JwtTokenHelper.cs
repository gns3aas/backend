﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace GNS3aaS.Lib.Authentification
{
    public class JwtTokenHelper
    {
        public const string USERID_CLAIM = "userid";
        public const string GROUP_CLAIM = "group";
        public const string EMAIL_CLAIM = "email";
        public const string ID_CLAIM = "id";

        public static string GenerateNewToken(string userId, string email, string groupName, string issuer, string audience, string keystr, JwtUserRoles role,
            int validForMinutes = 5, int validForMonths = 0)
        {
            var key = Encoding.ASCII.GetBytes
            (keystr);
            var claims = new List<Claim>()
                {
                new Claim(ID_CLAIM, Guid.NewGuid().ToString()),
                new Claim(USERID_CLAIM, userId),
                new Claim(GROUP_CLAIM, groupName),
                new Claim(EMAIL_CLAIM, email),
                new Claim(JwtRegisteredClaimNames.Jti,
                Guid.NewGuid().ToString()),
             };
            foreach(var r in Enum.GetValues(typeof(JwtUserRoles)).Cast<JwtUserRoles>().Where(jwr => jwr <= role).ToList())
            {
                claims.Add(new Claim(r.ToString(), "1"));
            }

            var expires = DateTime.UtcNow;
            if (validForMonths > 0) expires = expires.AddMonths(validForMonths);
            if (validForMinutes > 0) expires = expires.AddMinutes(validForMinutes);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims.ToArray()),
                Expires = expires,
                Issuer = issuer,
                Audience = audience,
                SigningCredentials = new SigningCredentials
                (new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha512Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = tokenHandler.WriteToken(token);
            var stringToken = tokenHandler.WriteToken(token);
            return stringToken;
        }

        public static string GenerateNewToken(IConfiguration configuration, string userId, string email, string groupName, JwtUserRoles role, int validForMinutes = 5, int validForMonths = 0)
        {
            var issuer = configuration["Jwt:Issuer"];
            var audience = configuration["Jwt:Audience"];
            var keystr = configuration["Jwt:Key"];
            return GenerateNewToken(userId, email, groupName, issuer, audience, keystr, role, validForMinutes, validForMonths);
        }
    }
}
