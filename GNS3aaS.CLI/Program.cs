﻿using GNS3aaS.CLI.Handler;
using GNS3aaS.CLI.Handler.Groups;
using GNS3aaS.CLI.Handler.Nodes;
using GNS3aaS.CLI.Handler.Sessions;
using GNS3aaS.CLI.Handler.Users;
using GNS3aaS.CLI.Services;
using System.CommandLine;

namespace GNS3aaS.CLI
{
    internal class Program
    {
        static int Main(string[] args)
        {
            var configStorage = ConfigStorage.CreateOrLoad();

            var loginHandler = new LoginHandler(configStorage);
            var tokenHandler = new TokenHandler(configStorage);
            var sessions = new SessionsHandler(configStorage);
            var serverNodes = new ServerNodesHandler(configStorage);
            var users = new UsersHandler(configStorage);
            var superusers = new SuperUserHandler(configStorage);
            var groups = new GroupsHandler(configStorage);

            var rootCommand = new RootCommand("GNS3aas CLI client")
            {
                loginHandler.GetCommand(),
                tokenHandler.GetCommand(),
                sessions.GetCommand(),
                serverNodes.GetCommand(),
                users.GetCommand(),
                superusers.GetCommand(),
                groups.GetCommand()
            };

            return rootCommand.InvokeAsync(args).Result;
        }
    }
}
