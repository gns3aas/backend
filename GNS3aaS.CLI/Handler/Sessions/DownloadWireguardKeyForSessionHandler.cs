﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    public class DownloadWireguardKeyForSessionHandler : BaseHandler, IHandler
    {

        public DownloadWireguardKeyForSessionHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("wireguard", $"download openvpnkey for a session");

            var sessionIdArg = new Argument<string>("sessionId");

            var filePathOptions = new Option<string>("--path");

            var offsetOption = new Option<int>("--offset", () => 0);

            command.AddArgument(sessionIdArg);
            command.AddOption(filePathOptions);

            command.SetHandler(async (sessionId, path, offset) =>
            {
                await handle(sessionId, path, offset);
            }, sessionIdArg, filePathOptions, offsetOption);
            return command;
        }

        private async Task handle(string sessionId, string filePath, int offset)
        {
            if (filePath == null)
            {
                filePath = $"{sessionId}-{offset}.conf";
            }
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.GetSession(sessionId);
                if (response != null)
                {
                    var wireguardKeys = response.WireguardVPNConfig.Trim().Replace("\r", "").Split("\n");
                    if(offset < 0 && offset >= wireguardKeys.Length-1)
                    {
                        Console.WriteLine("invalid offset");
                        return;
                    }
                    File.WriteAllText(filePath, Encoding.UTF8.GetString(Convert.FromBase64String(wireguardKeys[offset])));
                    Console.WriteLine(Path.GetFullPath(filePath));
                }
                else
                {
                    Console.WriteLine($"session download key failed");
                }
            }
        }
    }
}
