﻿using GNS3aaS.Controller;
using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Lib.Authentification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Tests.Models
{
    [TestClass]
    public class UserEntityTests
    {
        private HttpClient _client;

        public static readonly CustomWebApplicationFactory<Program> _factory = new CustomWebApplicationFactory<Program>();

        public static GNS3aaSDbContext Db
        {
            get
            {
                return _factory.DbTestHelper.DbContext;
            }
        }

        [TestInitialize]
        public void Init()
        {
            _client = _factory.CreateClient();
        }

        [TestMethod]
        public async Task T01_CreateUserEntity()
        {
            JwtTokenProvider jtp = new JwtTokenProvider(_factory.Configuration);
            var userID = Guid.NewGuid();
            var userEntity = new UserEntity()
            {
                Id = userID,
                EMailHash = "TheMagicHash",
                GroupName = "GroupNo1",
                JwtToken = jtp.GenerateTokenFor(userID, "TheMagicHash", "GroupNo1", JwtUserRoles.Default, validForMinutes: 24*60),
                JwtTokenValidUntil = DateTime.Now.AddDays(1)
            };
            Db.Users.Add(userEntity);
            Db.SaveChanges();
            var userEntityFromDb = Db.Users.Single(u => u.Id == userEntity.Id);
            Assert.AreEqual("TheMagicHash", userEntityFromDb.EMailHash);
        }
    }
}
