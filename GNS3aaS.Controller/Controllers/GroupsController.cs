﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Model.Extensions;
using GNS3aaS.Controller.Providers;
using GNS3aaS.Controller.Services;
using GNS3aaS.Lib.Authentification;
using GNS3aaS.Lib.Helper;
using GNS3aaS.Lib.Models.Requests;
using GNS3aaS.Lib.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GNS3aaS.Controller.Controllers
{
    [ApiController]
    public class GroupsController : ControllerBase
    {
        private readonly GNS3aaSDbContext dbContext;
        private readonly SecretProvider secretProvider;
        private readonly JwtTokenProvider tokenProvider;

        public GroupsController(GNS3aaSDbContext dbContext, SecretProvider secretProvider, JwtTokenProvider tokenProvider)
        {
            this.dbContext = dbContext;
            this.secretProvider = secretProvider;
            this.tokenProvider = tokenProvider;
        }


        [Authorize(Policy = "SuperUser")]
        [HttpGet("api/v1/groups")]
        public IActionResult Groups()
        {
            var groups = dbContext.Users.Select(u => u.GroupName).Distinct().ToList().Order().ToList();

            return Ok(groups);
        }

        [Authorize(Policy = "SuperUser")]
        [HttpGet("api/v1/active-groups")]
        public IActionResult ActiveGroups()
        {
            var activeGroupResponse = new ActiveGroupsResponse();
            
            activeGroupResponse.ActiveGroupNames = dbContext.ActiveGroups.OrderBy(i => i.Name).Select(u => u.Name).Distinct().ToList();
            activeGroupResponse.AvailableGroups = dbContext.Users.Select(u => u.GroupName).Distinct().ToList().Order().ToList();
            activeGroupResponse.ActiveGroups = dbContext.ActiveGroups.ToList().Select(a => a.ToPoco()).ToList();

            return Ok(activeGroupResponse);
        }

        [Authorize(Policy = "SuperUser")]
        [HttpPost("api/v1/active-group/add")]
        public IActionResult Groups([FromBody] GroupRequest groupRequest)
        {
            if (!dbContext.AuthentificateUser(HttpContext, out var eMail, out var userId, out var user))
            {
                return BadRequest();
            }
            if(user.UserRole != JwtUserRoles.Administrator && secretProvider.HashEmail(groupRequest.ResponsiblePersonEmail.Trim()) != user.EMailHash)
            {
                return BadRequest(new DefaultResponse() { Success = false, Message = "SuperUsers must authorize groups with their registered email address." });
            }
            if (string.IsNullOrWhiteSpace(groupRequest.Name) || groupRequest.Name.Length < 2 || groupRequest.Name.Length > 20)
            {
                return BadRequest(new DefaultResponse() { Success = false, Message = "group name invalid or too short or too long (2 - 20)" });
            }
            if(dbContext.ActiveGroups.Any(u => u.Name == groupRequest.Name))
            {
                return Ok(new DefaultResponse() { Success = false, Message = "group already active" });
            }
            if(!RegexUtilities.IsValidEmail(groupRequest.ResponsiblePersonEmail))
            {
                return BadRequest(new DefaultResponse() { Success = false, Message = "invalid email format" });
            }
            
            
            var groupEntity = new ActiveGroupEntity()
            {
                Id = Guid.NewGuid(),
                Name = groupRequest.Name,
                ResponsiblePersonEmail = groupRequest.ResponsiblePersonEmail,
                Created = DateTime.Now,
            };

            dbContext.ActiveGroups.Add(groupEntity);

            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "added group to active list", Data = groupEntity.Id});
        }

        [Authorize(Policy = "SuperUser")]
        [HttpDelete("api/v1/active-group/delete")]
        public IActionResult DeleteActiveGroup(string groupName)
        {
            var groupEntity = dbContext.ActiveGroups.FirstOrDefault(u => u.Name == groupName);

            if (groupEntity == null)
            {
                return Ok(new DefaultResponse() { Success = false, Message = "active group not found" });
            }

            dbContext.ActiveGroups.Remove(groupEntity);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = $"deleted active group", Data = groupEntity.Id });
        }



    }
}
