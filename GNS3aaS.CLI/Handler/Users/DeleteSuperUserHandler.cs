﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Users
{
    public class DeleteSuperUserHandler : BaseHandler, IHandler
    {
        public DeleteSuperUserHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("delete", $"delete a superuser by email");

            var emailArgs = new Argument<string>("email");

            command.AddArgument(emailArgs);

            command.SetHandler(async (email) =>
            {
                await handle(email);
            }, emailArgs);



            return command;
        }

        private async Task handle(string email)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.DeleteSuperUser(email);
                if (response.Success)
                {
                    Console.WriteLine(response.Data);
                }
                else
                {
                    Console.WriteLine($"super user delete failed {response.Message}");
                }
            }
        }
    }
}

