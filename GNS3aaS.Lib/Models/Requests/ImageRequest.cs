﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Models.Requests
{
    public class NewImageRequest
    {
        public string Name { get; set; }

        public string TemplateData { get; set; }

        public string DownloadUrl { get; set; }

        public string RemoteUnpackPath { get; set; }

    }
}
