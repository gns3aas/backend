﻿using GNS3aaS.CLI.Services;
using GNS3aaS.Lib.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    internal class ServerNodesReleaseHandler : ServerNodesAbstractActionHandler
    {
        public ServerNodesReleaseHandler(ConfigStorage configStorage) : base("release", "release a node", configStorage)
        {
        }

        public override Task<DefaultResponse> DoApiRequest(string nodeId, GNS3aaSApiClient client)
        {
            return client.ReleaseNode(nodeId);
        }
    }
}