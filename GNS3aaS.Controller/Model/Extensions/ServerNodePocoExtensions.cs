﻿using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Lib.Models.Pocos;

namespace GNS3aaS.Controller.Model.Extensions
{
    public static class ServerNodePocoExtensions
    {

        public static ServerNodePoco ToPoco(this ServerNodeEntity serverNodeEntity)
        {
            return new ServerNodePoco()
            {
                Id = serverNodeEntity.Id,
                HostAddress = serverNodeEntity.HostAddress,
                Name = serverNodeEntity.Name,
                ServerNodeState = serverNodeEntity.ServerNodeState,
                GNS3Version = serverNodeEntity.GNS3Version,
                LastStatsUpdate = serverNodeEntity.LastStatsUpdate,
                CPU = serverNodeEntity.CPU,
                RAM = serverNodeEntity.RAM,
                HDD = serverNodeEntity.HDD,
                MaasSystemId = serverNodeEntity.MaasSystemId,
                MaasState = serverNodeEntity.MaasState,
                OpenVpnPort = serverNodeEntity.OpenVpnPort,
                WireGuardPort = serverNodeEntity.WireGuardPort,
            };
        }

    }
}
