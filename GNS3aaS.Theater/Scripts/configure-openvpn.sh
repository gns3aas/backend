
HOSTNAME=`hostname`

MY_EXTERNAL_HOSTNAME="localhost"

port="1194"

mkdir -p /etc/openvpn/

echo "Create keys"

rm /etc/openvpn/dh.pem
rm /etc/openvpn/key.pem 
rm /etc/openvpn/csr.pem
rm /etc/openvpn/cert.pem

[ -f /etc/openvpn/dh.pem ] || openssl dhparam -out /etc/openvpn/dh.pem 2048
[ -f /etc/openvpn/key.pem ] || openssl genrsa -out /etc/openvpn/key.pem 2048
chmod 600 /etc/openvpn/key.pem
[ -f /etc/openvpn/csr.pem ] || openssl req -new -key /etc/openvpn/key.pem -out /etc/openvpn/csr.pem -subj /CN=OpenVPN/
[ -f /etc/openvpn/cert.pem ] || openssl x509 -req -in /etc/openvpn/csr.pem -out /etc/openvpn/cert.pem -signkey /etc/openvpn/key.pem -days 24855

echo "Create client configuration"
cat <<EOFCLIENT > /opt/gns3aas/client.ovpn
client
nobind
comp-lzo
dev tap
<key>
`cat /etc/openvpn/key.pem`
</key>
<cert>
`cat /etc/openvpn/cert.pem`
</cert>
<ca>
`cat /etc/openvpn/cert.pem`
</ca>
<connection>
remote $MY_EXTERNAL_HOSTNAME $port tcp4
</connection>
EOFCLIENT

cat <<EOFUDP > /etc/openvpn/server.conf
server-bridge
verb 3
duplicate-cn
comp-lzo
key key.pem
ca cert.pem
cert cert.pem
dh dh.pem
keepalive 10 60
persist-key
persist-tun
proto tcp
port 1194
dev tap1
status openvpn-status.log
log-append /var/log/openvpn.log
up "/bin/bash /etc/openvpn/bridge-start"
down "/bin/bash /etc/openvpn/bridge-stop"
EOFUDP

cat <<EOFBS > /etc/openvpn/bridge-start
#!/bin/bash
br="br0"
tap=\$1

for t in \$tap; do
    brctl addif \$br \$t
done

for t in \$tap; do
    ifconfig \$t 0.0.0.0 promisc up
done

EOFBS

cat <<EOFBS > /etc/openvpn/bridge-stop
#!/bin/bash
br="br0"
tap=\$1


EOFBS

echo "Restart OpenVPN"

set +e
systemctl daemon-reload
