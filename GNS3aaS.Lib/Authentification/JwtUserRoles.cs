﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Authentification
{
    public enum JwtUserRoles
    {
        Default = 0,
        User = 10,
        SuperUser = 80,
        Administrator = 100,
    }
}
