﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class StopVpnAndGNS3Playbook : IPlaybook
    {
        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public StopVpnAndGNS3Playbook()
        {
            orderList.Add(ExecutionOrderHelper.StopService("openvpn"));
            orderList.Add(ExecutionOrderHelper.StopService("gns3"));
            orderList.Add(ExecutionOrderHelper.StopService("gns3-server"));
            orderList.Add(ExecutionOrderHelper.StopService("wg-quick@wg0", false));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
