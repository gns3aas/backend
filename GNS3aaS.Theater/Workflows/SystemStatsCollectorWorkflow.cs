﻿using GNS3aaS.Theater.Playbooks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Workflows
{
    public class SystemStatsCollectorWorkflow : IWorkflow
    {
        public bool IsSuccessfull { get; set; } = false;

        public string Message { get; set; } = "";

        public bool IsFinished { get; set; } = false;

        public SshConnectionInfo Connection { get; }

        public string? CPU { get; private set; }
        public string? RAM { get; private set; }
        public string? HDD { get; private set; }


        public SystemStatsCollectorWorkflow(SshConnectionInfo connection)
        {
            Connection = connection;
        }

        public void Run()
        {
            var systemStatsPlaybook = new PlaybookRunner(Connection, new SystemStatsCollectorPlaybook());

            systemStatsPlaybook.Run();

            if (!systemStatsPlaybook.IsSuccess())
            {
                IsFinished = true;
                IsSuccessfull = false;
                return;
            }

            CPU = systemStatsPlaybook.Artifacts[SystemStatsCollectorPlaybook.CPU_ARTIFACT].Replace("\n", " ").Trim();
            RAM = systemStatsPlaybook.Artifacts[SystemStatsCollectorPlaybook.RAM_ARTIFACT].Replace("\n", " ").Trim();
            HDD = systemStatsPlaybook.Artifacts[SystemStatsCollectorPlaybook.HDD_ARTIFACT].Replace("\n", " ").Trim();

            IsFinished = true;
            IsSuccessfull = true;
        }
    }
}
