﻿using GNS3aaS.CLI.Services;
using GNS3aaS.Lib.Models.Requests;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Parsing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Users
{
    public class ImportUsersHandler : BaseHandler, IHandler
    {
        public ImportUsersHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("import", $"import users from csv (rows: e-mail, group)");

            var csvFilePath = new Argument<string>("csv file path");

            command.AddArgument(csvFilePath);

            command.SetHandler(async (csvFilePath) =>
            {
                await handle(csvFilePath);
            }, csvFilePath);



            return command;
        }

        private async Task handle(string csvFilePath)
        {
            if (!File.Exists(csvFilePath))
            {
                Console.WriteLine("File not found or not accessible");
                return;
            }

            List<UserEmailRequestPart> userToImport = new List<UserEmailRequestPart>();
            var textFileParser = new TextFieldParser(csvFilePath);
            textFileParser.TextFieldType = FieldType.Delimited;
            textFileParser.SetDelimiters(",");
            while (!textFileParser.EndOfData)
            {
                string[] fields = textFileParser.ReadFields();
                userToImport.Add(new UserEmailRequestPart()
                {
                    Email = fields[0],
                    Group = fields[1],
                });
            }
            Console.WriteLine($"Found {userToImport.Count} users");
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var result = await client.ImportUsers(new ImportUsersRequest() { Users = userToImport });
                if(result.Success)
                {
                    Console.WriteLine($"Imported {result.Data} users");
                }
                else
                {
                    Console.WriteLine($"Failed to import users {result.Message}");
                }
            }
        }
    }
}
