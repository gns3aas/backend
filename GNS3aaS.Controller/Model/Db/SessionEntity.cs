﻿using GNS3aaS.Lib.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace GNS3aaS.Controller.Model.Db
{
    public class SessionEntity
    {
        [Key]
        public Guid Id { get; set; }


        public SessionState State { get; set; }

        public Guid UserId { get; set; }

        [JsonIgnore]
        public virtual UserEntity User { get; set; }

        public string Email { get; set; }

        public string? Message { get; set; }

        public DateTime? Started { get; set; }

        public DateTime Updated { get; set; }

        public DateTime? ScheduledShutdown { get; set; }

        public Guid? ActiveNodeId { get; set; }

        public string? OpenVPNClientConfig { get; set; }

        public string? OpenVPNServerConfig { get; set; }

        public string? WireguardVPNConfig { get; set; }

        [JsonIgnore]
        public virtual ServerNodeEntity? ActiveNode { get; set; }

        public Guid? ArchiveLocationNodeId { get; set; }

        public string? ArchiveLocationNodeName { get; set; }

        public bool KeepSession { get; set; }

        public bool KeepRunning { get; set; }

        public int? ArchiveSize { get; set; }

    }
}
