﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler
{
    internal class LoginHandler : BaseHandler, IHandler
    {
        public LoginHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var hostArg = new Argument<string>("url", "base url of gns3aas controller");

            var emailArg = new Argument<string>("email", "email to login with");

            var loginCommand = new Command("login", "using email");
            loginCommand.AddArgument(hostArg);
            loginCommand.AddArgument(emailArg);

            loginCommand.SetHandler(async (url, email) =>
            {
                await handle(url, email);
            }, hostArg, emailArg);

            return loginCommand;
        }

        private async Task handle(string url, string email)
        {
            using (var client = new GNS3aaSApiClient(url))
            {
                var response = await client.Login(email);
                if(response.Success)
                {
                    Console.WriteLine("Requested Token via E-Mail");
                    configStorage.BaseUrl = url;
                    configStorage.Save();
                }
                else
                {
                    Console.WriteLine("Login failed");
                }
            }

        }

    }
}
