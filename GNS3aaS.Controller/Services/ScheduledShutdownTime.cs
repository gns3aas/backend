﻿using System.Globalization;
using System.Reflection.Metadata.Ecma335;

namespace GNS3aaS.Controller.Services
{
    public class ScheduledShutdownTimeProvider
    {
        private DateTime nextStop;

        public ScheduledShutdownTimeProvider(IConfiguration configuration)
        {
            Configuration = configuration;
            var stopSessionStr = Configuration["Schedule:StopSessionDaily"];
            if (stopSessionStr == null)
            {
                return;
            }
            nextStop = DateTime.ParseExact(stopSessionStr, "HH:mm", CultureInfo.InvariantCulture);
            IsShutdownTimeSet = true;
        }

        public bool IsShutdownTimeSet { get; private set; } = false;

        public DateTime NextScheduledShutdownTime()
        {
            while (DateTime.Now > nextStop)
            {
                nextStop = nextStop + TimeSpan.FromDays(1);
            }
            return nextStop;
        }

        public IConfiguration Configuration { get; }
    }
}
