﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Users
{
    internal class AddSuperUserHandler : BaseHandler, IHandler
    {
        public AddSuperUserHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("add", $"add a superuser by email");

            var emailArgs = new Argument<string>("email");

            var isAdminOptions = new Option<bool>("admin", () => false, "make it admin and not superuser");

            command.AddArgument(emailArgs);
            command.AddOption(isAdminOptions);

            command.SetHandler(async (email, isadmin) =>
            {
                await handle(email, isadmin);
            }, emailArgs, isAdminOptions);



            return command;
        }

        private async Task handle(string email, bool isadmin)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.AddSuperUser(email, isadmin);
                if (response.Success)
                {
                    Console.WriteLine(response.Data);
                }
                else
                {
                    Console.WriteLine($"superuser add failed: {response.Message}");
                }
            }
        }
    }
}
