﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Reflection.PortableExecutable;
using System.Xml.Linq;
using GNS3aaS.MAAS.Models;

namespace GNS3aaS.MAAS
{
    public class MAASApiClient : IDisposable
    {

        private readonly HttpClient Client;
        private string consumerKey;
        private string oAuthToken;
        private string oAuthSignature;

        public MAASApiClient(string baseUrl, string consumerKey, string oAuthToken, string oAuthSignature)
        {
            this.consumerKey = consumerKey;
            this.oAuthToken = oAuthToken;
            this.oAuthSignature = oAuthSignature;

            Client = new HttpClient();
            Client.BaseAddress = new Uri(baseUrl);
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public void updateAuthorization()
        {
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("OAuth", getOAuthAuthorization());
        }

        private string getOAuthAuthorization()
        {
            return $"oauth_version=1.0, oauth_signature_method=PLAINTEXT, oauth_consumer_key={consumerKey}, oauth_token={oAuthToken}, oauth_signature=&{oAuthSignature}, oauth_nonce={Guid.NewGuid()}, oauth_timestamp={DateTimeOffset.Now.ToUnixTimeSeconds()}";
        }

        public async Task<List<MaasMachine>> GetMachines()
        {
            updateAuthorization();
            var request = await Client.GetAsync("/MAAS/api/2.0/machines/");
            Console.WriteLine(request.StatusCode.ToString());
            if (request.StatusCode != System.Net.HttpStatusCode.OK) return null;
            var content = await request.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<List<MaasMachine>>(content, GetJsonSerializerOptions());
        }

        public async Task<MaasMachine> AllocateMachine(string name, string systemId)
        {
            updateAuthorization();
            var requestContent = new MultipartFormDataContent();
            requestContent.Add(new StringContent(name, Encoding.UTF8, MediaTypeNames.Text.Plain), "name");
            requestContent.Add(new StringContent(systemId, Encoding.UTF8, MediaTypeNames.Text.Plain), "system_id");
            var requestResult = await Client.PostAsync("/MAAS/api/2.0/machines/op-allocate", requestContent);
            if (requestResult.StatusCode != System.Net.HttpStatusCode.OK) return null;
            var content = await requestResult.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<MaasMachine>(content, GetJsonSerializerOptions());
        }

        public async Task<MaasMachine> DeployMachine(string systemId, string userData, string distroSeries = "jammy")
        {
            updateAuthorization();
            var requestContent = new MultipartFormDataContent();
            requestContent.Add(new StringContent(systemId, Encoding.UTF8, MediaTypeNames.Text.Plain), "system_id");
            requestContent.Add(new StringContent(userData, Encoding.UTF8, MediaTypeNames.Text.Plain), "user_data");
            requestContent.Add(new StringContent(distroSeries, Encoding.UTF8, MediaTypeNames.Text.Plain), "distro_series");
            var requestResult = await Client.PostAsync($"/MAAS/api/2.0/machines/{systemId}/op-deploy", requestContent);
            if (requestResult.StatusCode != System.Net.HttpStatusCode.OK) return null;
            var content = await requestResult.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<MaasMachine>(content, GetJsonSerializerOptions());
        }

        public async Task<MaasMachine> ReleaseMachine(string systemId)
        {
            updateAuthorization();
            var requestContent = new MultipartFormDataContent();
            requestContent.Add(new StringContent(systemId, Encoding.UTF8, MediaTypeNames.Text.Plain), "system_id");
            requestContent.Add(new StringContent("true", Encoding.UTF8, MediaTypeNames.Text.Plain), "erase");
            requestContent.Add(new StringContent("true", Encoding.UTF8, MediaTypeNames.Text.Plain), "quick_erase");
            var requestResult = await Client.PostAsync($"/MAAS/api/2.0/machines/{systemId}/op-release", requestContent);
            if (requestResult.StatusCode != System.Net.HttpStatusCode.OK) return null;
            var content = await requestResult.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<MaasMachine>(content, GetJsonSerializerOptions());
        }

        public async Task<MaasMachine> GetMachine(string systemId)
        {
            updateAuthorization();
            var requestResult = await Client.GetAsync($"/MAAS/api/2.0/machines/{systemId}");
            if (requestResult.StatusCode != System.Net.HttpStatusCode.OK) return null;
            var content = await requestResult.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<MaasMachine>(content, GetJsonSerializerOptions());
        }

        // GET /MAAS/api/2.0/machines/{system_id}/

        // POST 

        // POST /MAAS/api/2.0/machines/{system_id}/op-release

        private JsonSerializerOptions GetJsonSerializerOptions()
        {
            var enumConverter = new JsonStringEnumConverter();
            var jsonOptions = new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true,
            };
            jsonOptions.Converters.Add(enumConverter);
            return jsonOptions;
        }

        public void Dispose()
        {
            Client.Dispose();
        }


    }
}
