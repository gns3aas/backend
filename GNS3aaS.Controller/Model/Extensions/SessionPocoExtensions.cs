﻿using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Lib.Helper;
using GNS3aaS.Lib.Models.Pocos;

namespace GNS3aaS.Controller.Model.Extensions
{
    public static class SessionPocoExtensions
    {

        public static SessionPoco ToPoco(this SessionEntity sessionEntity)
        {
            return new SessionPoco()
            {
                Id = sessionEntity.Id,
                ActiveNodeId = sessionEntity.ActiveNodeId,
                State = sessionEntity.State,
                UserId = sessionEntity.UserId,
                ActiveNodeName = sessionEntity.ActiveNode?.Name,
                ArchiveLocationNodeId = sessionEntity.ArchiveLocationNodeId,
                ArchiveLocationNodeName = sessionEntity.ArchiveLocationNodeName,
                KeepSession = sessionEntity.KeepSession,
                Email = sessionEntity.Email,
                Message = sessionEntity.Message,
                OpenVPNConfiguration = sessionEntity.OpenVPNClientConfig,
                GNS3Version = sessionEntity.ActiveNode?.GNS3Version,
                KeepRunning = sessionEntity.KeepRunning,
                Updated = sessionEntity.Updated,
                Started = sessionEntity.Started,
                ScheduledShutdown = sessionEntity.ScheduledShutdown,
                ArchiveSize = sessionEntity.ArchiveSize,
                WireguardVPNConfig = WireguardStringConfigHelper.RemoveServerKey(sessionEntity.WireguardVPNConfig)
            };
        }

    }
}
