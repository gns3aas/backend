﻿using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Lib.Models.Pocos;

namespace GNS3aaS.Controller.Model.Extensions
{
    public static class ActiveGroupPocoExtensions
    {

        public static ActiveGroupPoco ToPoco(this ActiveGroupEntity entity)
        {
            return new ActiveGroupPoco()
            {
                Created = entity.Created,
                Id = entity.Id,
                Name = entity.Name,
                ResponsiblePersonEmail = entity.ResponsiblePersonEmail,
            };
        }

    }
}
