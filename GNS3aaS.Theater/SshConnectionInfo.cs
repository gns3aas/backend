﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater
{
    public class SshConnectionInfo
    {

        public string Host { get; set; }

        public string Username { get; set; }

        public string SshKey { get; set; }

        public SshConnectionInfo(string host, string username, string sshKey)
        {
            Host = host;
            Username = username;
            SshKey = sshKey;
        }

        public SshConnectionInfo CopyWithNewHost(string newHost)
        {
            return new SshConnectionInfo(newHost,Username,SshKey);
        }

        public override string ToString()
        {
            return $"{Username}@{Host}";
        }

        public override bool Equals(object? obj)
        {
            if(obj is SshConnectionInfo)
            {
                var other = (SshConnectionInfo)obj;
                return other.Host == Host && other.Username == Username && other.SshKey == SshKey;
            }
            return base.Equals(obj);
        }
    }
}
