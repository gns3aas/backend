﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class RemovePublicKeyToAuthorizedKeysPlaybook : IPlaybook
    {

        public const string SSH_AUTHORIZED_KEYS_PATH = ".ssh/authorized_keys";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public RemovePublicKeyToAuthorizedKeysPlaybook(string publicKey)
        {
            orderList.Add(ExecutionOrderHelper.RemovePublicKeyFromFile(publicKey.Trim(), SSH_AUTHORIZED_KEYS_PATH));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }

    }
}
