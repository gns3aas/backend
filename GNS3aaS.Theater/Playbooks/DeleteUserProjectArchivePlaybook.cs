﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class DeleteUserProjectArchivePlaybook : IPlaybook
    {

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public DeleteUserProjectArchivePlaybook(string userId)
        {
            var archivePath = $"{RemoteCopyUserProjectPlaybook.ARCHIVE_PATH}{userId}{LoadAndStartPlaybook.ARCHIVE_FILE_ENDING}";
            orderList.Add(ExecutionOrderHelper.DeleteFile(archivePath));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
