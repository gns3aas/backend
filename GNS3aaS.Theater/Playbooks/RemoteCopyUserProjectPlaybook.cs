﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class RemoteCopyUserProjectPlaybook : IPlaybook
    {
        public const string ARCHIVE_PATH = "/opt/project-archive/";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public RemoteCopyUserProjectPlaybook(string userId, SshConnectionInfo host)
        {
            var archiveFile = $"{ARCHIVE_PATH}{userId}.tar.gz";
            orderList.Add(ExecutionOrderHelper.Md5SumOfFile(archiveFile));
            orderList.Add(ExecutionOrderHelper.SshCopyFromLocalToRemote(archiveFile, host, ARCHIVE_PATH));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }


    }
}
