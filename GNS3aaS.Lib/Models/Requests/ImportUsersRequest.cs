﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Lib.Models.Requests
{
    public class ImportUsersRequest
    {

        public List<UserEmailRequestPart> Users { get; set; }

    }
}
