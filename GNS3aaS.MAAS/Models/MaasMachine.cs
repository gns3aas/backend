﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace GNS3aaS.MAAS.Models
{
    public class MaasMachine
    {

        [JsonPropertyName("status_message")]
        public string StatusMessage { get; set; }

        [JsonPropertyName("status_name")]
        public string StatusName { get; set; }

        [JsonPropertyName("fqdn")]
        public string FQDN { get; set; }

        [JsonPropertyName("power_state")]
        public string PowerState { get; set; }

        [JsonPropertyName("hostname")]
        public string HostName { get; set; }

        [JsonPropertyName("status")]
        public int Status { get; set; }

        [JsonPropertyName("system_id")]
        public string SystemId { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        public const int STATUS_READY = 4;
        public const int STATUS_DEPLOYED = 6;
        public const int ALLOCATED = 10;
    }
}
