﻿namespace GNS3aaS.Controller.Services.ServerNodeLocking
{
    public class ServerNodeLock
    {

        public Guid SessionId { get; set; }

        public Guid? FromNodeId { get; set; }

        public Guid OnNodeId { get; set; }

    }
}
