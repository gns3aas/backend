﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class AddPublicKeyToAuthorizedKeysPlaybook : IPlaybook
    {

        public const string SSH_AUTHORIZED_KEYS_PATH = ".ssh/authorized_keys";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public AddPublicKeyToAuthorizedKeysPlaybook(string publicKey)
        {
            // Ugly hack: If the server has no archives, the next Remote Copy task will fail, we need to ensure the folder created
            orderList.Add(ExecutionOrderHelper.CreateFolder(LoadAndStartPlaybook.ARCHIVE_BASE_PATH));
            orderList.Add(ExecutionOrderHelper.ChownFolder("user", "user", LoadAndStartPlaybook.ARCHIVE_BASE_PATH));
            orderList.Add(ExecutionOrderHelper.AppendToExistingFile(publicKey.Trim(), SSH_AUTHORIZED_KEYS_PATH));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }

    }
}
