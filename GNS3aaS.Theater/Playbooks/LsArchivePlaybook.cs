﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    internal class LsArchivePlaybook : IPlaybook
    {
        public const string LS_ARTIFACT = "ls";


        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public LsArchivePlaybook()
        {
            orderList.Add(ExecutionOrderHelper.Ls(LoadAndStartPlaybook.ARCHIVE_BASE_PATH, LS_ARTIFACT));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
