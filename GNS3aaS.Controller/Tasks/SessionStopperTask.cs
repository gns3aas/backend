﻿
using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Services;
using GNS3aaS.Theater;
using GNS3aaS.Theater.Workflows;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace GNS3aaS.Controller.Tasks
{
    public class SessionStopperTask : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly ILogger<SessionStopperTask> logger;
        private readonly SshConnectionInfoProvider sshConnectionInfoProvider;

        public IServiceProvider Services { get; }
        public IConfiguration Configuration { get; }

        private bool isRunning = false;

        public SessionStopperTask(ILogger<SessionStopperTask> _logger, IServiceProvider services, IConfiguration configuration, SshConnectionInfoProvider sshConnectionInfoProvider)
        {
            logger = _logger;
            Services = services;
            Configuration = configuration;
            this.sshConnectionInfoProvider = sshConnectionInfoProvider;
        }

        public void Dispose()
        {
            _timer?.Change(Timeout.Infinite, 0);
            _timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(tryDoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(5));

            logger.LogInformation("SessionStopperTask is started.");

            return Task.CompletedTask;
        }

        private void tryDoWork(object? state)
        {
            try
            {
                if (isRunning) return;
                isRunning = true;
                doWork(state);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    throw ex;
                }
                logger.LogError($"SessionStopperTask {ex.Message}", ex);
            }
            isRunning = false;
        }

        private void doWork(object? state)
        {
            using(var dbContext = new GNS3aaSDbContext())
            {
                var sessions = dbContext.Sessions.Where(
                    s => s.State == Lib.Models.Enums.SessionState.STOP_REQUESTED
                      || s.State == Lib.Models.Enums.SessionState.STOPPING
                    ).ToList();

                if (!sessions.Any()) return;

                logger.LogInformation($"Found {sessions.Count} sessions ready to stop");

                foreach(var session in sessions)
                {
                    HandleSession(dbContext, session);
                }
            }
        }

        private void HandleSession(GNS3aaSDbContext dbContext, SessionEntity session)
        {
            logger.LogInformation($"Stopping session {session.Id} for user {session.Email}");
            session.State = Lib.Models.Enums.SessionState.STOPPING;
            dbContext.SaveChanges();

            if(session.ActiveNode == null)
            {
                logger.LogError($"session {session.Id} has no active node. cannot stop it.");
                session.State = Lib.Models.Enums.SessionState.BROKEN;
                session.Updated = DateTime.Now;
                dbContext.SaveChanges();
                return;
            }

            var connectionInfo = sshConnectionInfoProvider.GetConnectionInfo(session.ActiveNode!);

            var stopWorkflow = new StopAndArchiveSessionWorkflow(session.Id.ToString(), connectionInfo);

            stopWorkflow.Run();

            var assignedNode = session.ActiveNode;

            if (stopWorkflow.IsSuccessfull)
            {
                session.State = Lib.Models.Enums.SessionState.IDLE; 
                session.Updated = DateTime.Now;
                session.ArchiveLocationNodeId = session.ActiveNodeId;
                session.ArchiveLocationNodeName = session.ActiveNode?.Name;
                session.ActiveNode = null;
                session.ActiveNodeId = null;
                session.Started = null;
                session.ArchiveSize = stopWorkflow.ArchiveFileSize;
                assignedNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Available;
                logger.LogInformation($"Stopped and archived session {session.Id} for user {session.Email}");
            }
            else
            {
                session.State = Lib.Models.Enums.SessionState.BROKEN;
                session.Message = stopWorkflow.Message;
                session.Updated = DateTime.Now;
                assignedNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
                logger.LogError($"Stopping session {session.Id} failed");
            }
            dbContext.SaveChanges();
        }

        

        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("SessionStopperTask is stopped");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
