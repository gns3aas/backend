﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Users
{
    internal class UserStatsHandler : BaseHandler, IHandler
    {
        public const string LineFormat = "{0,20} = {1}";
        public UserStatsHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("stats", $"get number of users in the db by userrole");

            command.SetHandler(async () =>
            {
                await handle();
            });



            return command;
        }

        private async Task handle()
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.UsersStats();
                if(response != null)
                {
                    Console.WriteLine(string.Format(LineFormat, "NumOfAdministrators", response.NumOfAdministrators));
                    Console.WriteLine(string.Format(LineFormat, "NumOfSuperUsers", response.NumOfSuperUsers));
                    Console.WriteLine(string.Format(LineFormat, "NumOfUsers", response.NumOfUsers));
                    Console.WriteLine(string.Format(LineFormat, "Total", response.Total));
                }
            }
        }
    }
}

