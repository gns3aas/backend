﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GNS3aaS.Controller.Migrations
{
    /// <inheritdoc />
    public partial class NodeStats : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CPU",
                table: "ServerNodes",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HDD",
                table: "ServerNodes",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastStatsUpdate",
                table: "ServerNodes",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RAM",
                table: "ServerNodes",
                type: "TEXT",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CPU",
                table: "ServerNodes");

            migrationBuilder.DropColumn(
                name: "HDD",
                table: "ServerNodes");

            migrationBuilder.DropColumn(
                name: "LastStatsUpdate",
                table: "ServerNodes");

            migrationBuilder.DropColumn(
                name: "RAM",
                table: "ServerNodes");
        }
    }
}
