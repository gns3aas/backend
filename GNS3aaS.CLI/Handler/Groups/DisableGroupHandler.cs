﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Groups
{
    public class DisableGroupHandler : BaseHandler, IHandler
    {
        public DisableGroupHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("disable", $"disable service for this group");

            var groupArgs = new Argument<string>("group");

            command.AddArgument(groupArgs);

            command.SetHandler(async (group) =>
            {
                await handle(group);
            }, groupArgs);



            return command;
        }

        private async Task handle(string group)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.DeleteActiveGroup(group);
                if (response.Success)
                {
                    Console.WriteLine(response.Data);
                }
                else
                {
                    Console.WriteLine($"group disable failed: {response.Message}");
                }
            }
        }
    }
}