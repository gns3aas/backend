﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Model.Extensions;
using GNS3aaS.Lib.Models.Requests;
using GNS3aaS.Lib.Models.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GNS3aaS.Controller.Controllers
{
    [ApiController]
    public class ApplianceImageController : ControllerBase
    {

        private readonly GNS3aaSDbContext dbContext;

        public ApplianceImageController(GNS3aaSDbContext dbContext)
        {
            this.dbContext = dbContext;
        }


        [Authorize(Policy = "Administrator")]
        [HttpGet("api/v1/images")]
        public IActionResult Images()
        {
            var appliances = dbContext.ApplianceImages.ToList().Select(a => a.ToPoco()).ToList();

            return Ok(appliances);
        }

        [Authorize(Policy = "Administrator")]
        [HttpPost("api/v1/images/add")]
        public IActionResult Groups([FromBody] NewImageRequest request)
        {
            if (dbContext.ApplianceImages.Any(u => u.Name == request.Name))
            {
                return Ok(new DefaultResponse() { Success = false, Message = "template with same name already exists" });
            }

            var appliaceEntity = new ApplianceImageEntity()
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                DownloadUrl = request.DownloadUrl,
                TemplateData = request.TemplateData,
                RemoteUnpackPath = request.RemoteUnpackPath,
            };

            dbContext.ApplianceImages.Add(appliaceEntity);

            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = "added image", Data = appliaceEntity.Id });
        }

        [Authorize(Policy = "Administrator")]
        [HttpDelete("api/v1/images/delete/{id}")]
        public IActionResult DeleteActiveGroup(Guid id)
        {
            var applianceEntity = dbContext.ApplianceImages.SingleOrDefault(a => a.Id == id);

            if (applianceEntity == null)
            {
                return Ok(new DefaultResponse() { Success = false, Message = "image not found" });
            }

            dbContext.ApplianceImages.Remove(applianceEntity);
            dbContext.SaveChanges();

            return Ok(new DefaultResponse() { Success = true, Message = $"deleted image template", Data = applianceEntity.Id });
        }

    }
}
