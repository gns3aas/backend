﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class VerifyMd5SumPlaybook : IPlaybook
    {
        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public VerifyMd5SumPlaybook(string md5SumWithPath)
        {
            orderList.Add(ExecutionOrderHelper.VerifyMd5Sum(md5SumWithPath));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
