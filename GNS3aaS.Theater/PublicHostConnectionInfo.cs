﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater
{
    public class PublicHostConnectionInfo
    {
        public string HostName { get; set; }

        public int OpenVpnPort { get; set; }

        public int WireGuardPort { get; set; }

    }
}
