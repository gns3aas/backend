﻿using GNS3aaS.CLI.Services;
using GNS3aaS.Lib.Models.Response;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    public abstract class ServerNodesAbstractActionHandler : BaseHandler, IHandler
    {
        private readonly string commandStr;
        private readonly string description;

        public ServerNodesAbstractActionHandler(string commandStr, string description, ConfigStorage configStorage) : base(configStorage)
        {
            this.commandStr = commandStr;
            this.description = description;
        }

        public Command GetCommand()
        {
            var command = new Command(commandStr, description);

            var nodeIdArgs = new Argument<string>("nodeId");

            command.AddArgument(nodeIdArgs);

            command.SetHandler(async (nodeId) =>
            {
                await handle(nodeId);
            }, nodeIdArgs);

            return command;
        }

        private async Task handle(string nodeId)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                DefaultResponse response = await DoApiRequest(nodeId, client);
                if (response.Success)
                {
                    Console.WriteLine($"{nodeId}");
                }
                else
                {
                    Console.WriteLine($"node {nodeId} {commandStr} FAILED: {response.Message}");
                }
            }
        }

        public abstract Task<DefaultResponse> DoApiRequest(string nodeId, GNS3aaSApiClient client);
    }
}