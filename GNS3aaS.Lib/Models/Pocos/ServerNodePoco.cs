﻿
using GNS3aaS.Lib.Models.Enums;

namespace GNS3aaS.Lib.Models.Pocos
{
    public class ServerNodePoco
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string HostAddress { get; set; }

        public ServerNodeState ServerNodeState { get; set; }

        public string? GNS3Version { get; set; }

        public string? CPU { get; set; }

        public string? RAM { get; set; }

        public string? HDD { get; set; }

        public int OpenVpnPort { get; set; }

        public int WireGuardPort { get; set; }

        public DateTime? LastStatsUpdate { get; set; }

        public string? MaasSystemId { get; set; }

        public int MaasState { get; set; }  


    }
}
