﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GNS3aaS.Controller.Migrations
{
    /// <inheritdoc />
    public partial class SessionArchiveFileSize : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ArchiveSize",
                table: "Sessions",
                type: "INTEGER",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArchiveSize",
                table: "Sessions");
        }
    }
}
