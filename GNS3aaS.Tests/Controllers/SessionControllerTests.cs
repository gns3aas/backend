﻿using GNS3aaS.Controller;
using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Testing;
using GNS3aaS.Lib.Models.Requests;
using GNS3aaS.Lib.Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Tests.Controllers
{
    [TestClass]
    public class SessionControllerTests
    {
        private HttpClient _client;

        public static readonly CustomWebApplicationFactory<Program> _factory = new CustomWebApplicationFactory<Program>();

        public static GNS3aaSDbContext Db
        {
            get
            {
                return _factory.DbTestHelper.DbContext;
            }
        }

        [TestInitialize]
        public void Init()
        {
            _client = _factory.CreateClient();
        }

        [TestMethod]
        public async Task SessionControllerTest()
        {
            var content = new LoginRequest() { Email = DbTestHelper.AdminEmail };
            var resp_hosts = await _client.PostAsync($"api/v1/session/create", TestHelper.SerializeToHttpContent(content));

            Assert.AreEqual(System.Net.HttpStatusCode.OK, resp_hosts.StatusCode);

            var rawContent = await resp_hosts.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<DefaultResponse>(rawContent);
            Assert.IsTrue(response.Success);
            var testUser = Db.Users.Single(u => u.Id == DbTestHelper.TestUser1.Id);
            Assert.IsNotNull(testUser.JwtToken);
        }

    }
}
