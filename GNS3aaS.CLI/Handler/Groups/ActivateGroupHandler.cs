﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Groups
{
    internal class ActivateGroupHandler : BaseHandler, IHandler
    {
        public ActivateGroupHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("activate", $"activate service for this group");

            var groupArgs = new Argument<string>("group");

            var responsibleArgs = new Argument<string>("responsible", "Email address of the person responsible for the group.");

            command.AddArgument(groupArgs);
            command.AddArgument(responsibleArgs);

            command.SetHandler(async (group, responsible) =>
            {
                await handle(group, responsible);
            }, groupArgs, responsibleArgs);

            return command;
        }

        private async Task handle(string group, string responsible)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.AddActiveGroup(group, responsible);
                if (response.Success)
                {
                    Console.WriteLine(response.Data);
                }
                else
                {
                    Console.WriteLine($"group activation failed: {response.Message}");
                }
            }
        }
    }
}

