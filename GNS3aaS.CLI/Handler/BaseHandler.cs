﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler
{
    public abstract class BaseHandler
    {
        protected readonly ConfigStorage configStorage;

        public BaseHandler(ConfigStorage configStorage)
        {
            this.configStorage = configStorage;
        }

    }
}
