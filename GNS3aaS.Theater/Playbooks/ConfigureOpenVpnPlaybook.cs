﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class ConfigureOpenVpnPlaybook : IPlaybook
    {
        public const string SCRIPT_PATH = "/opt/gns3aas/configure-openvpn.sh";
        public const string OPENVPN_FILE_PATH = "/opt/gns3aas/client.ovpn";
        public const string OPENVPN_ETC_FOLDER = "/etc/openvpn";
        public const string OPENVPN_BACKUP_FILE = "/opt/gns3aas/openvpn.tar.gz";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public ConfigureOpenVpnPlaybook()
        {
            orderList.Add(ExecutionOrderHelper.StopService("openvpn"));
            orderList.Add(ExecutionOrderHelper.CreateFolder("/opt/gns3aas"));
            orderList.Add(ExecutionOrderHelper.UploadFile(SCRIPT_PATH));
            orderList.Add(ExecutionOrderHelper.FixLineEndings(SCRIPT_PATH));
            orderList.Add(ExecutionOrderHelper.ExecuteBashScript(SCRIPT_PATH));
            orderList.Add(ExecutionOrderHelper.StartService("openvpn"));
            orderList.Add(ExecutionOrderHelper.DownloadFile(OPENVPN_FILE_PATH));
            orderList.Add(ExecutionOrderHelper.CompressFolder(OPENVPN_ETC_FOLDER, OPENVPN_BACKUP_FILE));
            orderList.Add(ExecutionOrderHelper.DownloadFile(OPENVPN_BACKUP_FILE));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
