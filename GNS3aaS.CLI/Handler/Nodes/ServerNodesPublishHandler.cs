﻿using GNS3aaS.CLI.Services;
using GNS3aaS.Lib.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    internal class ServerNodesPublishHandler : ServerNodesAbstractActionHandler
    {
        public ServerNodesPublishHandler(ConfigStorage configStorage) : base("publish", "make a node available", configStorage)
        {
        }

        public override Task<DefaultResponse> DoApiRequest(string nodeId, GNS3aaSApiClient client)
        {
            return client.PublishNode(nodeId);
        }
    }
}