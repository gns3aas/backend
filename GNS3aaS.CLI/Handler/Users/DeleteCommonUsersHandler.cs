﻿using GNS3aaS.CLI.Services;
using GNS3aaS.Lib.Models.Requests;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Users
{
    internal class DeleteCommonUsersHandler : BaseHandler, IHandler
    {
        public DeleteCommonUsersHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("delete-common-users", $"delete all users with UserRole = User");

            command.SetHandler(async () =>
            {
                await handle();
            });
            return command;
        }

        private async Task handle()
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var result = await client.DeleteCommonUsers();
                if (result.Success)
                {
                    Console.WriteLine($"Deleted {result.Data} users with UserRole = User");
                }
                else
                {
                    Console.WriteLine($"Failed to delete users: {result.Message}");
                }
            }
        }
    }
}
