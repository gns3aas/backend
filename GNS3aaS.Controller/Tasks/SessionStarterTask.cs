﻿
using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Services;
using GNS3aaS.Controller.Services.ServerNodeLocking;
using GNS3aaS.Lib.Helper;
using GNS3aaS.Theater;
using GNS3aaS.Theater.Workflows;
using Microsoft.Extensions.Logging;
using Renci.SshNet;
using Renci.SshNet.Common;
using System.Diagnostics;

namespace GNS3aaS.Controller.Tasks
{
    public class SessionStarterTask : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly ILogger<SessionStarterTask> logger;
        private readonly SshConnectionInfoProvider sshConnectionInfoProvider;
        private readonly ScheduledShutdownTimeProvider scheduledShutdownTimeProvider;
        public const int MAX_NUMBER_OF_PARALLEL_SESSION_STARTS = 5;
        public IServiceProvider Services { get; }
        public IConfiguration Configuration { get; }

        private bool isRunning = false;

        public ServerNodeLocks ServerNodeLocks { get; private set; }
        public string PublicHostName { get; private set; }

        public SessionStarterTask(ILogger<SessionStarterTask> _logger, IServiceProvider services
            , IConfiguration configuration, SshConnectionInfoProvider sshConnectionInfoProvider, ScheduledShutdownTimeProvider scheduledShutdownTimeProvider)
        {
            logger = _logger;
            Services = services;
            Configuration = configuration;
            this.sshConnectionInfoProvider = sshConnectionInfoProvider;
            this.scheduledShutdownTimeProvider = scheduledShutdownTimeProvider;
            ServerNodeLocks = new ServerNodeLocks();
        }

        public void Dispose()
        {
            _timer?.Change(Timeout.Infinite, 0);
            _timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            PublicHostName = "localhost";
            if (!string.IsNullOrWhiteSpace(Configuration["PublicHostName"]))
            {
                PublicHostName = Configuration["PublicHostName"];
            }
            else
            {
                logger.LogError($"Missing PublicHostName in configuration. Using {PublicHostName}");
            }
            
            _timer = new Timer(tryDoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(15));

            logger.LogInformation("SessionManagerTask is started.");

            return Task.CompletedTask;
        }

        private void tryDoWork(object? state)
        {
            try
            {
                if (isRunning) return;
                isRunning = true;
                doWork(state);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    throw ex;
                }
                logger.LogError($"SessionManagerTask {ex.Message}", ex);
            }
            isRunning = false;
        }

        private void doWork(object? state)
        {
            using(var dbContext = new GNS3aaSDbContext())
            {
                var sessions = dbContext.Sessions.Where(s => s.State == Lib.Models.Enums.SessionState.START_REQUESTED).ToList();

                if (!sessions.Any()) return;

                logger.LogInformation($"Found {sessions.Count} sessions ready to start");

                int counter = 0;

                foreach(var session in sessions)
                {
                    if(NumberOfActiveThreads < MAX_NUMBER_OF_PARALLEL_SESSION_STARTS)
                        HandleSession(dbContext, session);
                }
            }
        }
        
        private int NumberOfActiveThreads = 0;

        private void HandleSessionStartTask(Guid sessionId, Guid assignedNodeId, Guid? lastNodeId)
        {
            Interlocked.Increment(ref NumberOfActiveThreads);
            var thread = new Thread(() =>
            {
                try
                {
                    using (var dbContext = new GNS3aaSDbContext()) {
                        var session = dbContext.Sessions.Single(s => s.Id == sessionId);
                        var assignedNode = dbContext.ServerNodes.Single(s => s.Id == assignedNodeId);
                        if (lastNodeId != null && session.ArchiveLocationNodeId != null)
                        {
                            logger.LogInformation($"Restoring session {session.Id} for user {session.Email}");
                            var lastNode = dbContext.ServerNodes.Single(s => s.Id == lastNodeId);
                            HandleRestoreSession(dbContext, session, assignedNode, lastNode);
                        }
                        else
                        {
                            logger.LogInformation($"Creating session {session.Id} for user {session.Email}");
                            HandleNewSession(dbContext, session, assignedNode);
                        }
                    }
                }catch(Exception e)
                {
                    logger.LogError(e, $"handle session failed ({sessionId}, assigned node {assignedNodeId}, lastNode {lastNodeId})");
                    tryUpdateSessionToBroken(sessionId, assignedNodeId, lastNodeId, $"{e.GetType().Name} {e.Message}");
                }
                ServerNodeLocks.Release(sessionId);
                Interlocked.Decrement(ref NumberOfActiveThreads);
            });
            thread.Start();
        }

        private void tryUpdateSessionToBroken(Guid sessionId, Guid assignedNodeId, Guid? lastNodeId, string message)
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var session = dbContext.Sessions.Single(s => s.Id == sessionId);
                session.Message = message;
                session.Updated = DateTime.UtcNow;
                session.Started = null;
                session.State = Lib.Models.Enums.SessionState.BROKEN;
                var assignedNode = dbContext.ServerNodes.Single(s => s.Id == assignedNodeId);
                assignedNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
                if(lastNodeId != null)
                {
                    var lastNode = dbContext.ServerNodes.Single(s => s.Id == lastNodeId);
                    lastNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
                }
                dbContext.SaveChanges();
            }
        }

        private void HandleSession(GNS3aaSDbContext dbContext, SessionEntity session)
        {
            ServerNodeEntity? assignedNode = null;
            ServerNodeEntity? lastNode = null;
            if (session.ArchiveLocationNodeId != null)
            {
                lastNode = dbContext.ServerNodes.SingleOrDefault(s => s.Id == session.ArchiveLocationNodeId);
                if (lastNode != null && lastNode.ServerNodeState == Lib.Models.Enums.ServerNodeState.Available)
                {
                    assignedNode = lastNode;
                }
                if(lastNode == null)
                {
                    logger.LogError($"node that contains project archive not found for session {session.Id}");
                    session.State = Lib.Models.Enums.SessionState.BROKEN;
                    session.Message = "node that contains project archive not found";
                    dbContext.SaveChanges();
                    return;
                }
            }
            if(assignedNode == null)
            {
                var rnd = new Random();
                assignedNode = dbContext.ServerNodes.Where(s => s.ServerNodeState == Lib.Models.Enums.ServerNodeState.Available)
                    .ToList().OrderBy(x => rnd.Next()).FirstOrDefault();
            }
            if (assignedNode == null)
            {
                logger.LogError($"No server available for session {session.Id}");
                session.State = Lib.Models.Enums.SessionState.IDLE;
                session.Message = "no server available";
                dbContext.SaveChanges();
                return;
            }
            
            if(ServerNodeLocks.IsNodeLocked(assignedNode.Id) || (lastNode != null && ServerNodeLocks.IsNodeLocked(lastNode.Id)))
            {
                logger.LogWarning($"Node {assignedNode.Id} or {lastNode?.Id} is locked for session {session.Id} skipping start - delayed.");
            }
            else
            {
                session.State = Lib.Models.Enums.SessionState.STARTING;
                dbContext.SaveChanges();
                ServerNodeLocks.Lock(session.Id, assignedNode.Id, lastNode?.Id);
                HandleSessionStartTask(session.Id, assignedNode.Id, lastNode?.Id);
            }
        }

        private PublicHostConnectionInfo GetPublicHostConnectionInfo(ServerNodeEntity node)
        {
            return new PublicHostConnectionInfo()
            {
                HostName = PublicHostName,
                OpenVpnPort = node.OpenVpnPort,
                WireGuardPort = node.WireGuardPort,
            };
        }

        private void HandleNewSession(GNS3aaSDbContext dbContext, SessionEntity session, ServerNodeEntity assignedNode)
        {
            var assignedNodeConnectionInfo = sshConnectionInfoProvider.GetConnectionInfo(assignedNode);

            var createNewSessionWorkflow = new CreateNewSessionNoArchiveWorkflow(assignedNodeConnectionInfo, GetPublicHostConnectionInfo(assignedNode));

            createNewSessionWorkflow.Run();

            if(createNewSessionWorkflow.IsSuccessfull)
            {
                workflowSuccess(dbContext, session, assignedNode, 
                    createNewSessionWorkflow.OpenVPNClientConfigFile, 
                    createNewSessionWorkflow.OpenVPNServerConfigFile,
                    createNewSessionWorkflow.WireGuardConf, 
                    createNewSessionWorkflow.GNS3Version);
            }
            else
            {
                workflowFailed(dbContext, session, assignedNode, createNewSessionWorkflow.Message);
            }
        }

        private void workflowSuccess(GNS3aaSDbContext dbContext, SessionEntity session, ServerNodeEntity assignedNode,
            string? openVPNClientConfigFile,
            string? openVPNServerConfigFile,
            string? wireguardConf, 
            string? gns3version)
        {
            session.State = Lib.Models.Enums.SessionState.RUNNING;
            session.ActiveNode = assignedNode;
            session.ArchiveLocationNodeId = null;
            session.ArchiveLocationNodeName = null;
            session.Updated = DateTime.Now;
            session.Started = DateTime.Now;
            session.WireguardVPNConfig = wireguardConf;
            session.ScheduledShutdown = scheduledShutdownTimeProvider.NextScheduledShutdownTime();
            session.OpenVPNClientConfig = openVPNClientConfigFile;
            if(openVPNServerConfigFile != null) session.OpenVPNServerConfig = openVPNServerConfigFile;
            assignedNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Occupied;
            assignedNode.GNS3Version = gns3version;

            logger.LogInformation($"session {session.Id} started on {assignedNode.Name}");
            dbContext.SaveChanges();
        }

        private void workflowFailed(GNS3aaSDbContext dbContext, SessionEntity session, ServerNodeEntity assignedNode, string message, ServerNodeEntity? lastNode = null)
        {
            session.State = Lib.Models.Enums.SessionState.BROKEN;
            session.Message = message;
            session.Updated = DateTime.Now;
            assignedNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
            if (lastNode != null) lastNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
            logger.LogInformation($"session {session.Id} failed to start on {assignedNode.Name} ({lastNode?.Name})");
            dbContext.SaveChanges();
        }

        private void HandleRestoreSession(GNS3aaSDbContext dbContext, SessionEntity session, ServerNodeEntity assignedNode, ServerNodeEntity? lastNode)
        {
            var assignedNodeConnectionInfo = sshConnectionInfoProvider.GetConnectionInfo(assignedNode);

            var oldNodeConnectionInfo = sshConnectionInfoProvider.GetConnectionInfo(lastNode!);

            var serverWireGuardConf = WireguardStringConfigHelper.ExtractServerKey(session.WireguardVPNConfig);

            var restoreWorkflow = new RestoreSessionWorkflow(
                session.Id.ToString(),
                existingWireGuardServerConf: serverWireGuardConf,
                existingOpenVPNClientConf: session.OpenVPNClientConfig,
                existingOpenVPNServerConf: session.OpenVPNServerConfig,
                publicHostInfo: GetPublicHostConnectionInfo(assignedNode),
                onHost: assignedNodeConnectionInfo,
                fromHost: oldNodeConnectionInfo); 

            restoreWorkflow.WireGuardConf = session.WireguardVPNConfig;

            restoreWorkflow.Run();

            if(restoreWorkflow.IsSuccessfull)
            {
                workflowSuccess(dbContext, session, assignedNode, restoreWorkflow.OpenVPNClientConfigFile, restoreWorkflow.OpenVPNServerConfigFile, restoreWorkflow.WireGuardConf, restoreWorkflow.GNS3Version);
            }
            else
            {
                workflowFailed(dbContext, session, assignedNode, restoreWorkflow.Message, lastNode);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("SessionManagerTask is stopped");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
