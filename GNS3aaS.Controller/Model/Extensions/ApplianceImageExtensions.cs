﻿using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Lib.Models.Pocos;

namespace GNS3aaS.Controller.Model.Extensions
{
    public static class ApplianceImageExtensions
    {

        public static ApplianceImagePoco ToPoco(this ApplianceImageEntity applianceImageEntity)
        {
            return new ApplianceImagePoco()
            {
                Id = applianceImageEntity.Id,
                DownloadUrl = applianceImageEntity.DownloadUrl,
                TemplateData = applianceImageEntity.TemplateData,
                Name = applianceImageEntity.Name,
                RemoteUnpackPath = applianceImageEntity.RemoteUnpackPath,
            };
        }

    }
}
