﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    internal class StopSessionHandler : StartStopSessionHandler
    {
        public StopSessionHandler(ConfigStorage configStorage) : base("stop", configStorage)
        {
        }
    }
}
