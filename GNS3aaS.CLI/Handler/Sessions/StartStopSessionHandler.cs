﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    public class StartStopSessionHandler : BaseHandler, IHandler
    {
        private string commandStr;

        public StartStopSessionHandler(string commandStr, ConfigStorage configStorage) : base(configStorage)
        {
            this.commandStr = commandStr;
        }

        public Command GetCommand()
        {
            var command = new Command(commandStr, $"{commandStr} a session");

            var sessionIdArg = new Argument<string>("sessionId");

            command.AddArgument(sessionIdArg);

            command.SetHandler(async (sessionId) =>
            {
                await handle(sessionId);
            }, sessionIdArg);



            return command;
        }

        private async Task handle(string sessionId)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.SessionAction(commandStr, sessionId);
                if (response.Success)
                {
                    Console.WriteLine($"session {commandStr}");
                }
                else
                {
                    Console.WriteLine($"session {commandStr} failed");
                }
            }
        }
    }
}
