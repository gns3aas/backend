#!/bin/bash
# Original Code: https://gist.githubusercontent.com/pR0Ps/eba07c962447139a33e538afbb7b2749/raw/8cdc920b30a74d2332a3d4cd27c9bbceb4b8f285/make-wg-client.sh

set -e

if [ $# -eq 0 ]; then
    echo "{server_domain}"
    exit 1
fi

wireguard_install=$(sudo apt-get install wireguard -y -qq)

HOSTNAME=`hostname`
server_port="1195"
internal_server_port="1195"
wg_iface="wg0"
config_file="/etc/wireguard/$wg_iface.conf"
server_domain=$1
ipv4_prefix="172.29.11"
allowed_ips="$ipv4_prefix.0/24, 192.168.23.0/24"
ipv4_mask="32"
client_number="2"

# Require root to change wg-related settings
if ! [ "$(id -u)" = "0" ]; then
    echo "ERROR: root is required to configure WireGuard clients"
    exit 1
fi

# Generate and store keypair
server_priv=$(wg genkey)
server_pub=$(echo "$server_priv" | wg pubkey)

# Add peer to config file (blank line is on purpose)
server_config=$(cat <<-EOM
[Interface]
PrivateKey = $server_priv
ListenPort = $internal_server_port
Address = $ipv4_prefix.1/24
EOM
)


function gen_client_conf() {
client_number=$1
client_priv=$(wg genkey)
client_pub=$(echo "$client_priv" | wg pubkey)
client_ipv4="$ipv4_prefix.$client_number/$ipv4_mask"

client_config=$(cat <<-EOM
[Interface]
PrivateKey = $client_priv
Address = $client_ipv4

[Peer]
PublicKey = $server_pub
AllowedIPs = $allowed_ips
Endpoint = $server_domain:$server_port
PersistentKeepalive = 25
EOM
)

server_config+=$(cat <<-EOM


[Peer]
PublicKey = $client_pub
AllowedIPs = $client_ipv4
EOM
)
}

for ((i=2; i<=10; i++))
do
gen_client_conf "$i"
echo "$client_config" | base64 -w 0
echo ""
done

echo "$server_config" > $config_file

echo "$server_config" | base64 -w 0
echo ""

systemctl enable wg-quick@wg0

