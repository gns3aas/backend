﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Sessions
{
    internal class StartSessionHandler : StartStopSessionHandler
    {
        public StartSessionHandler(ConfigStorage configStorage) : base("start", configStorage)
        {
        }
    }
}
