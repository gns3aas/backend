﻿using GNS3aaS.CLI.Services;
using Microsoft.IdentityModel.JsonWebTokens;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler
{
    public class TokenHandler : BaseHandler, IHandler
    {
        public TokenHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var tokenArg = new Argument<string>("token", "jwt token");

            var command = new Command("token", "set token");
            command.AddArgument(tokenArg);

            command.SetHandler(async (token) =>
            {
                await handle(token);
            }, tokenArg);

            return command;
        }

        private async Task handle(string token)
        {
            
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = (JwtSecurityToken)handler.ReadToken(token);
            if(!jsonToken.Payload.Any(i => i.Key == "Default"))
            {
                Console.WriteLine("Invalid json token: missing claim 'Default'");
                return;
            }
            if(jsonToken.ValidTo < DateTime.Now)
            {
                Console.WriteLine("token passed validity");
                return;
            }
            configStorage.JwtToken = token;
            configStorage.Save();
            Console.WriteLine($"stored token in config {ConfigStorage.GetConfigFilePath()}");
        }
    }
}
