﻿using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Lib.Models.Pocos;

namespace GNS3aaS.Controller.Model.Extensions
{
    public static class UserPocoExtensions
    {
        public static UserPoco ToPoco(this UserEntity userEntity) {
            return new UserPoco()
            {
                Id = userEntity.Id,
                EMailHash = userEntity.EMailHash,
                GroupName = userEntity.GroupName,
                JwtTokenValidUntil = userEntity.JwtTokenValidUntil,
            };
        }

    }
}
