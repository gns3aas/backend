﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GNS3aaS.Controller.Migrations
{
    /// <inheritdoc />
    public partial class OpenVPNConfig : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OpenVPNClientConfig",
                table: "ServerNodes");

            migrationBuilder.AddColumn<string>(
                name: "OpenVPNClientConfig",
                table: "Sessions",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpenVPNServerConfig",
                table: "Sessions",
                type: "TEXT",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OpenVPNClientConfig",
                table: "Sessions");

            migrationBuilder.DropColumn(
                name: "OpenVPNServerConfig",
                table: "Sessions");

            migrationBuilder.AddColumn<string>(
                name: "OpenVPNClientConfig",
                table: "ServerNodes",
                type: "TEXT",
                nullable: true);
        }
    }
}
