﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class SystemStatsCollectorPlaybook : IPlaybook
    {
        public const string CPU_ARTIFACT = "cpu";
        public const string RAM_ARTIFACT = "ram";
        public const string HDD_ARTIFACT = "hdd";

        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public SystemStatsCollectorPlaybook()
        {
            orderList.Add(ExecutionOrderHelper.CPUStats(CPU_ARTIFACT));
            orderList.Add(ExecutionOrderHelper.RAMStats(RAM_ARTIFACT));
            orderList.Add(ExecutionOrderHelper.HDDStats(HDD_ARTIFACT));
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }
    }
}
