﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    internal class AddServerNodeHandler : BaseHandler, IHandler
    {
        public AddServerNodeHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("add", $"add server node");

            var nameArgs = new Argument<string>("name");

            var hostArgs = new Argument<string>("host");

            var openVpnPortArgs = new Argument<int>("openvpnport");
            
            var wireGuardPortArgs = new Argument<int>("wireguardport");

            command.AddArgument(nameArgs);
            command.AddArgument(hostArgs);
            command.AddArgument(openVpnPortArgs);
            command.AddArgument(wireGuardPortArgs);

            command.SetHandler(async (name, host, openVpnPort, wireGuardPort) =>
            {
                await handle(name, host, openVpnPort, wireGuardPort);
            }, nameArgs, hostArgs, openVpnPortArgs, wireGuardPortArgs);

            return command;
        }

        private async Task handle(string name, string host, int openVpnPort, int wireGuardPort)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var response = await client.AddServerNode(new Lib.Models.Requests.NewServerNodeRequest()
                {
                    HostAddress = host,
                    Name = name,
                    OpenVpnPort = openVpnPort,
                    WireGuardPort = wireGuardPort,
                });
                if (response.Success)
                {
                    Console.WriteLine(response.Data);
                }
                else
                {
                    Console.WriteLine($"server node add failed: {response.Message}");
                }
            }
        }
    }
}

