﻿using GNS3aaS.Lib.Models.Enums;

namespace GNS3aaS.Lib.Models.Pocos
{
    public class SessionPoco
    {
        public Guid Id { get; set; }

        public SessionState State { get; set; }

        public Guid UserId { get; set; }

        public Guid? ActiveNodeId { get; set; }

        public string? ActiveNodeName { get; set; }

        public string? Message { get; set; }

        public string? OpenVPNConfiguration { get; set; }

        public string? WireguardVPNConfig { get; set; }

        public DateTime? ScheduledShutdown { get; set; }

        public string? GNS3Version { get; set; }

        public Guid? ArchiveLocationNodeId { get; set; }

        public string? ArchiveLocationNodeName { get; set; }

        public bool KeepSession { get; set; }

        public string Email { get; set; }

        public bool KeepRunning { get; set; }

        public DateTime? Started { get; set; }

        public DateTime Updated { get; set; }

        public int? ArchiveSize { get; set; }


    }
}
