﻿using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Nodes
{
    public class ServerNodesEditParameters : BaseHandler, IHandler
    {
        public ServerNodesEditParameters(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var command = new Command("edit", "edit parameters");
            
            var nodeIdArgs = new Argument<string>("nodeId");

            var wireGuardPortOption = new Option<int?>("--wireguard-port");

            var openVpnPortOption = new Option<int?>("--openvpn-port");

            command.AddArgument(nodeIdArgs);
            command.AddOption(wireGuardPortOption);
            command.AddOption(openVpnPortOption);

            command.SetHandler(async (nodeId, wireGuardPort, openVpnPort) =>
            {
                await handle(nodeId, wireGuardPort, openVpnPort);
            }, nodeIdArgs, wireGuardPortOption, openVpnPortOption);

            return command;
        }

        private async Task handle(string nodeId, int? wireGuardPort, int? openVpnPort)
        {
            using (var client = new GNS3aaSApiClient(configStorage.BaseUrl, configStorage.JwtToken))
            {
                var updateRequestResult = await client.EditServerNode(new Lib.Models.Requests.UpdateServerNodeRequest()
                {
                    Id = Guid.Parse(nodeId),
                    WireGuardPort = wireGuardPort,
                    OpenVpnPort = openVpnPort,
                });
                if(updateRequestResult.Success) 
                {
                    Console.WriteLine($"{nodeId}");
                }
                else
                {
                    Console.WriteLine($"failed to edit {nodeId}");
                }
            }
        }
    }
}
