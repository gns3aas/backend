﻿using GNS3aaS.CLI.Handler.Sessions;
using GNS3aaS.CLI.Services;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.CLI.Handler.Users
{
    internal class UsersHandler : BaseHandler, IHandler
    {
        public UsersHandler(ConfigStorage configStorage) : base(configStorage)
        {
        }

        public Command GetCommand()
        {
            var subCommands = new Command("user");

            subCommands.Add(new ImportUsersHandler(configStorage).GetCommand());
            subCommands.Add(new DeleteCommonUsersHandler(configStorage).GetCommand());
            subCommands.Add(new UserStatsHandler(configStorage).GetCommand());

            return subCommands;
        }
    }
}
