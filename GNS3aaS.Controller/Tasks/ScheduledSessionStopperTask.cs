﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Services;
using GNS3aaS.Theater.Workflows;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Globalization;

namespace GNS3aaS.Controller.Tasks
{
    public class ScheduledSessionStopperTask : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly ILogger<ScheduledSessionStopperTask> logger;
        private readonly ScheduledShutdownTimeProvider scheduledShutdownTime;
        private readonly SshConnectionInfoProvider sshConnectionInfoProvider;

        public IServiceProvider Services { get; }
        public IConfiguration Configuration { get; }

        private bool isRunning = false;

        private DateTime nextStop;

        private bool isConfigured = false;

        public ScheduledSessionStopperTask(ILogger<ScheduledSessionStopperTask> _logger
            , IServiceProvider services, IConfiguration configuration, ScheduledShutdownTimeProvider scheduledShutdownTime)
        {
            logger = _logger;
            Services = services;
            Configuration = configuration;
            this.scheduledShutdownTime = scheduledShutdownTime;
            nextStop = DateTime.Now;
        }

        public void Dispose()
        {
            _timer?.Change(Timeout.Infinite, 0);
            _timer?.Dispose();
        }

        private void init()
        {
            
            if(!scheduledShutdownTime.IsShutdownTimeSet)
            {
                return;
            }
            nextStop = scheduledShutdownTime.NextScheduledShutdownTime();
            LogNextEventDateTime();
            isConfigured = true;
        }

        private void LogNextEventDateTime()
        {
            logger.LogInformation($"All sessions will be stopped {nextStop}");
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            init();
            if(!isConfigured)
            {
                logger.LogWarning("ScheduledSessionStopperTask is not configured, set Schedule:StopSessionDaily in format HH:mm to enable it.");
                return Task.CompletedTask;
            }
            _timer = new Timer(tryDoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(60));

            logger.LogInformation("ScheduledSessionStopperTask is started.");

            return Task.CompletedTask;
        }

        private void tryDoWork(object? state)
        {
            try
            {
                if (isRunning) return;
                isRunning = true;
                doWork(state);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    throw ex;
                }
                logger.LogError($"ScheduledSessionStopperTask {ex.Message}", ex);
            }
            isRunning = false;
        }

        private void doWork(object? state)
        {
            if(DateTime.Now > nextStop)
            {
                logger.LogInformation("Reached scheduled time. Marking all running sessions for stopping");
                nextStop = scheduledShutdownTime.NextScheduledShutdownTime();
                MarkAllSessionToStop();
                LogNextEventDateTime();
            }
        }

        public void MarkAllSessionToStop()
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var sessions = dbContext.Sessions.Where(s => s.State == Lib.Models.Enums.SessionState.RUNNING && s.KeepRunning == false).ToList();

                if (!sessions.Any()) return;

                logger.LogInformation($"Found {sessions.Count} sessions running that are going to be marked for stopping");

                foreach (var session in sessions)
                {
                    session.State = Lib.Models.Enums.SessionState.STOP_REQUESTED;
                }
                dbContext.SaveChanges();
            }
            
        }


        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("ScheduledSessionStopperTask is stopped");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
