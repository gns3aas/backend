﻿using GNS3aaS.Lib.Logging;
using GNS3aaS.Theater.Playbooks;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Workflows
{
    public class StopAndArchiveSessionWorkflow : IWorkflow
    {
        private readonly string sessionId;
        private readonly SshConnectionInfo onHost;

        public bool IsSuccessfull { get; private set; } = false;
        public string Message { get; private set; } = "";

        public bool IsFinished { get; private set; } = false;

        public int? ArchiveFileSize { get; set; }

        private readonly ILogger<StopAndArchiveSessionWorkflow> logger = StaticLoggerFactory.GetStaticLogger<StopAndArchiveSessionWorkflow>();

        public StopAndArchiveSessionWorkflow(string sessionId, SshConnectionInfo onHost)
        {
            this.sessionId = sessionId;
            this.onHost = onHost;
        }

        public void Run()
        {
            if(!DoStopAndArchivePlaybook(this.onHost, sessionId))
            {
                Message = $"Failed to archive and stop session {sessionId}";
                IsFinished = true;
                IsSuccessfull = false;
                return;
            }
            var stopGns3AndOpenVPN = new PlaybookRunner(this.onHost, new StopVpnAndGNS3Playbook());
            stopGns3AndOpenVPN.Run();
            IsSuccessfull = stopGns3AndOpenVPN.IsSuccess();
            IsFinished = true;
            
        }

        public bool DoStopAndArchivePlaybook(SshConnectionInfo host, string sessionId)
        {
            var stopPlaybook = new StopAndArchivePlaybook(sessionId);

            var playbookRunner = new PlaybookRunner(host, stopPlaybook);
            playbookRunner.Run();

            if(!playbookRunner.IsSuccess())
            {
                return false;
            }

            if(playbookRunner.Artifacts.ContainsKey(StopAndArchivePlaybook.FILE_SIZE_ARTIFACT_NAME)
                && int.TryParse(playbookRunner.Artifacts[StopAndArchivePlaybook.FILE_SIZE_ARTIFACT_NAME], out var fileSize))
            {
                ArchiveFileSize = fileSize;
            }
            return true;
        }
    }
}
