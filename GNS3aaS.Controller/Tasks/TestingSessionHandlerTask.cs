﻿using GNS3aaS.Controller.Data;
using GNS3aaS.Controller.Model.Db;
using GNS3aaS.Controller.Services.ServerNodeLocking;
using GNS3aaS.Controller.Services;
using GNS3aaS.Theater.Workflows;
using System.Diagnostics;

namespace GNS3aaS.Controller.Tasks
{
    public class TestingSessionHandlerTask : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly ILogger<TestingSessionHandlerTask> logger;
        private readonly ScheduledShutdownTimeProvider scheduledShutdownTimeProvider;
        public const int MAX_NUMBER_OF_PARALLEL_SESSION_STARTS = 5;

        public IServiceProvider Services { get; }
        public IConfiguration Configuration { get; }

        private bool isRunning = false;


        public TestingSessionHandlerTask(ILogger<TestingSessionHandlerTask> _logger, IServiceProvider services
            , IConfiguration configuration, ScheduledShutdownTimeProvider scheduledShutdownTimeProvider)
        {
            logger = _logger;
            Services = services;
            Configuration = configuration;
            this.scheduledShutdownTimeProvider = scheduledShutdownTimeProvider;
        }

        public void Dispose()
        {
            _timer?.Change(Timeout.Infinite, 0);
            _timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(tryDoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(15));

            logger.LogInformation("TestingSessionHandlerTask is started.");

            return Task.CompletedTask;
        }

        private void tryDoWork(object? state)
        {
            try
            {
                if (isRunning) return;
                isRunning = true;
                doWorkUp(state);
                doWorkDown(state);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    throw ex;
                }
                logger.LogError($"TestingSessionHandlerTask {ex.Message}", ex);
            }
            isRunning = false;
        }

        private void doWorkUp(object? state)
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var sessions = dbContext.Sessions.Where(s => s.State == Lib.Models.Enums.SessionState.START_REQUESTED).ToList();

                if (!sessions.Any()) return;

                logger.LogInformation($"Found {sessions.Count} sessions ready to start");

                int counter = 0;

                foreach (var session in sessions)
                {
                    HandleStartSession(dbContext, session);
                }
            }
        }

        private void doWorkDown(object? state)
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var sessions = dbContext.Sessions.Where(s => s.State == Lib.Models.Enums.SessionState.STOP_REQUESTED).ToList();

                if (!sessions.Any()) return;

                logger.LogInformation($"Found {sessions.Count} sessions ready to stop");

                foreach (var session in sessions)
                {
                    HandleStopSession(dbContext, session);
                }
            }
        }

        private void HandleStopSession(GNS3aaSDbContext dbContext, SessionEntity session)
        {
            var assignedNode = dbContext.ServerNodes.Where(i => i.Id == session.ActiveNodeId).Single();

            session.State = Lib.Models.Enums.SessionState.IDLE;
            session.Updated = DateTime.Now;
            session.ArchiveLocationNodeId = session.ActiveNodeId;
            session.ArchiveLocationNodeName = session.ActiveNode?.Name;
            session.ActiveNode = null;
            session.ActiveNodeId = null;
            session.Started = null;
            
            assignedNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Available;
            dbContext.SaveChanges();

            logger.LogInformation($"session {session.Id} stopped");
        }


        private void HandleStartSession(GNS3aaSDbContext dbContext, SessionEntity session)
        {
            ServerNodeEntity? assignedNode = null;
            ServerNodeEntity? lastNode = null;
            if (session.ArchiveLocationNodeId != null)
            {
                lastNode = dbContext.ServerNodes.SingleOrDefault(s => s.Id == session.ArchiveLocationNodeId);
                if (lastNode != null && lastNode.ServerNodeState == Lib.Models.Enums.ServerNodeState.Available)
                {
                    assignedNode = lastNode;
                }
                if (lastNode == null)
                {
                    logger.LogError($"node that contains project archive not found for session {session.Id}");
                    session.State = Lib.Models.Enums.SessionState.BROKEN;
                    session.Message = "node that contains project archive not found";
                    dbContext.SaveChanges();
                    return;
                }
            }
            if (assignedNode == null)
            {
                var rnd = new Random();
                assignedNode = dbContext.ServerNodes.Where(s => s.ServerNodeState == Lib.Models.Enums.ServerNodeState.Available).ToList().OrderBy(x => rnd.Next()).FirstOrDefault();
            }
            if (assignedNode == null)
            {
                logger.LogError($"No server available for session {session.Id}");
                session.State = Lib.Models.Enums.SessionState.IDLE;
                session.Message = "no server available";
                dbContext.SaveChanges();
                return;
            }

            session.State = Lib.Models.Enums.SessionState.STARTING;
            dbContext.SaveChanges();

            workflowSuccess(dbContext, session, assignedNode);
            
        }

        private void tryUpdateSessionToBroken(Guid sessionId, string message)
        {
            using (var dbContext = new GNS3aaSDbContext())
            {
                var session = dbContext.Sessions.Single(s => s.Id == sessionId);
                session.Message = message;
                session.Updated = DateTime.UtcNow;
                session.Started = null;
                session.State = Lib.Models.Enums.SessionState.BROKEN;
                dbContext.SaveChanges();
            }
        }


        private void workflowSuccess(GNS3aaSDbContext dbContext, SessionEntity session, ServerNodeEntity assignedNode)
        {
            session.State = Lib.Models.Enums.SessionState.RUNNING;
            session.ActiveNode = assignedNode;
            session.ArchiveLocationNodeId = null;
            session.ArchiveLocationNodeName = null;
            session.Updated = DateTime.Now;
            session.Started = DateTime.Now;
            session.ScheduledShutdown = scheduledShutdownTimeProvider.NextScheduledShutdownTime();
            session.WireguardVPNConfig = "W0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IHdGZEloYmVxa3VlblkxSS9GWjE4Y2JUWGY1dk1RenBCcE9SQ25xRTNUVTA9CkFkZHJlc3MgPSAxNzIuMjkuMTEuMi8zMgoKW1BlZXJdClB1YmxpY0tleSA9IDRLdXc1QzBDYkdRZlpvOEV3ZXAwTVpmMzExUTI1US9pdDhUSHhkYXRLdzg9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMC8yNCwgMTkyLjE2OC4yMy4wLzI0CkVuZHBvaW50ID0gY2xvdWQudGJ6LmNoOjMyMDMyClBlcnNpc3RlbnRLZWVwYWxpdmUgPSAyNQo=\r\nW0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IElHQ29HRVk1L3BLbndnWS9JNURLUXk3bmdPc2hOQnI1S2pFSjE5aUlXVU09CkFkZHJlc3MgPSAxNzIuMjkuMTEuMy8zMgoKW1BlZXJdClB1YmxpY0tleSA9IDRLdXc1QzBDYkdRZlpvOEV3ZXAwTVpmMzExUTI1US9pdDhUSHhkYXRLdzg9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMC8yNCwgMTkyLjE2OC4yMy4wLzI0CkVuZHBvaW50ID0gY2xvdWQudGJ6LmNoOjMyMDMyClBlcnNpc3RlbnRLZWVwYWxpdmUgPSAyNQo=\r\nW0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IFdBZDlReGExSjB3cVBXR1BkckV2SjczZFhXemNvZ1RpN3gzMXhoV0dRV2s9CkFkZHJlc3MgPSAxNzIuMjkuMTEuNC8zMgoKW1BlZXJdClB1YmxpY0tleSA9IDRLdXc1QzBDYkdRZlpvOEV3ZXAwTVpmMzExUTI1US9pdDhUSHhkYXRLdzg9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMC8yNCwgMTkyLjE2OC4yMy4wLzI0CkVuZHBvaW50ID0gY2xvdWQudGJ6LmNoOjMyMDMyClBlcnNpc3RlbnRLZWVwYWxpdmUgPSAyNQo=\r\nW0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IEtEdXhGeTEwTUVMRjlmQVk0QmE3MU4wc2M4dkdHY1dlVkJ4WExhS3RPa289CkFkZHJlc3MgPSAxNzIuMjkuMTEuNS8zMgoKW1BlZXJdClB1YmxpY0tleSA9IDRLdXc1QzBDYkdRZlpvOEV3ZXAwTVpmMzExUTI1US9pdDhUSHhkYXRLdzg9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMC8yNCwgMTkyLjE2OC4yMy4wLzI0CkVuZHBvaW50ID0gY2xvdWQudGJ6LmNoOjMyMDMyClBlcnNpc3RlbnRLZWVwYWxpdmUgPSAyNQo=\r\nW0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IFdEaFRRMmQ5WTNUcENPUThhb01WRFZxWjlDQ0x1eFR3SGlEdko4Z0QwSHc9CkFkZHJlc3MgPSAxNzIuMjkuMTEuNi8zMgoKW1BlZXJdClB1YmxpY0tleSA9IDRLdXc1QzBDYkdRZlpvOEV3ZXAwTVpmMzExUTI1US9pdDhUSHhkYXRLdzg9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMC8yNCwgMTkyLjE2OC4yMy4wLzI0CkVuZHBvaW50ID0gY2xvdWQudGJ6LmNoOjMyMDMyClBlcnNpc3RlbnRLZWVwYWxpdmUgPSAyNQo=\r\nW0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IHVGK0ZCNXEyTFFudTNabitxdXZWM1B2Z2ZlV0V3R3Vza0hzcTV5Uy9KSFU9CkFkZHJlc3MgPSAxNzIuMjkuMTEuNy8zMgoKW1BlZXJdClB1YmxpY0tleSA9IDRLdXc1QzBDYkdRZlpvOEV3ZXAwTVpmMzExUTI1US9pdDhUSHhkYXRLdzg9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMC8yNCwgMTkyLjE2OC4yMy4wLzI0CkVuZHBvaW50ID0gY2xvdWQudGJ6LmNoOjMyMDMyClBlcnNpc3RlbnRLZWVwYWxpdmUgPSAyNQo=\r\nW0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IElKdXFjc3V5MWxNdTdWT3hJZXZmZVdwSmFicWNjV29qTU9SY2lTVE00VzQ9CkFkZHJlc3MgPSAxNzIuMjkuMTEuOC8zMgoKW1BlZXJdClB1YmxpY0tleSA9IDRLdXc1QzBDYkdRZlpvOEV3ZXAwTVpmMzExUTI1US9pdDhUSHhkYXRLdzg9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMC8yNCwgMTkyLjE2OC4yMy4wLzI0CkVuZHBvaW50ID0gY2xvdWQudGJ6LmNoOjMyMDMyClBlcnNpc3RlbnRLZWVwYWxpdmUgPSAyNQo=\r\nW0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IEVKdnMyaGpVMTNqUnFieGhTZDF0dFlDcWZJVkh2K0tDVHl2R3BEbnBOR1k9CkFkZHJlc3MgPSAxNzIuMjkuMTEuOS8zMgoKW1BlZXJdClB1YmxpY0tleSA9IDRLdXc1QzBDYkdRZlpvOEV3ZXAwTVpmMzExUTI1US9pdDhUSHhkYXRLdzg9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMC8yNCwgMTkyLjE2OC4yMy4wLzI0CkVuZHBvaW50ID0gY2xvdWQudGJ6LmNoOjMyMDMyClBlcnNpc3RlbnRLZWVwYWxpdmUgPSAyNQo=\r\nW0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IGNMV0R5eWY3K0RTWFliM0dQMzljK1pQZjJzMGZaeFlNakthbi9NRTF0MDQ9CkFkZHJlc3MgPSAxNzIuMjkuMTEuMTAvMzIKCltQZWVyXQpQdWJsaWNLZXkgPSA0S3V3NUMwQ2JHUWZabzhFd2VwME1aZjMxMVEyNVEvaXQ4VEh4ZGF0S3c4PQpBbGxvd2VkSVBzID0gMTcyLjI5LjExLjAvMjQsIDE5Mi4xNjguMjMuMC8yNApFbmRwb2ludCA9IGNsb3VkLnRiei5jaDozMjAzMgpQZXJzaXN0ZW50S2VlcGFsaXZlID0gMjUK\r\nW0ludGVyZmFjZV0KUHJpdmF0ZUtleSA9IGNQdEVRQ2RUdUw2ZWp5NU1POXh3ek02OGFCMXFaVFBRS2JId1lSRUgwMzA9Ckxpc3RlblBvcnQgPSAxMTk1CkFkZHJlc3MgPSAxNzIuMjkuMTEuMS8yNAoKW1BlZXJdClB1YmxpY0tleSA9IHdKT2YrODNxWEgwTDc4eE5DWFZFdnVTWjN2dkN6YTFONnNuY2RMM0o1RWc9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMi8zMgoKW1BlZXJdClB1YmxpY0tleSA9IG1Yc3d1VGp0WW5IcFc3dCtiYjN1bXUrMU9RdE1iRXdRWnJKOUNuRDVDQjg9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMy8zMgoKW1BlZXJdClB1YmxpY0tleSA9IFBwZ1hWcUpuMkNqTFh0U1ZsbzU1QW1wcDRXVjRCY3UrVGdkYXpJZmlSVFU9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuNC8zMgoKW1BlZXJdClB1YmxpY0tleSA9IGEwUEFxeWVTd3R1TTZPdzRPNnZ1R1dXYVlNajRJRmlObWdKa2t5aUxEQ3M9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuNS8zMgoKW1BlZXJdClB1YmxpY0tleSA9IEFqdE50NktLS3JYb2tSdFZaRkg5Qk9Sb0pvelhmTjlGU095cVFoYVJURUU9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuNi8zMgoKW1BlZXJdClB1YmxpY0tleSA9IEdNcVFYdFF4WHlCR3NCT0hsblV1U2t0aVZPT09CSytWL3cwU0NrVm44SHM9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuNy8zMgoKW1BlZXJdClB1YmxpY0tleSA9IEVmRjE4b2t0Tk0xblNSN1ZpdXA3a3hOTWxWWVBXYzFXNktWb1B5VmZ1U0U9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuOC8zMgoKW1BlZXJdClB1YmxpY0tleSA9IFhqZDdqVGI1NFpmN2syaWtpQ3phblpUZ3M0OXFzaVk3aExsTXYzcGRRUXM9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuOS8zMgoKW1BlZXJdClB1YmxpY0tleSA9IEhteUxCTVNHdXEzeHp4cVhMVFl6T1h1cmRsS0pnVkdneVY1REJpQVN2MVE9CkFsbG93ZWRJUHMgPSAxNzIuMjkuMTEuMTAvMzIK";
            session.OpenVPNClientConfig = "OpenVPN";
            session.OpenVPNServerConfig = "OpenVPN";
            assignedNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Occupied;
            assignedNode.GNS3Version = "2.2.45";

            logger.LogInformation($"session {session.Id} started on {assignedNode.Name}");
            dbContext.SaveChanges();
        }

        private void workflowFailed(GNS3aaSDbContext dbContext, SessionEntity session, ServerNodeEntity assignedNode, string message, ServerNodeEntity? lastNode = null)
        {
            session.State = Lib.Models.Enums.SessionState.BROKEN;
            session.Message = message;
            session.Updated = DateTime.Now;
            assignedNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
            if (lastNode != null) lastNode.ServerNodeState = Lib.Models.Enums.ServerNodeState.Broken;
            logger.LogInformation($"session {session.Id} failed to start on {assignedNode.Name} ({lastNode?.Name})");
            dbContext.SaveChanges();
        }


        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("TestingSessionHandlerTask is stopped");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
