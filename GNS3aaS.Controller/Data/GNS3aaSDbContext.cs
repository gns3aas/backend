﻿using GNS3aaS.Controller.Model.Db;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Runtime.InteropServices;
using System;

namespace GNS3aaS.Controller.Data
{
    public class GNS3aaSDbContext : DbContext
    {

        public DbSet<SessionEntity> Sessions { get; set; }

        public DbSet<UserEntity> Users { get; set; }

        public DbSet<ServerNodeEntity> ServerNodes { get; set; }

        public DbSet<ActiveGroupEntity> ActiveGroups { get; set; }

        public DbSet<ApplianceImageEntity> ApplianceImages { get; set; }

        public static string? DbPath { get; private set; } = null;

        private readonly bool inMemory;

        public GNS3aaSDbContext(bool inMemory = false)
        {
            var path = "";
            if (!Global.IsDevelopment)
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    //path = Path.Join("/var/lib/gns3aas");
                    path = Path.Join("data");
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    path = Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "GNS3aaS");
                }
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            }
            if(DbPath == null)
            {
                DbPath = System.IO.Path.Join(path, "gns3aas.db");
                if (Global.IsUnitTest)
                {
                    DbPath = Path.GetTempFileName();
                }
            }
            this.inMemory = inMemory;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseLazyLoadingProxies();
            //options.LogTo(x => Debug.WriteLine(x));
            if (inMemory)
            {
                options.UseSqlite("DataSource=myshareddb;mode=memory;cache=shared");
            }
            else
            {
                options.UseSqlite($"Data Source={DbPath}");
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            base.OnModelCreating(modelBuilder);
        }

    }
}
