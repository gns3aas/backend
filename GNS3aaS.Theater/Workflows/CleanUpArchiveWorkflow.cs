﻿using GNS3aaS.Theater.Playbooks;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Workflows
{
    public class CleanUpArchiveWorkflow : IWorkflow
    {
        private readonly List<string> validSessionIds;

        public bool IsSuccessfull { get; set; } = false;

        public string Message { get; set; } = "";

        public bool IsFinished { get; set; } = false;
        public SshConnectionInfo Connection { get; }

        public int NumOfArchivesDeleted = 0;

        public int NumOfArchivesFound = 0;

        public List<string> DeletedArchivesIds = new List<string>();

        public CleanUpArchiveWorkflow(SshConnectionInfo connection, List<string> validSessionIds)
        {
            Connection = connection;
            this.validSessionIds = validSessionIds.Select(v => v.ToLower()).ToList();
        }

        public void Run()
        {
            var listFilesPlaybook = new PlaybookRunner(Connection, new LsArchivePlaybook());

            listFilesPlaybook.Run();
            if(!listFilesPlaybook.IsSuccess())
            {
                IsFinished = true;
                IsSuccessfull = false;
                return;
            }

            var listOfArchiveFiles = listFilesPlaybook.Artifacts[LsArchivePlaybook.LS_ARTIFACT]
                .Trim().Split("\n").ToList()
                .Select(i => i.Replace(LoadAndStartPlaybook.ARCHIVE_FILE_ENDING, "").Trim().ToLower())
                .Where(i => !string.IsNullOrWhiteSpace(i) && i.Length == 36).ToList();

            NumOfArchivesFound = listOfArchiveFiles.Count;

            var listOfArchiveFilesToDelete = listOfArchiveFiles.Where(i => !validSessionIds.Contains(i)).ToList();

            foreach (var archiveFile in listOfArchiveFilesToDelete)
            {
                NumOfArchivesDeleted++;
                DeletedArchivesIds.Add(archiveFile);
                var rmFilePlaybook = new PlaybookRunner(Connection, new DeleteUserProjectArchivePlaybook(archiveFile));
                rmFilePlaybook.Run();
                if(!rmFilePlaybook.IsSuccess())
                {
                    IsFinished = true;
                    IsSuccessfull = false;
                    return;
                }
            }
            IsFinished = true;
            IsSuccessfull = true;
        }

    }
}
