﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNS3aaS.Theater.Playbooks
{
    public class VerifyMachineInstallStatePlaybook : IPlaybook
    {
        public string[] ListFiles = {
            "/opt/cloudinitinstall/gns3.installed",
            "/opt/cloudinitinstall/networking.installed",
            "/opt/cloudinitinstall/tbz-gns3-config.installed",
        };
        
        List<FullExecutionOrder> orderList = new List<FullExecutionOrder>();

        public VerifyMachineInstallStatePlaybook()
        {
            foreach(var file in ListFiles)
            {
                orderList.Add(ExecutionOrderHelper.FileExists(file));
            }
        }

        public List<FullExecutionOrder> GetExecutionOrders()
        {
            return orderList;
        }

    }
}